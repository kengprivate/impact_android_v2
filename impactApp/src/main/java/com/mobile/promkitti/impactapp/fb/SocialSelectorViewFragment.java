package com.mobile.promkitti.impactapp.fb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.promkitti.impactapp.ActivityParameters;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.util.FlurryUtil;

public class SocialSelectorViewFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View v = inflater.inflate(R.layout.social_selector, container, false);
        return v;
    }

    public void fbButtonListener(View view) {
        Intent intent = new Intent(getActivity(), SocialWebViewActivity.class);
        intent.putExtra(ActivityParameters.URL,"http://www.facebook.com/IMPACTvenue");
        startActivity(intent);
        FlurryUtil.logEvent(FlurryConstants.SOCIAL_FB);
    }

    public void igButtonListener(View view) {
        Intent intent = new Intent(getActivity(), SocialWebViewActivity.class);
        intent.putExtra(ActivityParameters.URL,"https://www.instagram.com/impactvenue");
        startActivity(intent);
        FlurryUtil.logEvent(FlurryConstants.SOCIAL_IG);
    }
}
    