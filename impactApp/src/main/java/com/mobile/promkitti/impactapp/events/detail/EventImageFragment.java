package com.mobile.promkitti.impactapp.events.detail;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.common.EventImage;
import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.valueobjects.CalendarEvent;

public class EventImageFragment extends Fragment {

	private DataController dc = DataController.getInstance();
	private EventImage media;
	private ImageView eventDetailImageView;
	private Drawable placeHolder;
	private ImageView placeholderView;
	private ProgressBar mSpinner;

	public static final EventImageFragment newInstance(EventMedia eventMedia) {
		EventImageFragment fragment = new EventImageFragment();
		final Bundle args = new Bundle(1);
		args.putParcelable("EVENT_MEDIA", eventMedia);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * When creating, retrieve this instance's number from its arguments.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		media = getArguments().getParcelable("EVENT_MEDIA");
	}

	/**
	 * The Fragment's UI is just a simple text view showing its instance number.
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.event_image_fragment, container, false);
		
		eventDetailImageView = (ImageView) rootView.findViewById(R.id.eventDetailImage);
		mSpinner = (ProgressBar) rootView.findViewById(R.id.eventDetailImageProgressBar);
		placeHolder = ContextCompat.getDrawable(getContext(), R.drawable.ed_placeholder);

		/* Check Screen Density */ 
        DisplayMetrics metrics = new DisplayMetrics();    
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);    
        int screenDensity = metrics.densityDpi;
        int targetWidth = 0;
        int targetHeight = 0;
        
        if (screenDensity <= DisplayMetrics.DENSITY_HIGH) {
			float MULTIPLIER_FACTOR = 0.98f;
			targetWidth = (int) (placeHolder.getIntrinsicWidth() * MULTIPLIER_FACTOR);
			targetHeight = (int) (placeHolder.getIntrinsicHeight() * MULTIPLIER_FACTOR);
        } else if (screenDensity <= DisplayMetrics.DENSITY_XHIGH) {
			float MULTIPLIER_FACTOR = 1.0f;
			targetWidth = (int) (placeHolder.getIntrinsicWidth() * MULTIPLIER_FACTOR);
			targetHeight = (int) (placeHolder.getIntrinsicHeight() * MULTIPLIER_FACTOR);
        } else {
			float MULTIPLIER_FACTOR = 1.0f;
			targetWidth = (int) (placeHolder.getIntrinsicWidth() * MULTIPLIER_FACTOR);
			targetHeight = (int) (placeHolder.getIntrinsicHeight() * MULTIPLIER_FACTOR);
        }
		FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(targetWidth, targetHeight);
		frameLayoutParams.gravity = Gravity.CENTER;
		eventDetailImageView.setLayoutParams(frameLayoutParams);

		new LoadingImageTask().execute(this.media);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	private class LoadingImageTask extends AsyncTask<EventImage, Void, EventImage> {

		private Drawable content;

		/**
		 * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
		 */
		protected EventImage doInBackground(EventImage... val) {
			try {
				this.content = dc.getEventDetailImage(media.getId(), media.getUrl());
				return val[0];
			} catch (ImpactException e) {
				return val[0];
			}
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
		 */
		protected void onPostExecute(EventImage media) {
			CalendarEvent event;
			try {
				event = dc.getEventDetail(media.getId());
				if (event == null) {
					return;
				}
				if (this.content != null) {
					eventDetailImageView.setImageDrawable(this.content);
					eventDetailImageView.setScaleType(ScaleType.FIT_XY);
				} else {
					dc.setEventDetailImage(media.getId(), media.getUrl(), placeHolder);
				}
			} catch (ImpactException e) {
				e.printStackTrace();
			}

			mSpinner.setVisibility(View.GONE);
		}

	}
}