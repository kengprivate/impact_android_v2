package com.mobile.promkitti.impactapp.common;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nananetta on 3/15/16.
 */
public class ShareUtils {

    public static Intent share(Activity context, View view, String shareBody, String caption, String contentURL, String imageURL) {

        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, caption);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);

        // Share image
        Uri uri = null;
        try {
            URL url = new URL(imageURL);
            File file = new File(context.getExternalFilesDir(null)+"/image.jpg");
            org.apache.commons.io.FileUtils.copyURLToFile(url, file);
            uri = FileProvider.getUriForFile(context,
                    "com.mobile.promkitti.impactapp" , file);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        PackageManager pm = view.getContext().getPackageManager();
        List<ResolveInfo> launchables = pm.queryIntentActivities(sharingIntent, 0);

        for (final ResolveInfo app : launchables) {
            String packageName = app.activityInfo.packageName;
            String className = app.activityInfo.name;
            Intent targetedShareIntent = new Intent(Intent.ACTION_SEND);
            targetedShareIntent.setClassName(packageName, className);
            targetedShareIntent.putExtra(Intent.EXTRA_SUBJECT, caption);
            targetedShareIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            if("com.facebook.katana".equalsIgnoreCase(packageName)) {
                targetedShareIntent.setType("image/*");
                targetedShareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            } else if ("jp.naver.line.android".equalsIgnoreCase(packageName)) {
                targetedShareIntent.setType("text/plain");
            }else {
                targetedShareIntent.setType("image/*");
                targetedShareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            }
            targetedShareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            targetedShareIntents.add(targetedShareIntent);
        }
        Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share to...");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
        return chooserIntent;
    }

}
