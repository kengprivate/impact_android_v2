package com.mobile.promkitti.impactapp.events.detail;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.util.Pair;

import com.mobile.promkitti.impactapp.R;

import java.util.HashMap;
import java.util.Map;

public class BuildingMapBuilder {

	private static int mapWidth = 1429;
	private static int mapHeight = 1603;
	private static int pinWidth = 110;
	private static int pinHeight = 119;

	private static Map<String, Pair<Float, Float>> buildingLocationMap = new HashMap<String, Pair<Float, Float>>();
	private float locationScaleMultiplier = 1.5f;
	private Context context;

	private Bitmap map;
	private Bitmap pin;
	private Bitmap compass;

	private String eventId;
	private String[] locationArray;
	private static final float FACTOR = 0.69f;

	static {
		buildingLocationMap.put("C1", new Pair<Float, Float>(830f * FACTOR, 940f * FACTOR));
		buildingLocationMap.put("C2", new Pair<Float, Float>(700f * FACTOR, 940f * FACTOR));
		buildingLocationMap.put("C3", new Pair<Float, Float>(580f * FACTOR, 940f * FACTOR));
		buildingLocationMap.put("H1", new Pair<Float, Float>(690f * FACTOR, 725f * FACTOR));
		buildingLocationMap.put("H2", new Pair<Float, Float>(640f * FACTOR, 725f * FACTOR));
		buildingLocationMap.put("H3", new Pair<Float, Float>(595f * FACTOR, 725f * FACTOR));
		buildingLocationMap.put("H4", new Pair<Float, Float>(545f * FACTOR, 725f * FACTOR));
		buildingLocationMap.put("H5", new Pair<Float, Float>(495f * FACTOR, 725f * FACTOR));
		buildingLocationMap.put("H6", new Pair<Float, Float>(445f * FACTOR, 725f * FACTOR));
		buildingLocationMap.put("H7", new Pair<Float, Float>(395f * FACTOR, 725f * FACTOR));
		buildingLocationMap.put("H8", new Pair<Float, Float>(345f * FACTOR, 725f * FACTOR));
		buildingLocationMap.put("IA", new Pair<Float, Float>(760f * FACTOR, 705f * FACTOR));
		buildingLocationMap.put("IF", new Pair<Float, Float>(423f * FACTOR, 940f * FACTOR));
		buildingLocationMap.put("GD", new Pair<Float, Float>(423f * FACTOR, 980f * FACTOR));
		buildingLocationMap.put("SP", new Pair<Float, Float>(423f * FACTOR, 1020f * FACTOR));
		buildingLocationMap.put("PN", new Pair<Float, Float>(420f * FACTOR, 788f * FACTOR));
		buildingLocationMap.put("LS", new Pair<Float, Float>(300f * FACTOR, 1235f * FACTOR));
		buildingLocationMap.put("NO", new Pair<Float, Float>(415f * FACTOR, 875f * FACTOR));
		buildingLocationMap.put("RJ", new Pair<Float, Float>(838f * FACTOR, 865f * FACTOR));
		buildingLocationMap.put("JP", new Pair<Float, Float>(690f * FACTOR, 865f * FACTOR));
		buildingLocationMap.put("AU", new Pair<Float, Float>(423f * FACTOR, 1070f * FACTOR));
		buildingLocationMap.put("AS", new Pair<Float, Float>(1000f * FACTOR, 710f * FACTOR));
		buildingLocationMap.put("IO", new Pair<Float, Float>(770f * FACTOR, 550f * FACTOR));
		buildingLocationMap.put("TD", new Pair<Float, Float>(250f * FACTOR, 1040f * FACTOR));

		buildingLocationMap.put("AB", new Pair<Float, Float>(600f * FACTOR, 796f * FACTOR));
		buildingLocationMap.put("VN", new Pair<Float, Float>(687f * FACTOR, 786f * FACTOR));
		buildingLocationMap.put("TP", new Pair<Float, Float>(755f * FACTOR, 804f * FACTOR));

	}

	public BuildingMapBuilder(Context context, String locations) {
		locationArray = locations.split(",");
		this.context = context;
	}

	public Bitmap create() {
		Bitmap bitmap = null;
		try {
			int density = context.getResources().getDisplayMetrics().densityDpi;
			Log.d(this.getClass().getName(), "metrics densityDPI: " + density);
			Log.d("SCALE", ":"+locationScaleMultiplier);

			map = BitmapFactory.decodeResource(context.getResources(), R.drawable.map);
			pin = BitmapFactory.decodeResource(context.getResources(), R.drawable.map_pin);
			compass = BitmapFactory.decodeResource(context.getResources(), R.drawable.map_compass_full);
			Log.d(this.getClass().getName(), "map width: " + map.getWidth() + " height: " + map.getHeight());
			map = Bitmap.createScaledBitmap(map, mapWidth, mapHeight, false);
			pin = Bitmap.createScaledBitmap(pin, pinWidth, pinHeight, false);
			bitmap = Bitmap.createBitmap(mapWidth, mapHeight, Bitmap.Config.ARGB_8888);
			
			Rect destination = new Rect();
			destination.set(0, 0, mapWidth, mapHeight);
			
			Canvas c = new Canvas(bitmap);
			c.drawBitmap(map, null, destination, null);
			c.drawBitmap(compass, 30, 30, null);

			/* The rest of canvas-drawing code */
			c.drawRGB(239, 237, 225);
			c.drawBitmap(map, 0, 0, null);

			for (int i = 0; i < locationArray.length; i++) {
				Pair<Float, Float> positionPair = buildingLocationMap.get(locationArray[i]);
				if (positionPair != null) {
					Log.d(this.getClass().getName(), "LOCATION " + locationArray[i] + " -> "
							+ (positionPair.first - 40) * locationScaleMultiplier + ", " + (positionPair.second - 80)
							* locationScaleMultiplier);
					c.drawBitmap(pin, (positionPair.first - 40) * locationScaleMultiplier, (positionPair.second - 80)
							* locationScaleMultiplier, null);
				}
			}
		} catch (OutOfMemoryError e) {
			Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
		} finally {
			if (map != null) {
				map.recycle();
			}
			if (pin != null) {
				pin.recycle();
			}
			if (compass != null) {
				compass.recycle();
			}
		}
		return bitmap;
	}

	public String getEventId() {
		return this.eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String[] getLocations() {
		return this.locationArray;
	}

	public void setLocations(String[] locations) {
		this.locationArray = locations;
	}

	public int getActualMapWidth() {
		return map.getWidth();
	}

	public int getActualMapHeight() {
		return map.getHeight();
	}

	public void clearupMemory() {
		if (map != null)
			map.recycle();
		if (pin != null)
			pin.recycle();
		if (compass != null)
			compass.recycle();
	}

}
