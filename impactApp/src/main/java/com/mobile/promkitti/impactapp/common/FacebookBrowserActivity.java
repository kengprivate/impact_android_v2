package com.mobile.promkitti.impactapp.common;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;

public class FacebookBrowserActivity extends ImpactActivity {
	
	private WebView webView;
	private String url;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fb_webview);
		TextView title = (TextView)findViewById(R.id.fbBrowserActivityTitle);
		title.setText(getResources().getString(R.string.on_facebook));
		url = getIntent().getStringExtra("URL");
		
		display();
		webView.clearCache(false);
	}

	public void display() {
		Log.d("URL", "URL:"+url);
		webView = (WebView) findViewById(R.id.webView);
		
		WebSettings webSettings = webView.getSettings();
//		webSettings.setPluginsEnabled(true);
		webSettings.setJavaScriptEnabled(true);
//		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultZoom(ZoomDensity.MEDIUM);
//		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webSettings.setSupportMultipleWindows(true);
		webView.invokeZoomPicker();
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		webView.setWebViewClient(new WebViewClient() {  

			@Override  
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				view.loadUrl(url);
				return true;
			}


			@Override  
			public void onPageFinished(WebView view, String url)  
			{  
				if (webView!=null) {
					Log.d("PAGE","Finish loading "+webView.getUrl());
				}
			}

			public void onPageStarted(WebView view, String url, Bitmap favicon)
			{
				
			}
		});  

		webView.loadUrl(url);
	}
	
	public void stopButtonListener(View view) {
		webView.stopLoading();
		webView.clearCache(false);
		finish();
	}
	
	public void reloadButtonListener(View view) {
		webView.loadUrl(url);
	}
	
	public void homeButtonListener(View view) {
		webView.loadUrl(url);
	}
	

	public void backButtonListener(View view) {
		webView.goBack();
	}
	
	public void nextButtonListener(View view) {
		webView.goForward();
	}
	
	public void buttonListener(View view) {
		switch (view.getId()) {
	    	case R.id.fbBackButton:
	    		backButtonListener(view);
	    		break;
	    	case R.id.fbForwardButton:
	    		nextButtonListener(view);
	    		break;
	    	case R.id.fbHomeButton:
	    		homeButtonListener(view);
	    		break;
	    		
		}
	}
	
}
