package com.mobile.promkitti.impactapp.util;

import com.mobile.promkitti.impactapp.common.NotificationList;

public class NotificationUtil {

	private static NotificationList promotionNotificationList = new NotificationList();
	private static NotificationList eventNotificationList = new NotificationList();
	private static NotificationList adNotificationList = new NotificationList();

	public static NotificationList getPromotionNotificationList() {
		return promotionNotificationList;
	}

	public static NotificationList getAdNotificationList() {
		return adNotificationList;
	}

	public static NotificationList getEventNotificationList() {
		return eventNotificationList;
	}

}
