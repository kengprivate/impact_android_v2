package com.mobile.promkitti.impactapp.events.detail;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.mobile.promkitti.impactapp.R;

public class BuildingMapCanvas extends View {

	// These two constants specify the minimum and maximum zoom
	private static float MIN_ZOOM = 0.2f;
	private static float MAX_ZOOM = 1f;
	private static Map<String,Pair<Float, Float>> buildingLocationMap = new HashMap<String, Pair<Float,Float>>();
	private final float locationScaleMultiplier = 1.5f;  

	// These constants specify the mode that we're in
	private static int NONE = 0;
	private static int DRAG = 1;
	private static int ZOOM = 2;

	private int mode;
	private boolean dragged = false;
	private float displayWidth;
	private float displayHeight;

	// These two variables keep track of the X and Y coordinate of the finger
	// when it first
	// touches the screen
	private float startX = 0f;
	private float startY = 0f;

	// These two variables keep track of the amount we need to translate the
	// canvas along the X
	// and the Y coordinate
	private float translateX = 0f;
	private float translateY = 0f;

	// These two variables keep track of the amount we translated the X and Y
	// coordinates, the last time we
	// panned.
	private float previousTranslateX = 0f;
	private float previousTranslateY = 0f;

	private ScaleGestureDetector mScaleDetector;
	private float mScaleFactor = 1f;
	private Bitmap map;
	private Bitmap pin;
	private Bitmap compass;
	
	private String eventId;
	private String[] locationArray;

	static {
		buildingLocationMap.put("H1", new Pair<Float, Float>(690f, 710f));
		buildingLocationMap.put("H2", new Pair<Float, Float>(640f, 710f));
		buildingLocationMap.put("H3", new Pair<Float, Float>(590f, 710f));
		buildingLocationMap.put("H4", new Pair<Float, Float>(545f, 710f));
		buildingLocationMap.put("H5", new Pair<Float, Float>(495f, 710f));
		buildingLocationMap.put("H6", new Pair<Float, Float>(440f, 710f));
		buildingLocationMap.put("H7", new Pair<Float, Float>(390f, 710f));
		buildingLocationMap.put("H8", new Pair<Float, Float>(335f, 710f));
		buildingLocationMap.put("IA", new Pair<Float, Float>(760f, 690f));
		buildingLocationMap.put("C1", new Pair<Float, Float>(840f, 930f));
		buildingLocationMap.put("C2", new Pair<Float, Float>(700f, 930f));
		buildingLocationMap.put("C3", new Pair<Float, Float>(580f, 930f));
		buildingLocationMap.put("TD", new Pair<Float, Float>(335f, 710f));
	}
	
	private void init(Context context) {
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

//		displayWidth = display.getWidth();
//		displayHeight = display.getHeight();
		
		
		displayWidth = this.getWidth();
		displayHeight = this.getHeight();
		try {
			map = BitmapFactory.decodeResource(getResources(), R.drawable.map);
			int mapWidth = map.getWidth();
			int mapHeight = map.getHeight();
			float mapScaleFactorH = ((float)display.getHeight()/(float)mapHeight);
			float mapScaleFactorW = ((float)display.getWidth()/(float)mapWidth);
			mScaleFactor = mapScaleFactorH < mapScaleFactorW?mapScaleFactorH:mapScaleFactorW;
			MIN_ZOOM = mScaleFactor;
	
			pin = BitmapFactory.decodeResource(getResources(), R.drawable.map_pin);
	//		compass = BitmapFactory.decodeResource(getResources(), R.drawable.map_compass);
			mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
		} catch(OutOfMemoryError e) {
			Toast.makeText(context, "Out of memory.", Toast.LENGTH_SHORT).show();
		}
	}

	
	public BuildingMapCanvas(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public BuildingMapCanvas(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public BuildingMapCanvas(Context context) {
		super(context);
		init(context);
	}
	
	public String getEventId() {
		return this.eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	public String[] getLocations() {
		return this.locationArray;
	}

	public void setLocations(String[] locations) {
		this.locationArray = locations;
	}
	
	public int getActualMapWidth() {
		return map.getWidth();
	}

	public int getActualMapHeight() {
		return map.getHeight();
	}
	
	public void setScaleFactorActual() {
		this.mScaleFactor = 0.7f;
	}

	public void clearupMemory() {
		if(map!=null) map.recycle();
		if(pin!=null) pin.recycle();
		if(compass!=null) compass.recycle();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		switch (event.getAction() & MotionEvent.ACTION_MASK) {

		case MotionEvent.ACTION_DOWN:
			mode = DRAG;

			// We assign the current X and Y coordinate of the finger to startX
			// and startY minus the previously translated
			// amount for each coordinates This works even when we are
			// translating the first time because the initial
			// values for these two variables is zero.
			startX = event.getX() - previousTranslateX;
			startY = event.getY() - previousTranslateY;
			break;

		case MotionEvent.ACTION_MOVE:
			translateX = event.getX() - startX;
			translateY = event.getY() - startY;

			// We cannot use startX and startY directly because we have adjusted
			// their values using the previous translation values.
			// This is why we need to add those values to startX and startY so
			// that we can get the actual coordinates of the finger.
			double distance = Math.sqrt(Math.pow(event.getX() - (startX + previousTranslateX), 2) +
					Math.pow(event.getY() - (startY + previousTranslateY), 2)
					);

			if (distance > 0) {
				dragged = true;
			}

			break;

		case MotionEvent.ACTION_POINTER_DOWN:
			mode = ZOOM;
			break;

		case MotionEvent.ACTION_UP:
			mode = NONE;
			dragged = false;

			// All fingers went up, so let's save the value of translateX and
			// translateY into previousTranslateX and previousTranslate
			previousTranslateX = translateX;
			previousTranslateY = translateY;
			break;

		case MotionEvent.ACTION_POINTER_UP:
			mode = DRAG;

			// This is not strictly necessary; we save the value of translateX
			// and translateY into previousTranslateX
			// and previousTranslateY when the second finger goes up
			previousTranslateX = translateX;
			previousTranslateY = translateY;
			break;
		}

		mScaleDetector.onTouchEvent(event);

		// We redraw the canvas only in the following cases:
		//
		// o The mode is ZOOM
		// OR
		// o The mode is DRAG and the scale factor is not equal to 1 (meaning we
		// have zoomed) and dragged is
		// set to true (meaning the finger has actually moved)
		if ((mode == DRAG && mScaleFactor != 1f && dragged) || mode == ZOOM) {
			invalidate();
		}

		return true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		canvas.save();
		// We're going to scale the X and Y coordinates by the same amount
		canvas.scale(mScaleFactor, mScaleFactor);
//		canvas.scale(mScaleFactor, mScaleFactor, mScaleDetector.getFocusX(), mScaleDetector.getFocusY());

		// If translateX times -1 is lesser than zero, let's set it to zero.
		// This takes care of the left bound
		if ((translateX * -1) < 0) {
			translateX = 0;
		}

		// This is where we take care of the right bound. We compare translateX
		// times -1 to (scaleFactor - 1) * displayWidth.
		// If translateX is greater than that value, then we know that we've
		// gone over the bound. So we set the value of
		// translateX to (1 - scaleFactor) times the display width. Notice that
		// the terms are interchanged; it's the same
		// as doing -1 * (scaleFactor - 1) * displayWidth
		else if ((translateX * -1) > (mScaleFactor - 1) * displayWidth) {
			translateX = (1 - mScaleFactor) * displayWidth;
		}

		if (translateY * -1 < 0) {
			translateY = 0;
		}

		// We do the exact same thing for the bottom bound, except in this case
		// we use the height of the display
		else if ((translateY * -1) > (mScaleFactor - 1) * displayHeight) {
			translateY = (1 - mScaleFactor) * displayHeight;
		}

		// We need to divide by the scale factor here, otherwise we end up with
		// excessive panning based on our zoom level
		// because the translation amount also gets scaled according to how much
		// we've zoomed into the canvas.
		canvas.translate(translateX / mScaleFactor, translateY / mScaleFactor);

		/* The rest of your canvas-drawing code */
		canvas.drawRGB(255, 255, 255);
		canvas.drawBitmap(map, 0, 0, null);
		
		for (int i = 0; i < locationArray.length; i++) {
			Pair<Float,Float> positionPair = buildingLocationMap.get(locationArray[i]);
			canvas.drawBitmap(pin, (positionPair.first-40)*locationScaleMultiplier, (positionPair.second-80)*locationScaleMultiplier, null);
		}
//		map.recycle();
//		map = null;
//		pin.recycle();
//		pin = null;
		canvas.restore();
	}

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			mScaleFactor *= detector.getScaleFactor();

			// Don't let the object get too small or too large.
			mScaleFactor = Math.max(MIN_ZOOM, Math.min(mScaleFactor, MAX_ZOOM));

			invalidate();
			return true;
		}
	}
	
}
