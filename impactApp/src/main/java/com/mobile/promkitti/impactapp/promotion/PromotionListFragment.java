package com.mobile.promkitti.impactapp.promotion;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.AmazingListView;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.util.Utility;

public class PromotionListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

	private AmazingListView lsComposer;
	private PromotionSectionListAdapter adapter;
	private View v;
	private SwipeRefreshLayout swipeLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.promotion_listview, container, false);
		try {
			View view = v.findViewById(R.id.promotionListViewLayout);
			Utility.fixBackgroundRepeat(view);

			swipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
			swipeLayout.setOnRefreshListener(this);
			swipeLayout.setColorScheme(R.color.pulldown_refresh_color_1, R.color.pulldown_refresh_color_2, R.color.pulldown_refresh_color_3, R.color.pulldown_refresh_color_4);
			lsComposer = (AmazingListView) v.findViewById(R.id.lsComposer);
			refresh();

			LinearLayout overlay = (LinearLayout) v.findViewById(R.id.loadingOverlayLayout);
			overlay.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					return true;
				}
			});

		} catch (Exception e) {
			Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		refresh();
	}

	@Override
	public void onRefresh() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				swipeLayout.setRefreshing(false);
				DataController.getInstance().clearPromotionCache();
				refresh();
			}
		}, 2000);
	}

	public void refresh() {
		LinearLayout overlay = (LinearLayout) v.findViewById(R.id.loadingOverlayLayout);
		overlay.setVisibility(View.VISIBLE);
		new LoadingTask().execute();
	}

	private class LoadingTask extends AsyncTask<Void, Void, Integer> {
		/**
		 * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
		 */
		protected Integer doInBackground(Void... val) {
			try {
				DataController.getInstance().getPromotionList();

			} catch (Exception e) {
				Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
				cancel(false);
				AlertDialog.Builder builder = new AlertDialog.Builder(PromotionListFragment.this.getActivity());
				builder.setMessage(Constants.ERR_CONNECTION_ERROR).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						LinearLayout overlay = (LinearLayout) v.findViewById(R.id.loadingOverlayLayout);
						overlay.setVisibility(View.GONE);
						v.invalidate();
						v.refreshDrawableState();
					}
				});
				builder.create().show();
			}
			return 0;
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
		 */
		protected void onPostExecute(Integer i) {
			if (getActivity() != null) {
				adapter = new PromotionSectionListAdapter(getActivity());
				if (lsComposer != null) {
					lsComposer.setAdapter(adapter);
					lsComposer.setOnItemClickListener(adapter);
					lsComposer.setOnScrollListener(adapter);
					lsComposer.setPinnedHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_header, lsComposer, false)
					, LayoutInflater.from(getActivity()).inflate(R.layout.item_traffic_header, lsComposer, false));

					LinearLayout overlay = (LinearLayout) v.findViewById(R.id.loadingOverlayLayout);
					overlay.setVisibility(View.GONE);
				}
			}
		}
	}
}
