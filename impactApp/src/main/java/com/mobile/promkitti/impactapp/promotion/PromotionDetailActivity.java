package com.mobile.promkitti.impactapp.promotion;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.ActivityParameters;
import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.FragmentImpactActivity;
import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.MainActivity;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.common.ShareUtils;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.NotificationUtil;
import com.mobile.promkitti.impactapp.util.Utility;
import com.mobile.promkitti.impactapp.valueobjects.Promotion;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.mobile.promkitti.impactapp.Constants.SERVER_URL;

public class PromotionDetailActivity extends FragmentImpactActivity {

    private DataController dc = DataController.getInstance();
    private Promotion promotion;
    private String id;
    private boolean isDirect;
    private Context context;
    private int fullScreenWidth;
    private int targetWidth;
    private int targetHeight;
    private int paddingValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promotion_details);

        View view = findViewById(R.id.scrollViewLayout);
        Utility.fixBackgroundRepeat(view);
        Drawable d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.titlebar_bg);
        view.setPadding(0, d.getIntrinsicHeight() + Utility.convertDpToPx(this, 5), 0, 0);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        fullScreenWidth = dm.widthPixels;
        View borderLayout = findViewById(R.id.promotionDetailBorderLayout);
        paddingValue = Utility.convertDpToPx(this, 10);
        LinearLayout.LayoutParams borderLayoutParams = new LinearLayout.LayoutParams(fullScreenWidth - paddingValue, LayoutParams.MATCH_PARENT);
        borderLayout.setLayoutParams(borderLayoutParams);

        Drawable placeHolder = ContextCompat.getDrawable(getApplicationContext(), R.drawable.promo_detail_placeholder);
        targetWidth = placeHolder.getIntrinsicWidth();
        targetHeight = placeHolder.getIntrinsicHeight();

        paddingValue = Utility.convertDpToPx(this, 26);
        int height = (int) ((float) targetHeight / (float) targetWidth * (float) (fullScreenWidth - paddingValue));
        FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(fullScreenWidth - paddingValue, height);
        frameLayoutParams.gravity = Gravity.CENTER;

        android.support.v4.view.ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setLayoutParams(new FrameLayout.LayoutParams(fullScreenWidth - paddingValue, height));

        context = this;
        id = getIntent().getStringExtra("ID");
        isDirect = getIntent().getBooleanExtra(ActivityParameters.DIRECT, false);
        if (isDirect) {
            Log.d("FLURRY", FlurryConstants.PUSH_OPEN_PROMOTION_ID);
            Map<String, String> params = new HashMap<String, String>();
            params.put(FlurryConstants.PARAM_PROMO_ID, id);
            FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_PROMOTION_ID, params);
            FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_PROMOTION_ID + Constants.SPACE + id);
        }

        // Clear badge
        NotificationUtil.getPromotionNotificationList().removeNotification(id);

        Log.d("promotion ID:", id);
        FlurryUtil.onPageView();
        FlurryUtil.logEvent(FlurryConstants.PAGE_PROMO_DETAIL);
        new LoadingTask().execute();
    }

    public void sharePromotionButtonListener(View view) {
        String contentURL = "http://www.impact.co.th";
        String imageURL = SERVER_URL + "/pl/" + promotion.getId() + ".png";
        ArrayList<EventMedia> mediaList = promotion.getMediaList();
        for(EventMedia media : mediaList) {
            if("i".equalsIgnoreCase(media.getType())) {
                imageURL = media.getUrl();
                break;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append(promotion.getDescription());
        sb.append("\r\n\r\n");

        Intent chooserIntent = ShareUtils.share(this, view, sb.toString(), promotion.getTitle(), contentURL, imageURL);
        if(chooserIntent == null) {
            return;
        }

        Map<String, String> flurryParams = new HashMap<String, String>();
        flurryParams.put(FlurryConstants.PARAM_PROMO_ID, id);
        FlurryUtil.logEvent(FlurryConstants.SHARE_PROMO, flurryParams);
        FlurryUtil.logEvent(FlurryConstants.SHARE_PROMO + Constants.SPACE + id);

        startActivity(chooserIntent);
    }

    @Override
    public void onBackPressed() {
        if (isDirect) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private class LoadingTask extends AsyncTask<Void, Void, Integer> {
        /**
         * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
         */
        protected Integer doInBackground(Void... val) {
            try {
                dc.getPromotionDetail(id);
            } catch (ImpactException e) {
                Log.e(getClass().getName(), e.getMessage());
                return -1;
            }
            return 0;
        }

        /**
         * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
         */
        protected void onPostExecute(Integer i) {
            try {
                if (i < 0) {
                    throw new ImpactException("Loading error");
                }

                promotion = dc.getPromotionDetail(id);

            } catch (ImpactException e) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(Constants.ERR_CONNECTION_ERROR).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
                builder.create().show();
            }

            if (promotion == null) {
                finish();
                return;
            }

            ImageView placeholderView = (ImageView) findViewById(R.id.promotionDetailImagePlaceholder);
            placeholderView.setVisibility(View.GONE);

            // Set the pager with an adapter
            ArrayList<EventMedia> mediaList = promotion.getMediaList();
            PromotionPagerAdapter pagerAdapter = new PromotionPagerAdapter(getSupportFragmentManager(), mediaList);
            final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
            viewPager.setAdapter(pagerAdapter);

            // Bind the title indicator to the adapter
            CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
            pageIndicator.setViewPager(viewPager);

            if (mediaList.size() < 2) {
                pageIndicator.setVisibility(View.GONE);
            }

            TextView titleText = (TextView) findViewById(R.id.promotionDetailTitleText);
            titleText.setText(promotion.getTitle());
            registerForContextMenu(titleText);

            TextView descText = (TextView) findViewById(R.id.promotionDetailDescriptionText);
            descText.setText(promotion.getDescription());
            registerForContextMenu(descText);

            LinearLayout overlay = (LinearLayout) findViewById(R.id.loadingOverlayLayout);
            overlay.setVisibility(View.GONE);
        }

    }

}
