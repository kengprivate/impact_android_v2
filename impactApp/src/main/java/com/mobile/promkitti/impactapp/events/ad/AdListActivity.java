package com.mobile.promkitti.impactapp.events.ad;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.AmazingListView;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.Utility;

public class AdListActivity extends ImpactActivity implements SwipeRefreshLayout.OnRefreshListener {

	private DataController dc = DataController.getInstance();
	private AmazingListView lsComposer;
	private AdSectionListAdapter adapter;
	private SwipeRefreshLayout swipeLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ad_listview);
		try {
			View view = findViewById(R.id.adListViewLayout);
			Utility.fixBackgroundRepeat(view);

			swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
			swipeLayout.setOnRefreshListener(this);
			swipeLayout.setColorScheme(R.color.pulldown_refresh_color_1, R.color.pulldown_refresh_color_2, R.color.pulldown_refresh_color_3, R.color.pulldown_refresh_color_4);

			lsComposer = (AmazingListView) findViewById(R.id.lsComposer);
			FlurryUtil.onPageView();
			FlurryUtil.logEvent(FlurryConstants.PAGE_AD_LIST);
			refresh();

		} catch (Exception e) {
			Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
		}
	}

	@Override
	public void onRefresh() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				swipeLayout.setRefreshing(false);
				DataController.getInstance().clearAdCache();
				refresh();
			}
		}, 2000);
	}

	public void refresh() {
		new LoadingTask().execute();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}

	public void buttonListener(View view) {
		switch (view.getId()) {
		case R.id.listRefreshButton:
			Log.d("", "Refresh()");
			dc.clearAdCache();
			Intent intent = getIntent();
			overridePendingTransition(0, 0);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			finish();
			overridePendingTransition(0, 0);
			startActivity(intent);
			break;
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	private class LoadingTask extends AsyncTask<Void, Void, Integer> {
		/**
		 * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
		 */

		protected Integer doInBackground(Void... val) {
			// Initial load of Event list
			String currDateString = dateFormat.format(new Date(System.currentTimeMillis()));
			try {
				dc.getAdList(currDateString);
			} catch (Exception e) {
				Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
			}
			return 0;
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
		 */
		protected void onPostExecute(Integer i) {
			adapter = new AdSectionListAdapter(getApplicationContext());
			if (lsComposer != null) {
				lsComposer.setAdapter(adapter);
				lsComposer.setOnItemClickListener(adapter);
				lsComposer.setOnScrollListener(adapter);
				lsComposer.setPinnedHeaderView(getLayoutInflater().inflate(R.layout.item_event_header, lsComposer, false),
						getLayoutInflater().inflate(R.layout.item_traffic_header, lsComposer, false));

				LinearLayout overlay = (LinearLayout) findViewById(R.id.loadingOverlayLayout);
				overlay.setVisibility(View.GONE);
			}
		}
	}
}
