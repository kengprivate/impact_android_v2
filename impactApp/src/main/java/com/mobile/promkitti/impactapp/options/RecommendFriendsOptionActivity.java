package com.mobile.promkitti.impactapp.options;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.ShareUtils;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.Utility;

import java.util.Locale;

public class RecommendFriendsOptionActivity extends ImpactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        refresh();
    }

    public void emailFriendsButtonListener(View view) {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out IMPACT app on Play Store");
        emailIntent.setType("plain/text");
        FlurryUtil.logEvent(FlurryConstants.RF_EMAIL);
        String language = Locale.getDefault().getLanguage();
        if (Constants.LANGUAGE_THAI.equals(language)) {
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "ลอง IMPACT app จาก Play Store ได้ที่ https://play.google.com/store/apps/details?id=com.mobile.promkitti.impactapp");
        } else {
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out IMPACT app from Playstore here https://play.google.com/store/apps/details?id=com.mobile.promkitti.impactapp");
        }

        startActivity(emailIntent);
    }

    public void share(View view) {
        String shareBody = null;
        String caption = "IMPACT Muang Thong Thani";
        String language = Locale.getDefault().getLanguage();
        if (Constants.LANGUAGE_THAI.equals(language)) {
            shareBody = "ตรวจสอบ วัน เวลา และสถานที่ ของการจัดงานต่างๆ ในเวลาปัจจุบันและอนาคตได้อย่างง่ายดาย พร้อมทั้งโปรโมชั่น การแชร์ผ่าน Facebook และอื่นๆ อีกมากมาย" +
                    "\r\n\r\nลอง IMPACT app จาก Play Store ได้ที่ https://play.google.com/store/apps/details?id=com.mobile.promkitti.impactapp";
        } else {
            shareBody = "Conveniently get today and upcoming event information, promotions, Facebook sharing and a lot more." +
                    "\r\n\r\nCheck out IMPACT app from Playstore here https://play.google.com/store/apps/details?id=com.mobile.promkitti.impactapp";
        }

        String contentURL = "http://iphoneapp.impact.co.th/i/r.php?u=https://play.google.com/store/apps/details?id=com.mobile.promkitti.impactapp";
//		String imageURL = "http://iphoneapp.impact.co.th/i/admin/img/logo_200x200.png";
        String imageURL = "http://iphoneapp.impact.co.th/i/img/logo_200x380.png";
        Intent chooserIntent = ShareUtils.share(this, view, shareBody, caption, contentURL, imageURL);
        if(chooserIntent == null) {
            return;
        }

        FlurryUtil.logEvent(FlurryConstants.RF_SHARE_TO_FRIENDS);
        startActivity(chooserIntent);
    }

    public void shareToFriendsButtonListener(View view) {
        share(view);
    }

    public void rateAppButtonListener(View view) {
        Uri uri = Uri.parse("market://details?id=com.mobile.promkitti.impactapp");
        Intent i = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(i);
        FlurryUtil.logEvent(FlurryConstants.RF_RATE_IMPACT_APP);
    }

    public void reminderButtonListener(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RecommendFriendsOptionActivity.this);

        // set title
        alertDialogBuilder.setTitle("Rate This App");

        // set dialog message
        alertDialogBuilder
                .setMessage("If you enjoy the app, please take a short moment to rate it.")
                .setCancelable(false)
                .setPositiveButton("Rate This App", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        Toast.makeText(getApplicationContext(), "Rate this app", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNeutralButton("Remind Me Later", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        Toast.makeText(getApplicationContext(), "Remind me later", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("No, Thanks", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        Toast.makeText(getApplicationContext(), "No, Thanks", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private void refresh() {
        setContentView(R.layout.recommend_friends_options);
        View view = findViewById(R.id.recommendFriendsOptionLayout);
        Utility.fixBackgroundRepeat(view);
    }

}
