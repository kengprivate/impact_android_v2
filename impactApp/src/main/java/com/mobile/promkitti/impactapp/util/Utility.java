package com.mobile.promkitti.impactapp.util;

import android.content.Context;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;

public class Utility {
	

	public static boolean isBlank(String input) {
		return (input==null || input.length()==0);
	}
	
	public static String unEscapeHtml(String input) {
		return input.replace("&quot;", "\"");
	}
	
	public static void fixBackgroundRepeat(View view) {
	      Drawable bg = view.getBackground();
	      if(bg != null) {
	           if(bg instanceof BitmapDrawable) {
	                BitmapDrawable bmp = (BitmapDrawable) bg;
	                bmp.mutate(); // make sure that we aren't sharing state anymore
	                bmp.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
	           }
	      }
	 }
	
	public static int convertDpToPx(Context context, int dip) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, context.getResources().getDisplayMetrics());
	}
	
	
//	public static void addTextToClipboard(Activity activity, ContextMenu menu, View v, ContextMenuInfo menuInfo) {
//		//user has long pressed your TextView
//	    menu.add(0, v.getId(), 0, "text that you want to show in the context menu - I use simply Copy");
//
//	    //cast the received View to TextView so that you can get its text
//	    TextView textView = (TextView) v;
//
//	    //place your TextView's text in clipboard
//	    ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE); 
//	    clipboard.setText(textView.getText());
////	    ClipData clip = ClipData.newPlainText("label", "Text to copy");
////	    clipboard.setPrimaryClip(clip);
//	}

}
