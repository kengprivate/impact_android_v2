package com.mobile.promkitti.impactapp.util;


public abstract class Callback {

    public abstract void execute();
    
}
