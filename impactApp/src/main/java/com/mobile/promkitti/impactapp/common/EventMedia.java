package com.mobile.promkitti.impactapp.common;

import android.os.Parcel;
import android.os.Parcelable;

public abstract class EventMedia implements Parcelable {

	private String id; // Event ID
	private String url;
	private String type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(new String[] { this.id, this.url, this.type });
	}
}
