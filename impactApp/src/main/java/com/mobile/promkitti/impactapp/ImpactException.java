package com.mobile.promkitti.impactapp;

public class ImpactException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ImpactException(){
        super();
    }

    public ImpactException(String s){
        super(s);
    }

    public ImpactException(String s, Throwable t){
        super(s, t);
    }
    
    @Override
    public String getMessage() {
    	String msg = super.getMessage();
    	return msg==null?"":msg;
    }
}
