package com.mobile.promkitti.impactapp.options;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.MainActivity;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.Utility;

import java.util.Locale;

import static com.mobile.promkitti.impactapp.R.id.languageEnglishButton;

public class LanguageOptionActivity extends ImpactActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		refresh();
		// Load from sharedPreference. Default is Thai.
		SharedPreferences mPrefs = getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);
		String language = mPrefs.getString("language", Constants.LANGUAGE_THAI);
		Log.i("load language", language);
		if(Constants.LANGUAGE_ENGLISH.equals(language)) {
			runOnUiThread(new Runnable()
	        {
	            @Override
	            public void run()
	            {
	            	findViewById(R.id.languageEnglishChecker).setVisibility(View.VISIBLE);
					findViewById(R.id.languageThaiChecker).setVisibility(View.INVISIBLE);
	            }
	        });
		} else {
			runOnUiThread(new Runnable()
	        {
	            @Override
	            public void run()
	            {
					findViewById(R.id.languageEnglishChecker).setVisibility(View.INVISIBLE);
	            	findViewById(R.id.languageThaiChecker).setVisibility(View.VISIBLE);
	            }
	        });
		}
		
        FlurryUtil.logEvent(FlurryConstants.PAGE_LANGUAGE_OPTION);
		FlurryUtil.onPageView();

	}

	
	public void buttonListener(View view) {
		DataController dc = DataController.getInstance();
		String currentLanguage = dc.getLanguage();
		Locale locale = Locale.getDefault();
		Log.i("current language", currentLanguage);
		switch (view.getId()) {
	    	case R.id.languageThaiButton:
				runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						findViewById(R.id.languageEnglishChecker).setVisibility(View.INVISIBLE);
						findViewById(R.id.languageThaiChecker).setVisibility(View.VISIBLE);
					}
				});
					locale = new Locale(Constants.LANGUAGE_THAI);
					changeLanguage(locale);
					dc.clearCache();
					dc.clearCalendarCache();
					dc.clearAdCache();
					dc.clearPromotionCache();
					MainActivity.setEventTabLoaded(false);
					MainActivity.setCalendarTabLoaded(false);
					MainActivity.setSocialTabLoaded(false);
					MainActivity.setPromotionTabLoaded(false);
				if(!Constants.LANGUAGE_THAI.equals(currentLanguage)) {
		            FlurryUtil.logEvent(FlurryConstants.LANG_SWITCH_TO_THAI);
	    		}
	    		break;
	    		
	    	case languageEnglishButton:
				runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						findViewById(R.id.languageEnglishChecker).setVisibility(View.VISIBLE);
						findViewById(R.id.languageThaiChecker).setVisibility(View.INVISIBLE);
					}
				});
		            locale = new Locale(Constants.LANGUAGE_ENGLISH);
		            changeLanguage(locale);
		            dc.clearCache();
		            dc.clearCalendarCache();
		            dc.clearAdCache();
		            dc.clearPromotionCache();
		            MainActivity.setEventTabLoaded(false);
		            MainActivity.setCalendarTabLoaded(false);
		            MainActivity.setSocialTabLoaded(false);
		            MainActivity.setPromotionTabLoaded(false);
				if(!Constants.LANGUAGE_ENGLISH.equals(currentLanguage)) {
		            FlurryUtil.logEvent(FlurryConstants.LANG_SWITCH_TO_ENG);
	    		}
	    		break;
		}
	}
	
	
	private void changeLanguage(Locale locale) {
		Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        
		SharedPreferences mPrefs = getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("language", locale.getLanguage());
        Log.i("change language to", locale.getLanguage());
        editor.commit();

        DataController dc = DataController.getInstance();
        dc.setLanguage(locale.getLanguage());
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	private void refresh() {
		setContentView(R.layout.language_options);
		
//		View view = findViewById(R.id.titlebarLayout);
//		Utility.fixBackgroundRepeat(view);
		View view = findViewById(R.id.languageOptionLayout);
		Utility.fixBackgroundRepeat(view);
	}

}
