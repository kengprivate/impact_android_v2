/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobile.promkitti.impactapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.common.NotificationList;
import com.mobile.promkitti.impactapp.events.ad.AdDetailActivity;
import com.mobile.promkitti.impactapp.events.calendar.CalendarFragment;
import com.mobile.promkitti.impactapp.events.detail.EventDetailsActivity;
import com.mobile.promkitti.impactapp.events.list.EventListFragment;
import com.mobile.promkitti.impactapp.fb.SocialSelectorViewFragment;
import com.mobile.promkitti.impactapp.options.MoreOptionFragment;
import com.mobile.promkitti.impactapp.promotion.PromotionDetailActivity;
import com.mobile.promkitti.impactapp.promotion.PromotionListFragment;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.NotificationUtil;
import com.mobile.promkitti.impactapp.util.Utility;
import com.tjeannin.apprate.AppRate;

import java.util.Locale;

/**
 * Demonstration of hiding and showing fragments.
 */
public class MainActivity extends FragmentActivity {

	private FragmentManager fm = getSupportFragmentManager();
	private Context context;
	private EventListFragment eventList;
	private CalendarFragment calendar;
	private SocialSelectorViewFragment socialSelectorView;
	private PromotionListFragment promotionList;
	private MoreOptionFragment moreOption;

	private static int currentTab = 0;
	private String currentLanguage = "";

	private ImageView eventTabButton;
	private ImageView calendarTabButton;
	private ImageView socialTabButton;
	private ImageView promotionTabButton;
	private ImageView moreTabButton;

	private ImageView eventTabBg;
	private ImageView calendarTabBg;
	private ImageView socialTabBg;
	private ImageView promotionTabBg;
	private ImageView moreTabBg;

	private static boolean isEventTabLoaded = false;
	private static boolean isCalendarTabLoaded = false;
	private static boolean isSocialTabLoaded = false;
	private static boolean isPromotionTabLoaded = false;

	private static DataController dc = DataController.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Constants.setActive(true);
		setDefaultLanguage();
		isEventTabLoaded = false;
		isCalendarTabLoaded = false;
		isSocialTabLoaded = false;
		isPromotionTabLoaded = false;
		context = this;

		initAppRateReminder();
		loadSavedNotificationValues();

		// Lazy approach to prevent NetworkOnMainThreadException
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		FlurryUtil.onPageView();
		FlurryUtil.logEvent(FlurryConstants.PAGE_EVENT_LIST);
		refresh();
	}

	private void loadSavedNotificationValues() {
		loadSpecifiedNotificationValues("promotionIdList", NotificationUtil.getPromotionNotificationList());
		loadSpecifiedNotificationValues("adIdList", NotificationUtil.getAdNotificationList());
		loadSpecifiedNotificationValues("eventIdList", NotificationUtil.getEventNotificationList());
	}

	private void loadSpecifiedNotificationValues(String name, NotificationList list) {
		if (list.size() == 0) {
			SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);
			String idListString = settings.getString(name, "");
			String[] ids = idListString.split(",");
			for (String id : ids) {
				if (!Utility.isBlank(id)) {
					list.addNotification(id);

				}
			}
		}
	}

	private void saveNotificationValues() {
		saveSpecifiedNotificationValues("promotionIdList", NotificationUtil.getPromotionNotificationList());
		saveSpecifiedNotificationValues("adIdList", NotificationUtil.getAdNotificationList());
		saveSpecifiedNotificationValues("eventIdList", NotificationUtil.getEventNotificationList());
	}

	private void saveSpecifiedNotificationValues(String name, NotificationList list) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
			if (i < list.size() - 1) {
				sb.append(",");
			}
		}
		SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, sb.toString());

		// Commit the edits!
		editor.commit();
	}

	private void passThroughPromotion(String promotionId) {
		currentTab = 3;
		refresh();
		Intent intent = new Intent(this, PromotionDetailActivity.class);
		intent.putExtra("ID", promotionId);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void passThroughAd(String adId) {
		currentTab = 1;
		refresh();
		Intent intent = new Intent(this, AdDetailActivity.class);
		intent.putExtra("ID", adId);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void passThroughEvent(String eventId) {
		currentTab = 1;
		refresh();
		Intent intent = new Intent(this, EventDetailsActivity.class);
		intent.putExtra("ID", eventId);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void initAppRateReminder() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle("Rate IMPACT")
				.setMessage("If you enjoy using IMPACT, would you mind taking a moment to rate it? It won't take more than a minute. Thanks for your support!").setPositiveButton("Rate IMPACT", null)
				.setNegativeButton("No, thanks", null).setNeutralButton("Remind me later", null);

		new AppRate(this).setCustomDialog(builder).setMinDaysUntilPrompt(Constants.APP_RATE_DAYS_UNTIL_PROMPT).setMinLaunchesUntilPrompt(Constants.APP_RATE_LAUNCHES_UNTIL_PROMPT).init();
	}

	private void setDefaultLanguage() {
		Locale locale = null;
		SharedPreferences mPrefs = getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);

		if (mPrefs.contains("language")) {
			locale = new Locale(mPrefs.getString("language", Constants.LANGUAGE_DEFAULT));
		} else {
			locale = new Locale(Constants.LANGUAGE_DEFAULT);
		}
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

		DataController dc = DataController.getInstance();
		dc.setLanguage(locale.getLanguage());

//		Log.w("load mainactivity:", "language: "+ locale.getLanguage());
	}

	public void tabListener(View view) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		Fragment eventTab = fm.findFragmentById(R.id.eventTab);
		Fragment calendarTab = fm.findFragmentById(R.id.calendarTab);
		Fragment socialTab = fm.findFragmentById(R.id.socialTab);
		Fragment promotionTab = fm.findFragmentById(R.id.promotionTab);
		Fragment moreTab = fm.findFragmentById(R.id.moreTab);

		switch (view.getId()) {
		case R.id.eventTabLayout:
			ft.show(eventTab);
			ft.hide(calendarTab);
			ft.hide(socialTab);
			ft.hide(promotionTab);
			ft.hide(moreTab);
			selectEventTab();
			if (currentTab != 0) {
				FlurryUtil.onPageView();
				FlurryUtil.logEvent(FlurryConstants.PAGE_EVENT_LIST);
			}
			currentTab = 0;
			break;

		case R.id.calendarTabLayout:
			ft.hide(eventTab);
			ft.show(calendarTab);
			ft.hide(socialTab);
			ft.hide(promotionTab);
			ft.hide(moreTab);
			selectCalendarTab();
			if (currentTab != 1) {
				FlurryUtil.onPageView();
				FlurryUtil.logEvent(FlurryConstants.PAGE_CALENDAR);
			}
			currentTab = 1;
			break;

		case R.id.socialTabLayout:
			ft.hide(eventTab);
			ft.hide(calendarTab);
			ft.show(socialTab);
			ft.hide(promotionTab);
			ft.hide(moreTab);
			selectSocialTab();
			if (currentTab != 2) {
				FlurryUtil.onPageView();
				FlurryUtil.logEvent(FlurryConstants.PAGE_FB_FAN_PAGE);
			}
			currentTab = 2;
			break;

		case R.id.promotionTabLayout:
			ft.hide(eventTab);
			ft.hide(calendarTab);
			ft.hide(socialTab);
			ft.show(promotionTab);
			ft.hide(moreTab);
			selectPromotionTab();
			if (currentTab != 3) {
				FlurryUtil.onPageView();
				FlurryUtil.logEvent(FlurryConstants.PAGE_PROMO_LIST);
			}
			currentTab = 3;
			break;

		case R.id.moreTabLayout:
			ft.hide(eventTab);
			ft.hide(calendarTab);
			ft.hide(socialTab);
			ft.hide(promotionTab);
			ft.show(moreTab);
			selectMoreTab();
			if (currentTab != 4) {
				FlurryUtil.onPageView();
				FlurryUtil.logEvent(FlurryConstants.PAGE_MORE);
			}
			currentTab = 4;
			break;

		default:
			break;
		}
		ft.commit();
		refreshTabContent(currentTab);
	}

	@Override
	public void onBackPressed() {
		boolean isEventListVisible = eventList.isVisible();
		boolean isCalendarVisible = calendar.isVisible();
		boolean isSocialWebviewVisible = socialSelectorView.isVisible();
		boolean isPromotionListVisible = promotionList.isVisible();
		boolean isMoreOptionVisible = moreOption.isVisible();
		if (isEventListVisible || isCalendarVisible || isSocialWebviewVisible || isPromotionListVisible || isMoreOptionVisible) {
			currentTab = 0;
			finish();
		} else {
			super.onBackPressed();
		}
	}

	public void buttonListener(View view) {
		switch (view.getId()) {
		case R.id.languageButton:
			moreOption.languageButtonListener(view);
			break;
		case R.id.feedbackButton:
			moreOption.feedBackButtonListener(view);
			break;
		case R.id.websiteButton:
			moreOption.websiteButtonListener(view);
			break;
		case R.id.mapsButton:
			moreOption.mapsButtonListener(view);
			break;
		case R.id.facebookLoginButton:
			moreOption.facebookLoginButtonListener(view);
			break;
		case R.id.recommendFriendsButton:
			moreOption.recommendFriendsButtonListener(view);
			break;
		case R.id.listRefreshButton:
			Log.d("", "Refresh()");
			dc.clearCalendarCache();
			eventList.refresh();
			break;
		case R.id.todayDateButton:
			calendar.refresh();
			break;
		// case R.id.calendarRefreshButton:
		// dc.clearCalendarCache();
		// calendar.refresh();
		// break;
		// case R.id.promotionListRefreshButton:
		// Log.d("", "Refresh()");
		// dc.clearPromotionCache();
		// promotionList.refresh();
		// break;
		case R.id.fbSelectorButton:
			socialSelectorView.fbButtonListener(view);
			break;
		case R.id.igSelectorButton:
			socialSelectorView.igButtonListener(view);
			break;

		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		Constants.setActive(true);
		FlurryUtil.onStartSession(this);
		refreshTabContent(currentTab);
		Log.d(getClass().getName(), "onStart : " + getLocalClassName());
	}

	@Override
	protected void onResume() {
		super.onResume();
		Constants.setActive(true);
		refresh();

		TextView ptv = (TextView) findViewById(R.id.promotionNotificationCount);
		if (NotificationUtil.getPromotionNotificationList().size() > 0 && NotificationUtil.getPromotionNotificationList().hasNew()) {
			ptv.setText(Integer.toString(NotificationUtil.getPromotionNotificationList().size()));
			ptv.setVisibility(View.VISIBLE);
		} else {
			ptv.setText("0");
			ptv.setVisibility(View.GONE);
		}

		TextView etv = (TextView) findViewById(R.id.eventNotificationCount);
		if (NotificationUtil.getEventNotificationList().size() > 0 && NotificationUtil.getEventNotificationList().hasNew()) {
			etv.setText(Integer.toString(NotificationUtil.getEventNotificationList().size()));
			etv.setVisibility(View.VISIBLE);
		} else {
			etv.setText("0");
			etv.setVisibility(View.GONE);
		}

		Log.d(getClass().getName(), "onResume : " + getLocalClassName());
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryUtil.onEndSession(this);
		saveNotificationValues();
		Log.d(getClass().getName(), "onStop : " + getLocalClassName());
	}

	@Override
	protected void onPause() {
		super.onPause();
		Constants.setActive(false);
		Log.d(getClass().getName(), "onPause : " + getLocalClassName());
	}

	private void refresh() {
		Locale locale = Locale.getDefault();
		String language = locale.getDisplayLanguage();
		if (!currentLanguage.equals(locale.getDisplayLanguage())) {
			currentLanguage = language;
			setContentView(R.layout.main);

			eventTabButton = (ImageView) findViewById(R.id.eventTabButton);
			calendarTabButton = (ImageView) findViewById(R.id.calendarTabButton);
			socialTabButton = (ImageView) findViewById(R.id.socialTabButton);
			promotionTabButton = (ImageView) findViewById(R.id.promotionTabButton);
			moreTabButton = (ImageView) findViewById(R.id.moreTabButton);

			eventTabBg = (ImageView) findViewById(R.id.eventTabBg);
			calendarTabBg = (ImageView) findViewById(R.id.calendarTabBg);
			socialTabBg = (ImageView) findViewById(R.id.socialTabBg);
			promotionTabBg = (ImageView) findViewById(R.id.promotionTabBg);
			moreTabBg = (ImageView) findViewById(R.id.moreTabBg);

			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			eventList = new EventListFragment();
			calendar = new CalendarFragment();
			socialSelectorView = new SocialSelectorViewFragment();
			promotionList = new PromotionListFragment();
			moreOption = new MoreOptionFragment();

			ft.add(R.id.moreTab, moreOption);
			ft.add(R.id.promotionTab, promotionList);
			ft.add(R.id.socialTab, socialSelectorView);
			ft.add(R.id.calendarTab, calendar);
			ft.add(R.id.eventTab, eventList);

			switch (currentTab) {
			case 0:
				ft.show(eventList);
				ft.hide(calendar);
				ft.hide(socialSelectorView);
				ft.hide(promotionList);
				ft.hide(moreOption);
				selectEventTab();
				break;
			case 1:
				ft.hide(eventList);
				ft.show(calendar);
				ft.hide(socialSelectorView);
				ft.hide(promotionList);
				ft.hide(moreOption);
				selectCalendarTab();
				break;
			case 2:
				ft.hide(eventList);
				ft.hide(calendar);
				ft.show(socialSelectorView);
				ft.hide(promotionList);
				ft.hide(moreOption);
				selectSocialTab();
				break;
			case 3:
				ft.hide(eventList);
				ft.hide(calendar);
				ft.hide(socialSelectorView);
				ft.show(promotionList);
				ft.hide(moreOption);
				selectPromotionTab();
				break;
			case 4:
				ft.hide(eventList);
				ft.hide(calendar);
				ft.hide(socialSelectorView);
				ft.hide(promotionList);
				ft.show(moreOption);
				selectMoreTab();
				break;
			default:
				ft.show(eventList);
				ft.hide(calendar);
				ft.hide(socialSelectorView);
				ft.hide(promotionList);
				ft.hide(moreOption);
				selectEventTab();
				break;
			}
			ft.commit();
		}
	}

	private void refreshTabContent(int selectedTab) {
		switch (selectedTab) {
		case 0:
			if (!isEventTabLoaded) {
				eventList.refresh();
				isEventTabLoaded = true;
			}
			break;

		case 1:
			if (!isCalendarTabLoaded) {
				calendar.refresh();
				isCalendarTabLoaded = true;
			}
			break;

		case 2:
			if (!isSocialTabLoaded) {
				isSocialTabLoaded = true;
			}
			break;

		case 3:
			if (!isPromotionTabLoaded) {
				promotionList.refresh();
				isPromotionTabLoaded = true;
			}
			break;
		}
	}

	private void selectEventTab() {
		eventTabButton.setImageResource(R.drawable.tabbar_eventlist);
		calendarTabButton.setImageResource(R.drawable.tabbar_calendar);
		socialTabButton.setImageResource(R.drawable.tabbar_social);
		promotionTabButton.setImageResource(R.drawable.tabbar_promotion);
		moreTabButton.setImageResource(R.drawable.tabbar_more);
		eventTabBg.setImageResource(R.drawable.tabbar_bg_h2);
		calendarTabBg.setImageResource(R.drawable.tabbar_bg_n);
		socialTabBg.setImageResource(R.drawable.tabbar_bg_n);
		promotionTabBg.setImageResource(R.drawable.tabbar_bg_n);
		moreTabBg.setImageResource(R.drawable.tabbar_bg_n);
		eventList.startAdBannerRotation();
	}

	private void selectCalendarTab() {
		eventTabButton.setImageResource(R.drawable.tabbar_eventlist);
		calendarTabButton.setImageResource(R.drawable.tabbar_calendar);
		socialTabButton.setImageResource(R.drawable.tabbar_social);
		promotionTabButton.setImageResource(R.drawable.tabbar_promotion);
		moreTabButton.setImageResource(R.drawable.tabbar_more);
		eventTabBg.setImageResource(R.drawable.tabbar_bg_n);
		calendarTabBg.setImageResource(R.drawable.tabbar_bg_h2);
		socialTabBg.setImageResource(R.drawable.tabbar_bg_n);
		promotionTabBg.setImageResource(R.drawable.tabbar_bg_n);
		moreTabBg.setImageResource(R.drawable.tabbar_bg_n);
		if (!isCalendarTabLoaded) {
			calendar.refresh();
		}
		eventList.stopAdBannerRotation();
	}

	private void selectSocialTab() {
		eventTabButton.setImageResource(R.drawable.tabbar_eventlist);
		calendarTabButton.setImageResource(R.drawable.tabbar_calendar);
		socialTabButton.setImageResource(R.drawable.tabbar_social);
		promotionTabButton.setImageResource(R.drawable.tabbar_promotion);
		moreTabButton.setImageResource(R.drawable.tabbar_more);
		eventTabBg.setImageResource(R.drawable.tabbar_bg_n);
		calendarTabBg.setImageResource(R.drawable.tabbar_bg_n);
		socialTabBg.setImageResource(R.drawable.tabbar_bg_h2);
		promotionTabBg.setImageResource(R.drawable.tabbar_bg_n);
		moreTabBg.setImageResource(R.drawable.tabbar_bg_n);
		eventList.stopAdBannerRotation();
	}

	private void selectPromotionTab() {
		eventTabButton.setImageResource(R.drawable.tabbar_eventlist);
		calendarTabButton.setImageResource(R.drawable.tabbar_calendar);
		socialTabButton.setImageResource(R.drawable.tabbar_social);
		promotionTabButton.setImageResource(R.drawable.tabbar_promotion);
		moreTabButton.setImageResource(R.drawable.tabbar_more);
		eventTabBg.setImageResource(R.drawable.tabbar_bg_n);
		calendarTabBg.setImageResource(R.drawable.tabbar_bg_n);
		socialTabBg.setImageResource(R.drawable.tabbar_bg_n);
		promotionTabBg.setImageResource(R.drawable.tabbar_bg_h2);
		moreTabBg.setImageResource(R.drawable.tabbar_bg_n);
		eventList.stopAdBannerRotation();
	}

	private void selectMoreTab() {
		eventTabButton.setImageResource(R.drawable.tabbar_eventlist);
		calendarTabButton.setImageResource(R.drawable.tabbar_calendar);
		socialTabButton.setImageResource(R.drawable.tabbar_social);
		promotionTabButton.setImageResource(R.drawable.tabbar_promotion);
		moreTabButton.setImageResource(R.drawable.tabbar_more);
		eventTabBg.setImageResource(R.drawable.tabbar_bg_n);
		calendarTabBg.setImageResource(R.drawable.tabbar_bg_n);
		socialTabBg.setImageResource(R.drawable.tabbar_bg_n);
		promotionTabBg.setImageResource(R.drawable.tabbar_bg_n);
		moreTabBg.setImageResource(R.drawable.tabbar_bg_h2);
		eventList.stopAdBannerRotation();
	}

	public static boolean isEventTabLoaded() {
		return isEventTabLoaded;
	}

	public static void setEventTabLoaded(boolean isEventTabLoaded) {
		MainActivity.isEventTabLoaded = isEventTabLoaded;
	}

	public static boolean isCalendarTabLoaded() {
		return isCalendarTabLoaded;
	}

	public static void setCalendarTabLoaded(boolean isCalendarTabLoaded) {
		MainActivity.isCalendarTabLoaded = isCalendarTabLoaded;
	}

	public static boolean isSocialTabLoaded() {
		return isSocialTabLoaded;
	}

	public static void setSocialTabLoaded(boolean isSocialTabLoaded) {
		MainActivity.isSocialTabLoaded = isSocialTabLoaded;
	}

	public static boolean isPromotionTabLoaded() {
		return isPromotionTabLoaded;
	}

	public static void setPromotionTabLoaded(boolean isPromotionTabLoaded) {
		MainActivity.isPromotionTabLoaded = isPromotionTabLoaded;
	}

	public static int getCurrentTab() {
		return MainActivity.currentTab;
	}


}
