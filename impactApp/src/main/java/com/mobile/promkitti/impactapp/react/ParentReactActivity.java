package com.mobile.promkitti.impactapp.react;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.airbnb.android.react.maps.MapsPackage;
import com.facebook.react.BuildConfig;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.shell.MainReactPackage;
import com.mobile.promkitti.impactapp.ImpactActivity;
import ca.jaysoo.extradimensions.ExtraDimensionsPackage;

import static com.facebook.react.common.LifecycleState.RESUMED;

/**
 * Created by nananetta on 7/12/16.
 */
public class ParentReactActivity extends ImpactActivity {

    private static ReactInstanceManager reactInstanceManager;
    private static final int OVERLAY_PERMISSION_REQUEST_CODE = 2;


    @TargetApi(Build.VERSION_CODES.M)
    protected void _askForOverlayPermission() {
        if (!BuildConfig.DEBUG || Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        if (!Settings.canDrawOverlays(this)) {
            Intent settingsIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(settingsIntent, OVERLAY_PERMISSION_REQUEST_CODE);
        }
    }


    protected ReactInstanceManager getReactInstance() {
        Log.w("REACT-N", "getReacInstance() - START");
        if(reactInstanceManager == null) {
            Log.w("REACT-N", "getReacInstance()");
            reactInstanceManager = ReactInstanceManager.builder()
                    .setApplication(getApplication())
                    .setBundleAssetName("index.android.bundle")
                    .addPackage(new MainReactPackage())
                    .addPackage(new ImpactReactPackage())
                    .addPackage(new MapsPackage())
                    .addPackage(new ExtraDimensionsPackage())
                    .setUseDeveloperSupport(BuildConfig.DEBUG)
                    .setInitialLifecycleState(RESUMED)
                    .build();
            Log.w("REACT-N", "getReacInstance() - DONE");
        }
        return reactInstanceManager;
    }

}
