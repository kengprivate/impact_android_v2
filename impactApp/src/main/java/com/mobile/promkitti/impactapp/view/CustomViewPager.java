package com.mobile.promkitti.impactapp.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.mobile.promkitti.impactapp.common.EventImage;
import com.mobile.promkitti.impactapp.common.EventMedia;

import java.util.ArrayList;

public class CustomViewPager extends FrameLayout  {

	private Context context;

	public CustomViewPager(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init(attrs);
	}

	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init(attrs);
	}

	public CustomViewPager(Context context) {
		super(context);
		this.context = context;
		init(null);
	}

	private void init(AttributeSet attrs) {
		FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		frameLayoutParams.gravity = Gravity.CENTER;
		setLayoutParams(frameLayoutParams);

		ViewPager vp = new ViewPager(context);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		lp.gravity = Gravity.TOP;
		vp.setLayoutParams(lp);
//		getResources().getIdentifier("promo_detail_placeholder", "drawable", context.getPackageName());
		vp.setBackgroundColor(0xFFFFFFFF);
		addView(vp);

		ImageView v = new ImageView(context);
		int drawableResourceId = context.getResources().getIdentifier("promo_detail_placeholder", "drawable", context.getPackageName());
		v.setImageResource(drawableResourceId);
		addView(v);

		// Set the pager with an adapter
		ArrayList<EventMedia> mediaList = new ArrayList<EventMedia>();
		EventImage ei = new EventImage();
		ei.setType("i");
		ei.setId("3");
		ei.setUrl("http://iphoneapp.impact.co.th/i/el/600.png");
		mediaList.add(ei);
//		Activity activity = (Activity) context;
//		Log.e("AaaAAAaaAAAaaA", activity.getLocalClassName());
//		PromotionPagerAdapter pagerAdapter = new PromotionPagerAdapter(getSupportFragmentManager(), mediaList);
//		vp.setAdapter(pagerAdapter);

	}

	/*
	<FrameLayout
//	android:id="@+id/promotionDetailImageLayout"
//	android:layout_width="wrap_content"
//	android:layout_height="wrap_content"
//	android:layout_gravity="center" >

	<android.support.v4.view.ViewPager
	android:id="@+id/pager"
//	android:layout_width="fill_parent"
//	android:layout_height="wrap_content"
	android:layout_gravity="top|center_horizontal"
	android:background="#ffffff" >
	</android.support.v4.view.ViewPager>

	<ImageView
	android:id="@+id/promotionDetailImagePlaceholder"
	android:layout_width="fill_parent"
	android:layout_height="wrap_content"
	android:layout_gravity="center"
	android:padding="3dp"
	android:src="@drawable/promo_detail_placeholder" />
	</FrameLayout>
	*/

}