package com.mobile.promkitti.impactapp.valueobjects;


import android.graphics.drawable.Drawable;

import com.mobile.promkitti.impactapp.Constants;

public class AdBanner extends Event {

	private String fileType;
	private String displayTime; // seconds
	
	private Drawable bannerImage;
	private Drawable detailImage;
	
	public String getPrefix() {
		return Constants.PREFIX_AD_BANNER;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getDisplayTime() {
		return displayTime;
	}
	public void setDisplayTime(String displayTime) {
		this.displayTime = displayTime;
	}
	public Drawable getBannerImage() {
		return bannerImage;
	}
	public void setBannerImage(Drawable bannerImage) {
		this.bannerImage = bannerImage;
	}
	public Drawable getDetailImage() {
		return detailImage;
	}
	public void setDetailImage(Drawable detailImage) {
		this.detailImage = detailImage;
	}
	
	
}
