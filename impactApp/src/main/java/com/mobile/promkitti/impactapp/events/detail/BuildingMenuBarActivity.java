package com.mobile.promkitti.impactapp.events.detail;

import android.os.Bundle;
import android.view.View;

import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;

public class BuildingMenuBarActivity extends ImpactActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.building_menubar);
	}

	
	public void saveImageMapButtonListener(View view) {
		setResult(1);
		finish();
	}

	public void saveImageMapCancelButtonListener(View view) {
		setResult(-1);
		finish();
	}
}
