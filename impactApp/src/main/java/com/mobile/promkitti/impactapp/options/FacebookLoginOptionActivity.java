package com.mobile.promkitti.impactapp.options;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.util.Callback;
import com.mobile.promkitti.impactapp.util.FlurryUtil;

import java.util.Arrays;
import java.util.List;

public class FacebookLoginOptionActivity extends ImpactActivity {

    private FrameLayout fbLoginLayout;
    private FrameLayout fbLogoutLayout;

    private CallbackManager callbackManager;
    private LoginManager loginManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fblogin_options);
        fbLoginLayout = (FrameLayout) findViewById(R.id.loginFBLayout);
        fbLogoutLayout = (FrameLayout) findViewById(R.id.logoutFBLayout);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        updateView();
        
		FlurryUtil.onPageView();
        FlurryUtil.logEvent(FlurryConstants.MORE_FACEBOOK_LOGIN_LAUNCHED);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    
    public void updateView() {
        if(isLoggedIn()) {
    		fbLoginLayout.setVisibility(View.GONE);
    		fbLogoutLayout.setVisibility(View.VISIBLE);
        } else {
    		fbLoginLayout.setVisibility(View.VISIBLE);
    		fbLogoutLayout.setVisibility(View.GONE);
        }
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return (accessToken != null);
    }

    public void fbLoginButtonListener(View view) {
        //this loginManager helps you eliminate adding a LoginButton to your UI
        loginManager = LoginManager.getInstance();
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        loginManager.logInWithPublishPermissions(this, permissionNeeds);
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("FB","Login Success");
                updateView();
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("onError");
            }
        });


        FlurryUtil.logEvent(FlurryConstants.FB_LOGIN);
	}
    
    
    public void fbLogoutButtonListener(View view) {
        LoginManager.getInstance().logOut();
    	updateView();
    	FlurryUtil.logEvent(FlurryConstants.FB_LOGOUT);
	}
    
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
    }
    
   class FacebookLoginOptionCallback extends Callback {
	   public void execute() {
		   fbLoginLayout.setVisibility(View.GONE);
		   fbLogoutLayout.setVisibility(View.VISIBLE);
	   }
   }
   
}
