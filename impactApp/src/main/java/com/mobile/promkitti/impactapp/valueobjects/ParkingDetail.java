package com.mobile.promkitti.impactapp.valueobjects;

import android.graphics.drawable.Drawable;

import com.mobile.promkitti.impactapp.Constants;

import java.util.HashMap;
import java.util.Map;

public class ParkingDetail {

	private String title;
	private String desc;
	private String availableParking;
	private HashMap<String, String> parkingLot = new HashMap<String, String>();

	private boolean isDetailsLoaded = false;

	public ParkingDetail() {
		super();
	}

	public String getPrefix() {
		return Constants.PREFIX_PARKING_DETAIL;
	}


	public boolean isDetailsLoaded() {
		return isDetailsLoaded;
	}

	public void setDetailsLoaded(boolean isDetailsLoaded) {
		this.isDetailsLoaded = isDetailsLoaded;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String dateLocation) {
		this.desc = desc;
	}


	public HashMap<String, String> getParkingLot() {
		return parkingLot;
	}

	public void setParkingLot(HashMap<String, String> parkingLot) {
		this.parkingLot = parkingLot;
	}


	public String getAvailableParking() {
		return availableParking;
	}

	public void setAvailableParking(String availableParking) {
		this.availableParking = availableParking;
	}
}
