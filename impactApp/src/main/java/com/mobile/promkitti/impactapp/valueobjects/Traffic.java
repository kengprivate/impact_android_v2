package com.mobile.promkitti.impactapp.valueobjects;

import com.mobile.promkitti.impactapp.Constants;

/**
 * Created by nananetta on 7/25/16.
 */
public class Traffic extends Event {

    private String longDesc;


    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public String getPrefix() {
        return Constants.PREFIX_TRAFFIC;
    }
}
