package com.mobile.promkitti.impactapp.valueobjects;

import java.util.ArrayList;

public class EventListObject {

	private ArrayList<CalendarEvent> eventList = new ArrayList<CalendarEvent>();
	private ArrayList<AdBanner> adList = new ArrayList<AdBanner>();
	private String firstDate;
	private String lastDate;
	
	public ArrayList<CalendarEvent> getEventList() {
		return eventList;
	}
	public void setEventList(ArrayList<CalendarEvent> eventList) {
		this.eventList = eventList;
	}
	public String getFirstDate() {
		return firstDate;
	}
	public void setFirstDate(String firstDate) {
		this.firstDate = firstDate;
	}
	public String getLastDate() {
		return lastDate;
	}
	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}
	public ArrayList<AdBanner> getAdList() {
		return adList;
	}
	public void setAdList(ArrayList<AdBanner> adList) {
		this.adList = adList;
	}
	
	
	
}
