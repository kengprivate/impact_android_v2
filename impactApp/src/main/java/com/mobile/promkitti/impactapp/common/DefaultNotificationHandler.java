package com.mobile.promkitti.impactapp.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.util.FlurryUtil;

public class DefaultNotificationHandler extends ImpactActivity implements NotificationHandler, OnClickListener {

	private Activity activity;

	public DefaultNotificationHandler(Activity activity) {
		this.activity = activity;
	}

	@Override
	public void process() {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(activity.getIntent().getStringExtra("MSG")).setCancelable(false).setTitle("IMPACT Message");
		builder.setPositiveButton("OK", this);
		builder.create().show();
	}

	public void onClick(DialogInterface dialog, int id) {
		Log.d("FLURRY", FlurryConstants.PUSH_OPEN_APP);
		FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_APP);
		activity.finish();
	}

}
