package com.mobile.promkitti.impactapp;


public class ActivityParameters {
	
	/* PARAMETERS */
	public static final String INTERNAL_ID = "INT_ID";
	public static final String ID = "ID";
	public static final String URL = "URL";
	public static final String TITLE = "TITLE";
	public static final String LOCATIONS = "LOCATIONS";
	public static final String ORG_INFO = "ORG_INFO";
	public static final String DIRECT = "DIRECT";

	private ActivityParameters() {
		
	}
	

}
