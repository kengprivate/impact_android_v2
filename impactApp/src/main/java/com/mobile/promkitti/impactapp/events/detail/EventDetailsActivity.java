package com.mobile.promkitti.impactapp.events.detail;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.ActivityParameters;
import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.FragmentImpactActivity;
import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.MainActivity;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.common.FacebookBrowserActivity;
import com.mobile.promkitti.impactapp.common.ShareUtils;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.NotificationUtil;
import com.mobile.promkitti.impactapp.util.Utility;
import com.mobile.promkitti.impactapp.valueobjects.CalendarEvent;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class EventDetailsActivity extends FragmentImpactActivity {

	private DataController dc = DataController.getInstance();
	private CalendarEvent event;
	private String id;
	private boolean isDirect;
	private ViewPager viewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_details);

		View view = findViewById(R.id.scrollViewLayout);
		Utility.fixBackgroundRepeat(view);

		id = getIntent().getStringExtra(ActivityParameters.ID);
		isDirect = getIntent().getBooleanExtra(ActivityParameters.DIRECT, false);
		if (isDirect) {
			Log.d("FLURRY", FlurryConstants.PUSH_OPEN_EVENT_ID);
			Map<String, String> params = new HashMap<String, String>();
			params.put(FlurryConstants.PARAM_EVENT_ID, id);
			FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_EVENT_ID, params);
			FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_EVENT_ID + Constants.SPACE + id);
		}

		// Clear badge
		NotificationUtil.getEventNotificationList().removeNotification(id);

		Log.d("Event ID:", id);
		FlurryUtil.onPageView();
		FlurryUtil.logEvent(FlurryConstants.PAGE_EVENT_DETAIL);

		ImageView placeholderView = (ImageView) findViewById(R.id.eventDetailImagePlaceholder);
		Drawable placeHolder = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ed_placeholder);
		viewPager = (ViewPager) findViewById(R.id.pager);

		/* Check Screen Density */
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int screenDensity = metrics.densityDpi;
		int targetWidth = 0;
		int targetHeight = 0;

		if (screenDensity <= DisplayMetrics.DENSITY_HIGH) {
			float MULTIPLIER_FACTOR = 1.02f;
			targetWidth = (int) (placeHolder.getIntrinsicWidth() * MULTIPLIER_FACTOR);
			targetHeight = (int) (placeHolder.getIntrinsicHeight() * MULTIPLIER_FACTOR);
		} else if (screenDensity <= DisplayMetrics.DENSITY_XHIGH) {
			float MULTIPLIER_FACTOR = 1.0f;
			targetWidth = (int) (placeHolder.getIntrinsicWidth() * MULTIPLIER_FACTOR);
			targetHeight = (int) (placeHolder.getIntrinsicHeight() * MULTIPLIER_FACTOR);
		} else {
			float MULTIPLIER_FACTOR = 1.01f;
			targetWidth = (int) (placeHolder.getIntrinsicWidth() * MULTIPLIER_FACTOR);
			targetHeight = (int) (placeHolder.getIntrinsicHeight() * MULTIPLIER_FACTOR);
		}
		FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(targetWidth, targetHeight);
		frameLayoutParams.gravity = Gravity.CENTER;
		placeholderView.setLayoutParams(frameLayoutParams);

		if (screenDensity <= DisplayMetrics.DENSITY_HIGH) {
			viewPager.setLayoutParams(frameLayoutParams);
		}

		new LoadingTask().execute();
	}

	@Override
	public void onBackPressed() {
		if (isDirect) {
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.setClass(getApplicationContext(), MainActivity.class);
			startActivity(intent);
			finish();
		} else {
			super.onBackPressed();
		}
	}

	public void urlButtonListener(View view) {
		String url = event.getUrl();
		if (Utility.isBlank(url)) {
			return;
		}

		Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(myIntent);

		FlurryUtil.logEvent(FlurryConstants.EVENT_URL);
	}

	public void directionButtonListener(View view) {
		if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
			return;
		}


		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (location == null) {
			location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		String currentLocation = "";
		if (location != null) {
			currentLocation = location.getLatitude() + "," + location.getLongitude();
		}

		// Map<String, String> params = new HashMap<String, String>();
		// params.put("eventInternalId", internalId);

		// Test Start Address
		// currentLocation = "13.777984,100.490227";

		// Impact
		// String destinationLocation =
		// "IMPACT Muang Thong Thani @13.910282,100.548309";
		String destinationLocation = "IMPACT Muang Thong Thani @13.91249,100.547905";
		String directionURL = String.format("http://maps.google.com/maps?f=d&saddr=%s&daddr=%s", currentLocation, destinationLocation);
		if(event.getLat()!=null && event.getLat().length() >0 && event.getLon()!=null && event.getLon().length() > 0) {
			directionURL = String.format("http://maps.google.com/maps?f=d&saddr=%s&daddr=%s,%s", currentLocation, event.getLat(), event.getLon());
		}
//		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?f=d&saddr=" + currentLocation + "&daddr=" + destinationLocation));
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(directionURL));
		startActivity(intent);

		FlurryUtil.logEvent(FlurryConstants.EVENT_DIRECTIONS_TO_EVENT);
		/*
		 * https://maps.google.com/maps?saddr=Current+Location&daddr=IMPACT+Muang + Thong+Thani+%4013.91249,100.547905&hl=en&sll=13.91249,100.547905&sspn
		 * =0.008769,0.013937&geocode=FZbh0gAdhgUABinzYQ0oMmAdMTEgSOJdsgABAQ%3 BFapJ1AAdQT3-BQ&mra=pd&t=m&z=18
		 */
	}

	public void buildingMapButtonListener(View view) {
		Intent intent = new Intent(getApplicationContext(), BuildingMapActivity.class);
		intent.putExtra(ActivityParameters.ID, event.getId());
		intent.putExtra(ActivityParameters.TITLE, event.getTitle());
		intent.putExtra(ActivityParameters.LOCATIONS, event.getLocations());
		startActivity(intent);

		FlurryUtil.logEvent(FlurryConstants.EVENT_BUILDINGS);
	}

	public void organizerButtonListener(View view) {
		Intent intent = new Intent(getApplicationContext(), OrganizerInfoActivity.class);
		intent.putExtra(ActivityParameters.ID, event.getId());
		intent.putExtra(ActivityParameters.ORG_INFO, event.getOrganizerInfo());
		startActivity(intent);

		FlurryUtil.logEvent(FlurryConstants.EVENT_ORG_INFO);
	}

	public void ticketUrlButtonListener(View view) {
		String url = event.getTicketUrl();
		Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(myIntent);

		FlurryUtil.logEvent(FlurryConstants.EVENT_TICKET_URL);
	}

	public void ticketCallButtonListener(View view) {
		TextView ticketCallButton = (TextView) findViewById(R.id.ticketCallButton);
//		String number = "";
//		try {
//			number = ticketCallButton.getText().toString().split(" ")[1];
//		} catch (Exception e) {
//			Log.e("", "Invalid Number");
//		}
		String url = "tel:" + ticketCallButton.getText().toString();
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));

		if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
		} else {
			startActivity(intent);
			FlurryUtil.logEvent(FlurryConstants.EVENT_TICKET_TEL);
		}
	}

	public void addToCalendarButtonListener(View view) {

		SharedPreferences mPrefs = getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = mPrefs.edit();
		StringBuilder infoSb = new StringBuilder();
		infoSb.append(event.getEndDate());
		infoSb.append(":");
		infoSb.append(Long.toString(event.getTimestamp()));
		String eventCalendarInfo = infoSb.toString();
		editor.putString(event.getInternalId(), eventCalendarInfo);
		editor.commit();

		Map<String, String> flurryParams = new HashMap<String, String>();
		flurryParams.put(FlurryConstants.PARAM_EVENT_ID, id);
		FlurryUtil.logEvent(FlurryConstants.EVENT_ADDED_TO_CALENDAR, flurryParams);

		Intent intent = new Intent(Intent.ACTION_EDIT);
		intent.setType("vnd.android.cursor.item/event");
		intent.putExtra("beginTime", event.getStartDateTime().getTime());
		// intent.putExtra("allDay", true);
		intent.putExtra("endTime", event.getEndDateTime().getTime());
		intent.putExtra("title", event.getTitle());

		StringBuilder sb = new StringBuilder();
		// sb.append("<html>");
		// sb.append("<head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"></head>");
		// sb.append("<body>");
		sb.append(event.getTitle());
		sb.append("\n\n");
		sb.append(event.getDescription());
		sb.append("\n\n");

		if (!Utility.isBlank(event.getTicketPrice())) {
			sb.append("Ticket Price (Baht):\n");
			sb.append(event.getTicketPrice());
			sb.append("\n\n");
		}

		if (!Utility.isBlank(event.getDateLocation())) {
			sb.append("Date & Time:\n");
			sb.append(event.getDateLocation());
			sb.append("\n\n");
		}

		if (!Utility.isBlank(event.getTicketUrl())) {
			sb.append("Ticket Reservation URL:\n");
			sb.append(event.getTicketUrl());
			sb.append("\n\n");
		}

		if (!Utility.isBlank(event.getTicketTel())) {
			sb.append("Ticket Reservation Tel:\n");
			sb.append(event.getTicketTel());
			sb.append("\n\n");
		}

		if (!Utility.isBlank(event.getUrl())) {
			sb.append("URL:\n");
			sb.append(event.getUrl());
			sb.append("\n\n");
		}

		if (!Utility.isBlank(event.getOrganizerInfo())) {
			sb.append("Organizer Information:\n");
			sb.append(event.getOrganizerInfo());
			sb.append("\n\n");
		}

		sb.append("\n");

		intent.putExtra("description", sb.toString());

		startActivity(intent);
	}

	public void shareEventButtonListener(View view) {
		String contentURL = null;
		String imageURL = "http://iphoneapp.impact.co.th/i/img/logo_200x380.png";
		ArrayList<EventMedia> mediaList = event.getMediaList();
		if(event.hasImage()) {
			for (EventMedia media : mediaList) {
				if ("i".equalsIgnoreCase(media.getType())) {
					imageURL = media.getUrl();
					break;
				}
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append(event.getDescription());
		sb.append("\r\n\r\n");


		if (!Utility.isBlank(event.getTicketPrice())) {
			sb.append("Ticket Price (Baht):\r\n");
			sb.append(event.getTicketPrice());
			sb.append("\r\n\r\n");
		}

		if (!Utility.isBlank(event.getDateLocation())) {
			sb.append("Date & Time:\r\n");
			sb.append(event.getDateLocation());
			sb.append("\r\n\r\n");
		}

		if (!Utility.isBlank(event.getTicketUrl())) {
			sb.append("Ticket Reservation URL:\r\n");
			sb.append(event.getTicketUrl());
			sb.append("\r\n\r\n");
		}

		if (!Utility.isBlank(event.getTicketTel())) {
			sb.append("Ticket Reservation Tel:\r\n");
			sb.append(event.getTicketTel());
			sb.append("\r\n\r\n");
		}

		if (!Utility.isBlank(event.getUrl())) {
			sb.append("URL:\r\n");
			sb.append(event.getUrl());
			sb.append("\r\n\r\n");
			contentURL = event.getUrl();
		}

		if (!Utility.isBlank(event.getOrganizerInfo())) {
			sb.append("Organizer Information:\r\n");
			sb.append(event.getOrganizerInfo());
			sb.append("\r\n\r\n");
		}

		sb.append("Sent from my Android");
		Intent chooserIntent = ShareUtils.share(this, view, sb.toString(), event.getTitle(), contentURL, imageURL);
		if(chooserIntent == null) {
			return;
		}

		Map<String, String> flurryParams = new HashMap<String, String>();
		flurryParams.put(FlurryConstants.PARAM_EVENT_ID, id);
		FlurryUtil.logEvent(FlurryConstants.SHARE_EVENT, flurryParams);
		FlurryUtil.logEvent(FlurryConstants.SHARE_EVENT + Constants.SPACE + id);

		startActivity(chooserIntent);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void viewFacebookButtonListener(View view) {
		String url = event.getFbUrl();
		if (Utility.isBlank(url)) {
			return;
		}
		Intent intent = new Intent(EventDetailsActivity.this, FacebookBrowserActivity.class);
		intent.putExtra(ActivityParameters.URL, url);
		startActivity(intent);
		FlurryUtil.logEvent(FlurryConstants.EVENT_VIEW_ON_FB);
	}


	private class LoadingTask extends AsyncTask<Void, Void, Integer> {
		/**
		 * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
		 */
		protected Integer doInBackground(Void... val) {
			try {
				dc.getEventDetail(id);
			} catch (ImpactException e) {
				return -1;
			}
			return 0;
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
		 */
		protected void onPostExecute(Integer i) {
			try {
				if (i < 0) {
					throw new ImpactException(getString(R.string.loading_error));
				}
				event = dc.getEventDetail(id);

				if (event == null) {
					finish();
					return;
				}

				// Set the pager with an adapter
				ArrayList<EventMedia> mediaList = event.getMediaList();
				EventPagerAdapter pagerAdapter = new EventPagerAdapter(getSupportFragmentManager(), mediaList);
				viewPager.setAdapter(pagerAdapter);

				// Bind the title indicator to the adapter
				CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
				pageIndicator.setViewPager(viewPager);

				if (mediaList.size() < 2) {
					pageIndicator.setVisibility(View.GONE);
				}

				TextView titleText = (TextView) findViewById(R.id.eventDetailTitleText);
				// titleText.setText(event.getTitle() == null ? "" :
				// Utility.unEscapeHtml(event.getTitle()));
				titleText.setText(event.getTitle());
				registerForContextMenu(titleText);

				TextView descText = (TextView) findViewById(R.id.eventDetailDescriptionText);
				// descText.setText(event.getDescription() == null ? "" :
				// Utility.unEscapeHtml(event.getDescription()));
				descText.setText(event.getDescription());
				registerForContextMenu(descText);

				Drawable border = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ticket_border);
				LinearLayout.LayoutParams borderLayoutParams = new LinearLayout.LayoutParams(border.getIntrinsicWidth(), LinearLayout.LayoutParams.WRAP_CONTENT);
				borderLayoutParams.gravity = Gravity.CENTER;

				TextView ticketPriceText = (TextView) findViewById(R.id.TicketPriceText);
				ticketPriceText.setText(event.getTicketPrice() == null ? "" : event.getTicketPrice());
				ticketPriceText.setLayoutParams(borderLayoutParams);
				registerForContextMenu(ticketPriceText);

				TextView ticketDateText = (TextView) findViewById(R.id.TicketDateText);
				ticketDateText.setText(event.getDateLocation() == null ? "" : event.getDateLocation());
				ticketDateText.setLayoutParams(borderLayoutParams);
				registerForContextMenu(ticketDateText);

				// TODO: resolve eventDetailImage
				// ImageView eventDetailImageView = (ImageView) findViewById(R.id.eventDetailImage);
				// Drawable placeHolder = getResources().getDrawable(R.drawable.ed_placeholder);
				// float MULTIPLIER_FACTOR = 1;
				// int targetWidth = (int) (placeHolder.getIntrinsicWidth() * MULTIPLIER_FACTOR);
				// int targetHeight = (int) (placeHolder.getIntrinsicHeight() * MULTIPLIER_FACTOR);
				//
				// FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(targetWidth, targetHeight);
				// frameLayoutParams.gravity = Gravity.CENTER;
				// eventDetailImageView.setLayoutParams(frameLayoutParams);

				TextView urlButton = (TextView) findViewById(R.id.urlButton);
				TextView urlTitleText = (TextView) findViewById(R.id.urlTitleText);
				LinearLayout urlButtonLayout = (LinearLayout) findViewById(R.id.urlButtonLayout);
				if (Utility.isBlank(event.getUrl())) {
					urlButtonLayout.setVisibility(View.GONE);
					urlTitleText.setVisibility(View.GONE);
				} else {
					urlButton.setText(event.getUrl());
					urlButtonLayout.setVisibility(View.VISIBLE);
					urlTitleText.setVisibility(View.VISIBLE);
				}

				TextView ticketReservationText = (TextView) findViewById(R.id.ticketReservationText);
				registerForContextMenu(ticketReservationText);
				LinearLayout ticketUrlLayout = (LinearLayout) findViewById(R.id.ticketUrlLayout);
				FrameLayout ticketCallLayout = (FrameLayout) findViewById(R.id.ticketCallLayout);
				FrameLayout viewFacebookLayout = (FrameLayout) findViewById(R.id.viewFacebookLayout);
				TextView ticketUrlButton = (TextView) findViewById(R.id.ticketUrlButton);
				TextView ticketCallButton = (TextView) findViewById(R.id.ticketCallButton);
				FrameLayout organizerLayout = (FrameLayout) findViewById(R.id.organizerLayout);

				if (Utility.isBlank(event.getOrganizerInfo())) {
					organizerLayout.setVisibility(View.GONE);
				} else {
					organizerLayout.setVisibility(View.VISIBLE);
				}

				if (Utility.isBlank(event.getFbUrl())) {
					viewFacebookLayout.setVisibility(View.GONE);
				} else {
					viewFacebookLayout.setVisibility(View.VISIBLE);
				}

				int ticketReservationMethodCount = 0;
				if (!Utility.isBlank(event.getTicketUrl())) {
					ticketUrlButton.setText(event.getTicketUrl());
					ticketReservationMethodCount++;
				}
				if (!Utility.isBlank(event.getTicketTel())) {
					ticketCallButton.setText(event.getTicketTel());
					ticketReservationMethodCount++;
				}

				switch (ticketReservationMethodCount) {
				case 0:
					ticketReservationText.setVisibility(View.GONE);
					ticketUrlLayout.setVisibility(View.GONE);
					ticketCallLayout.setVisibility(View.GONE);
					break;

				case 1:
					ticketReservationText.setVisibility(View.VISIBLE);
					if (!Utility.isBlank(event.getTicketUrl())) {
						ticketCallLayout.setBackgroundResource(R.drawable.button_url);
						ticketUrlLayout.setVisibility(View.VISIBLE);
						ticketCallLayout.setVisibility(View.GONE);
					} else {
						ticketCallLayout.setBackgroundResource(R.drawable.button_url);
						ticketUrlLayout.setVisibility(View.GONE);
						ticketCallLayout.setVisibility(View.VISIBLE);
						ticketCallButton.setTextColor(getResources().getColorStateList(R.color.button_text_selector_3));
					}
					break;

				case 2:
					ticketReservationText.setVisibility(View.VISIBLE);
					ticketUrlLayout.setBackgroundResource(R.drawable.button_ticket_top);
					ticketCallLayout.setBackgroundResource(R.drawable.button_ticket_bottom);
					ticketUrlLayout.setVisibility(View.VISIBLE);
					ticketCallLayout.setVisibility(View.VISIBLE);
					break;
				}

			} catch (ImpactException e) {
				Log.e(getClass().toString(), e!=null?e.getMessage():"");
				AlertDialog.Builder builder = new AlertDialog.Builder(EventDetailsActivity.this);
				try {
					builder.setMessage(Constants.ERR_CONNECTION_ERROR).setCancelable(false).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							finish();
						}
					});
					builder.create().show();
				} catch (WindowManager.BadTokenException btx) {
					Log.e(getClass().toString(), btx!=null?btx.getMessage():"");
				}
			}
			LinearLayout contentLayout = (LinearLayout) findViewById(R.id.contentLayout);
			contentLayout.setVisibility(View.VISIBLE);
			LinearLayout overlay = (LinearLayout) findViewById(R.id.loadingOverlayLayout);
			overlay.setVisibility(View.GONE);

			ImageView placeholderView = (ImageView) findViewById(R.id.eventDetailImagePlaceholder);
			placeholderView.setVisibility(View.GONE);

			Map<String, String> flurryParams = new HashMap<String, String>();
			flurryParams.put(FlurryConstants.PARAM_EVENT_ID, id);
			FlurryUtil.logEvent(FlurryConstants.VIEW_EVENT_ID, flurryParams);
			FlurryUtil.logEvent(FlurryConstants.VIEW_EVENT_ID + Constants.SPACE + id);
		}
	}

}
