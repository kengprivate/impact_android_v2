package com.mobile.promkitti.impactapp.promotion;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.common.EventImage;
import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.valueobjects.Promotion;


public class PromotionImageFragment extends Fragment {

	private DataController dc = DataController.getInstance();
	private EventImage media;

	public static final PromotionImageFragment newInstance(EventMedia eventMedia) {
		PromotionImageFragment fragment = new PromotionImageFragment();
		final Bundle args = new Bundle(1);
		args.putParcelable("EVENT_MEDIA", eventMedia);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * When creating, retrieve this instance's number from its arguments.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		media = getArguments().getParcelable("EVENT_MEDIA");
	}

	/**
	 * The Fragment's UI is just a simple text view showing its instance number.
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.promotion_image_fragment, container, false);
		new LoadingImageTask().execute(this.media);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	private class LoadingImageTask extends AsyncTask<EventImage, Void, EventImage> {

		private Drawable content;

		/**
		 * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
		 */
		protected EventImage doInBackground(EventImage... val) {
			try {
				this.content = dc.getPromotionDetailImage(media.getId(), media.getUrl());
				return val[0];
			} catch (ImpactException e) {
				return val[0];
			}
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
		 */
		protected void onPostExecute(EventImage media) {
			Promotion promotion;
			try {
				promotion = dc.getPromotionDetail(media.getId());
				if (promotion == null) {
					return;
				}
				
				// if user exits the screen and view no longer exists 
				if(getView() == null) {
					return;
				}

				ImageView eventDetailImageView = (ImageView) getView().findViewById(R.id.promotionDetailImage);
				Drawable placeHolder = ContextCompat.getDrawable(getContext(), R.drawable.promo_detail_placeholder);

				int targetWidth = placeHolder.getIntrinsicWidth();
				int targetHeight = placeHolder.getIntrinsicHeight();

				FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(targetWidth, targetHeight);
				frameLayoutParams.gravity = Gravity.CENTER;
				eventDetailImageView.setLayoutParams(frameLayoutParams);

				if (this.content != null) {
					eventDetailImageView.setImageDrawable(this.content);
					eventDetailImageView.setScaleType(ScaleType.FIT_XY);
				} else {
					dc.setPromotionDetailImage(media.getId(), media.getUrl(), placeHolder);
				}
			} catch (ImpactException e) {
				e.printStackTrace();
			}

			ProgressBar mSpinner = (ProgressBar) getView().findViewById(R.id.promotionDetailImageProgressBar);
			mSpinner.setVisibility(View.GONE);
		}

	}
}