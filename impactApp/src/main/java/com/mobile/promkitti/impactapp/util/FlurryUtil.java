package com.mobile.promkitti.impactapp.util;

import android.content.Context;

import com.flurry.android.FlurryAgent;
import com.mobile.promkitti.impactapp.Constants;

import java.util.Map;

public class FlurryUtil {
	
	public static void onStartSession(Context context) {
//		if(Constants.FLURRY_ENABLED) {
//			FlurryAgent.setLogLevel(Log.DEBUG);
//			FlurryAgent.setLogEnabled(true);
//			FlurryAgent.setLogEvents(true);
//			FlurryAgent.onStartSession(context, Constants.FLURRY_API_KEY);
//		}
	}
	
	public static void onEndSession(Context context) {
//		if(Constants.FLURRY_ENABLED) {
//			FlurryAgent.onEndSession(context);
//		}
	}
	
	public static void onPageView() {
		if(Constants.FLURRY_ENABLED) {
			FlurryAgent.onPageView();
		}
	}
	
	public static void logEvent(String event) {
		if(Constants.FLURRY_ENABLED) {
			FlurryAgent.logEvent(event);
		}
	}

	public static void logEvent(String event, Map<String, String> map) {
		if(Constants.FLURRY_ENABLED) {
			FlurryAgent.logEvent(event, map);
		}
	}

}
