package com.mobile.promkitti.impactapp.events.detail;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;

import java.io.File;
import java.io.FileOutputStream;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class BuildingMapActivity extends ImpactActivity {

    private ImageViewTouch buildingMapImage;
    private String eventTitle = "";
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.building_map);

        eventTitle = getIntent().getStringExtra("TITLE");
        String locations = getIntent().getStringExtra("LOCATIONS");

        buildingMapImage = (ImageViewTouch) findViewById(R.id.buildingMapCanvas);
        bitmap = new BuildingMapBuilder(getApplicationContext(), locations).create();
        if (bitmap != null) {
            buildingMapImage.setImageBitmap(bitmap, true, null, 4.0f);
        } else {
            Toast.makeText(getApplicationContext(), "Image Error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bitmap != null) {
            bitmap.recycle();
        }
    }

    public void saveMapButtonListener(View view) {
        Intent intent = new Intent(getApplicationContext(), BuildingMenuBarActivity.class);
        intent.putExtra("TITLE", eventTitle);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, 200);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode > 0) {
            saveImage();
        }
    }

    private void saveImage() {
        Bitmap tempBitmap = null;
        Bitmap compass = null;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 12);
            return;
        }

        try {
            tempBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), null, true);
            Canvas c = new Canvas(tempBitmap);
            compass = BitmapFactory.decodeResource(getResources(), R.drawable.map_compass_full);
            c.drawBitmap(compass, 30, 30, null);

            FileOutputStream fos = null;
            boolean mExternalStorageAvailable = false;
            boolean mExternalStorageWriteable = false;
            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                // We can read and write the media
                mExternalStorageAvailable = mExternalStorageWriteable = true;
            } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                // We can only read the media
                mExternalStorageAvailable = true;
                mExternalStorageWriteable = false;
            } else {
                // Something else is wrong. It may be one of many other states, but all we need
                // to know is we can neither read nor write
                mExternalStorageAvailable = mExternalStorageWriteable = false;
            }

            if (mExternalStorageAvailable && mExternalStorageWriteable) {
                File impactDir = new File(Environment.getExternalStorageDirectory().getPath() + "/ImpactApp");
                if (!impactDir.exists()) {
                    impactDir.mkdir();
                }
                String filename = eventTitle.replace(" ", "_").replace("\"", "'");
                fos = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/ImpactApp/Event_Map_" + filename + ".jpg");
                if (fos != null) {
                    tempBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.close();
                }
                Toast.makeText(getApplicationContext(), "The map has been saved.", Toast.LENGTH_SHORT).show();
            }

        } catch (OutOfMemoryError e) {
            Toast.makeText(getApplicationContext(), "Out of memory.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("SaveMapButton", "Exception: " + e.toString());
        } finally {
            if (tempBitmap != null) {
                tempBitmap.recycle();
            }
            if (compass != null) {
                compass.recycle();
            }
        }
    }

}
