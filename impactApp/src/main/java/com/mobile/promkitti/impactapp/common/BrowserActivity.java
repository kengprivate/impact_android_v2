package com.mobile.promkitti.impactapp.common;

import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;

public class BrowserActivity extends ImpactActivity {
	
	private WebView webView;
	private String url;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.browser);

		url = getIntent().getStringExtra("URL");
		Map<String, String> params = new HashMap<String, String>();
		params.put("url", url);
		display();
		webView.clearCache(false);
	}

	public void display() {
		Log.d("URL", "URL:"+url);
		webView = (WebView) findViewById(R.id.webView);
		
		WebSettings webSettings = webView.getSettings();
//		webSettings.setSaveFormData(true);
//		webSettings.setPluginsEnabled(true);
		webSettings.setJavaScriptEnabled(true);
//		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultZoom(ZoomDensity.FAR);
//		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//		webSettings.setSupportMultipleWindows(true);
		webView.invokeZoomPicker();

		webView.setWebViewClient(new WebViewClient() {  


			@Override  
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				view.loadUrl(url);
				return true;
			}


			@Override  
			public void onPageFinished(WebView view, String url)  
			{  
				if (webView!=null) {
					Log.d("PAGE","Finish loading "+webView.getUrl());
				}
			}

			public void onPageStarted(WebView view, String url, Bitmap favicon)
			{
				
			}
		});  

		webView.loadUrl(url);
	}
	
	public void stopButtonListener(View view) {
		webView.stopLoading();
		webView.clearCache(false);
		finish();
	}
	
	public void reloadButtonListener(View view) {
		webView.loadUrl(url);
	}

	public void backButtonListener(View view) {
		webView.goBack();
	}
	
	public void nextButtonListener(View view) {
		webView.goForward();
	}
	
}
