package com.mobile.promkitti.impactapp.promotion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.AmazingAdapter;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.util.NotificationUtil;
import com.mobile.promkitti.impactapp.valueobjects.Event;
import com.mobile.promkitti.impactapp.valueobjects.Promotion;

public class PromotionSectionListAdapter extends AmazingAdapter implements OnItemClickListener, OnScrollListener {

	private DataController dc = DataController.getInstance();
	@SuppressWarnings("rawtypes")
	private List<Pair<String, List>> all = new ArrayList<Pair<String, List>>();
	private ArrayList<Event> items = new ArrayList<Event>();
	private Context context;

	private List<Promotion> promotionList = new ArrayList<Promotion>();
	
	public PromotionSectionListAdapter(Context context) {
		this.context = context;
		String currDateString = convertDateToString(getCurrentDate());
		refresh(context, currDateString);
	}
	

	private void refresh(Context context, String currDateString) {
		
//		// Clear badge
//		NotificationUtil.setPromotionNew(false);
		
		try {
			items.clear();
			all.clear();
			promotionList = dc.getPromotionList();

			// Add all events list to pair list
			all.add(new Pair<String, List>("Promo & Offers", promotionList));
			for (Iterator<Pair<String, List>> i = all.iterator(); i.hasNext();) {
				Pair<String, List> pair = i.next();
				items.addAll(pair.second);
			}
			
		} catch (ImpactException e) {
			Log.e(Constants.TAG_EVENT_LIST_FRAGMENT, e.getMessage());
			Toast.makeText(context, Constants.ERR_CONNECTION_ERROR, Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			Log.e(this.getClass().getName(),e.getMessage()==null?"":e.getMessage());
		}
	}
	
	private Date getCurrentDate() {
		return new Date(System.currentTimeMillis());
	}

	private String convertDateToString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(date);
	}

	
	public void reset() {
		notifyDataSetChanged();
	}
	
	public int getEventListCount() {
		return items.size();
	}
	
	@Override
	public int getCount() {
		int res = 0;
		for (int i = 0; i < all.size(); i++) {
			res += all.get(i).second.size();
		}
		return res;
	}

	@Override
	public Event getItem(int position) {
		int c = 0;
		for (int i = 0; i < all.size(); i++) {
			if (position >= c && position < c + all.get(i).second.size()) {
				return (Event)all.get(i).second.get(position - c);
			}
			c += all.get(i).second.size();
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	
	@Override
	protected void onNextPageRequested(int page) {
		Log.d(TAG, "Got onNextPageRequested page=" + page);
	}
	
	
	@Override
	protected void bindSectionHeader(View view, int position, boolean displaySectionHeader) {
		// no pinned header
	}

	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		View res = convertView;
		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Event event = getItem(position);
		if(event instanceof Promotion ) {
			
			if(NotificationUtil.getPromotionNotificationList().contains(event.getId())) {
				res = vi.inflate(R.layout.item_event_promotion_hilight, parent, false);
			} else {
				res = vi.inflate(R.layout.item_event_promotion, parent, false);
			}
			
			TextView titleText = (TextView) res.findViewById(R.id.titleText);
			TextView dateLocationText = (TextView) res.findViewById(R.id.dateLocationText);
			try {
			titleText.setText(((Promotion)event).getTitle());
			dateLocationText.setText(((Promotion)event).getDateLocation());
        	PromotionLoaderImageView imageView = (PromotionLoaderImageView) res.findViewById(R.id.loaderImageView);
        	imageView.setImageDrawable(((Promotion)event).getId());
			} catch(Exception e) {
				Log.e("",e.toString());
			}
		}
		return res;
	}

	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		// no pinned header
	}

	@Override
	public int getPositionForSection(int section) {
		// no pinned header
		return 0;
	}

	@Override
	public int getSectionForPosition(int position) {
		// no pinned header
		return 0;
	}

	@Override
	public String[] getSections() {
		String[] res = new String[all.size()];
		for (int i = 0; i < all.size(); i++) {
			res[i] = all.get(i).first;
		}
		return res;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent(context, PromotionDetailActivity.class);
		intent.putExtra("INT_ID", ((Promotion)items.get(position)).getInternalId());
		intent.putExtra("ID", ((Promotion)items.get(position)).getId());
		NotificationUtil.getPromotionNotificationList().removeNotification(items.get(position).getId());
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
}