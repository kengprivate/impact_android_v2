package com.mobile.promkitti.impactapp.common;

import java.util.ArrayList;

public class NotificationList extends ArrayList<String> {

	private static final long serialVersionUID = 1L;
	private boolean hasNew = false;

	public boolean addNotification(String id) {
		if (!contains(id)) {
			add(id);
			hasNew = true;
			return true;
		}
		return false;
	}

	public boolean removeNotification(String id) {
		if (contains(id)) {
			remove(id);
			if (size() < 1) {
				hasNew = false;
			}
			return true;
		}
		return false;
	}

	public boolean hasNew() {
		return hasNew;
	}

	public void setNew(boolean isNew) {
		hasNew = isNew;
	}
}
