package com.mobile.promkitti.impactapp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil
{
	private static final String DATE_FORMAT = "yyyyMMdd";
	private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String EVENT_DATETIME_FORMAT = "yyyyMMddHH:mm";
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
	private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATETIME_FORMAT);
	private static final SimpleDateFormat eventDateTimeFormat = new SimpleDateFormat(EVENT_DATETIME_FORMAT);
	
    public synchronized static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
    
    public static Date convertDateStringToDate(String string) throws ParseException {
    	return dateFormat.parse(string);
    }
    
    public static Date convertDateTimeStringToDate(String string) throws ParseException {
    	return dateTimeFormat.parse(string);
    }

    public static String convertDateToString(Date date) throws ParseException {
    	return dateFormat.format(date);
    }
    
    public static Date convertEventDateTimeToString(String string) throws ParseException {
    	return eventDateTimeFormat.parse(string);
    }
    
}