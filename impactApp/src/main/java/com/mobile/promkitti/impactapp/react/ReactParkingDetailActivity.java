package com.mobile.promkitti.impactapp.react;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.GridLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.FragmentImpactActivity;
import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.common.EventImage;
import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.common.EventYoutube;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.Utility;
import com.mobile.promkitti.impactapp.valueobjects.ParkingDetail;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import static com.mobile.promkitti.impactapp.Constants.SERVER_URL;

public class ReactParkingDetailActivity extends FragmentImpactActivity {

    private String id;
    private boolean isDirect;
    private Context context;
    private int fullScreenWidth;
    private int fullScreenHeight;
    private int targetWidth;
    private int targetHeight;
    private int paddingValue = 0;

    private String lat;
    private String lon;
    private String title;
    private String titleColor;
    private String description;
    private String availableTotalParking;
    private ArrayList<EventMedia> mediaList = new ArrayList<EventMedia>();
    private HashMap<String, String> parkingMap = new HashMap<String, String>();
    private DataController dc = DataController.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lat =  getIntent().getStringExtra("LAT");
        lon = getIntent().getStringExtra("LON");
        title = getIntent().getStringExtra("TITLE");
        titleColor = getIntent().getStringExtra("TITLE_COLOR");
        description = getIntent().getStringExtra("DESC");
        availableTotalParking = getIntent().getStringExtra("AVAIL");
        String mediaString = getIntent().getStringExtra("MEDIALIST");
        String parkingLotJson = getIntent().getStringExtra("PL");
        parseParkingLotJson(parkingLotJson);
        parseJSONResponse(mediaString);

        this.context = getApplicationContext();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.parking_details_dialog);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(dm);
        fullScreenWidth = dm.widthPixels;
        fullScreenHeight = dm.heightPixels;

        Drawable placeHolder = ContextCompat.getDrawable(getApplicationContext(), R.drawable.promo_detail_placeholder);
        targetWidth = placeHolder.getIntrinsicWidth();
        targetHeight = placeHolder.getIntrinsicHeight();

        paddingValue = Utility.convertDpToPx(this, 70);
        int height = (int) ((float) targetHeight / (float) targetWidth * (float) (fullScreenWidth - paddingValue));

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setLayoutParams(new FrameLayout.LayoutParams(fullScreenWidth - paddingValue, height));

//        /* sizing the wrapper height */
//        LinearLayout linearLayoutView = (LinearLayout) findViewById(R.id.wrapperLayout);
//        int wd = Utility.convertDpToPx(this, 360);
//        int btHeight = Utility.convertDpToPx(this, 220);
//        Log.i("DM", "fullscren w: "+fullScreenWidth);
//        Log.i("DM", "fullscren h: "+fullScreenHeight);
//        Log.i("DM", "paddingvalue: "+paddingValue);
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(wd, fullScreenHeight - btHeight );
//        params.gravity = Gravity.CENTER;
//        linearLayoutView.setLayoutParams(params);
//        linearLayoutView.setGravity(Gravity.CENTER);


        FlurryUtil.onPageView();
        FlurryUtil.logEvent(FlurryConstants.PAGE_PARKING_DETAIL);
        new LoadingTask().execute();

        if(title.contains("(P3)")) {
            final Handler handler = new Handler();
            Timer timer = new Timer();
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        public void run() {
                            try {
                                new LoadParkingTask().execute();
                            } catch (Exception e) {
                                // error, do something
                            }
                        }
                    });
                }
            };
            timer.schedule(task, 0, 10 * 1000);  // interval of one minute
        }
    }

    private void parseParkingLotJson(String parkingLotJson) {
        if(parkingLotJson == null) {
            return;
        }
        try {
            JSONObject obj = new JSONObject(parkingLotJson).getJSONObject("NativeMap");
            for (Iterator i = obj.keys(); i.hasNext();) {
                String key = (String) i.next();
                String val = obj.getString(key);
                parkingMap.put(key, val);
            }
        } catch(JSONException e) {
            Log.e("JSON", e.getMessage());
        }
    }

    private void createP1ParkingLayout() {
        if(this.title.contains("(P1)")) {
            GridLayout layout = (GridLayout) findViewById(R.id.P1ParkingLayout);
            layout.setVisibility(View.VISIBLE);
            createP1Box(layout, "#ed7e38", "zone\n1-1", "11");
            createP1Box(layout, "#ed7e38", "zone\n1-2", "102");
            createP1Box(layout, "#75acd8", "zone\n2-1", "21");
            createP1Box(layout, "#75acd8", "zone\n2-2", "202");
            createP1Box(layout, "#ec809d", "zone\n3-1", "31");
            createP1Box(layout, "#ec809d", "zone\n3-2", "Full");
        }
    }

    private void createP1Box(GridLayout gridLayout, String bgColorCode, String zone, String available) {
        // Create Layout Wrapper Box
        LinearLayout.LayoutParams layoutBoxWrapperParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearWrapperLayout = new LinearLayout(this);
        linearWrapperLayout.setGravity(Gravity.CENTER);
        linearWrapperLayout.setLayoutParams(layoutBoxWrapperParams);
        linearWrapperLayout.setPadding(Utility.convertDpToPx(this, 1), Utility.convertDpToPx(this, 1), Utility.convertDpToPx(this, 1), Utility.convertDpToPx(this, 1));
        gridLayout.addView(linearWrapperLayout);

        // Create Layout Box
        LinearLayout.LayoutParams layoutBoxParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setMinimumWidth(Utility.convertDpToPx(this, 55));
        linearLayout.setBackgroundColor(Color.parseColor(bgColorCode));
        linearLayout.setPadding(Utility.convertDpToPx(this,2), Utility.convertDpToPx(this,4), Utility.convertDpToPx(this,2), Utility.convertDpToPx(this,4));
        linearLayout.setLayoutParams(layoutBoxParams);
        linearWrapperLayout.addView(linearLayout);

        // Create Desc TextView
        LinearLayout.LayoutParams descTextParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        descTextParams.setMargins(Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2));
        TextView descText = new TextView(this);
        descText.setLayoutParams(descTextParams);
        descText.setText(zone);
        descText.setLineSpacing(0f, 0.9f);
        descText.setPadding(Utility.convertDpToPx(this,2), Utility.convertDpToPx(this,1), Utility.convertDpToPx(this,2), Utility.convertDpToPx(this,1));
        descText.setTextColor(Color.WHITE);
        descText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        descText.setGravity(Gravity.CENTER);
        linearLayout.addView(descText);

        // Create Availability TextView
        LinearLayout.LayoutParams zoneTextParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        zoneTextParams.setMargins(Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2));
        TextView zoneText = new TextView(this);
        zoneText.setLayoutParams(zoneTextParams);
        zoneText.setText(available);
        zoneText.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_whitebox));
        zoneText.setTextColor(Color.parseColor("#058A39"));
        zoneText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        zoneText.setTypeface(null, Typeface.BOLD);
        zoneText.setPadding(0, Utility.convertDpToPx(this, 3), 0, Utility.convertDpToPx(this, 3));
        zoneText.setGravity(Gravity.CENTER);
        linearLayout.addView(zoneText);
    }

    private void createP2ParkingLayout() {
        if(this.title.contains("(P2)")) {
            GridLayout layout = (GridLayout) findViewById(R.id.P2ParkingLayout);
            layout.setVisibility(View.VISIBLE);
            createP2P3Box(layout, "#2a74b4", "F1", "11");
            createP2P3Box(layout, "#4e8ac3", "FM", "102");
            createP2P3Box(layout, "#4e8ac3", "F2", "21");
            createP2P3Box(layout, "#2a74b4", "F3", "202");
            createP2P3Box(layout, "#2a74b4", "F4", "31");
            createP2P3Box(layout, "#4e8ac3", "F5", "Full");
            createP2P3Box(layout, "#4e8ac3", "F6", "Full");
            createP2P3Box(layout, "#2a74b4", "F7", "Full");
            createP2P3Box(layout, "#2a74b4", "F8", "Full");
            createP2P3Box(layout, "#4e8ac3", null, null);
        }
    }

    private void createP3ParkingLayout() {
        if(this.title.contains("(P3)")) {
            GridLayout layout = (GridLayout) findViewById(R.id.P3ParkingLayout);
            layout.setVisibility(View.VISIBLE);
            createP2P3Box(layout, "#2a74b4", "F1", parkingMap.get("F1"));
            createP2P3Box(layout, "#4e8ac3", "FM", parkingMap.get("FM"));
            createP2P3Box(layout, "#4e8ac3", "F2", parkingMap.get("F2"));
            createP2P3Box(layout, "#2a74b4", "F3", parkingMap.get("F3"));
            createP2P3Box(layout, "#2a74b4", "F4", parkingMap.get("F4"));
            createP2P3Box(layout, "#4e8ac3", "F5", parkingMap.get("F5"));
            createP2P3Box(layout, "#4e8ac3", "F6", parkingMap.get("F6"));
            createP2P3Box(layout, "#2a74b4", "F7", parkingMap.get("F7"));
            createP2P3Box(layout, "#2a74b4", "F8", parkingMap.get("F8"));
            createP2P3Box(layout, "#4e8ac3", "F9", parkingMap.get("F9"));
        }
    }

    private void createP2P3Box(GridLayout gridLayout, String bgColorCode, String zone, String available) {
        // Create Layout Wrapper Box
        LinearLayout.LayoutParams layoutBoxWrapperParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearWrapperLayout = new LinearLayout(this);
        linearWrapperLayout.setGravity(Gravity.CENTER);
        linearWrapperLayout.setLayoutParams(layoutBoxWrapperParams);
        linearWrapperLayout.setPadding(Utility.convertDpToPx(this, 1), Utility.convertDpToPx(this, 1), Utility.convertDpToPx(this, 1), Utility.convertDpToPx(this, 1));
        gridLayout.addView(linearWrapperLayout);

        // Create Layout Box
        LinearLayout.LayoutParams layoutBoxParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setMinimumWidth(Utility.convertDpToPx(this, 130));
        linearLayout.setMinimumHeight(Utility.convertDpToPx(this, 40));
        linearLayout.setBackgroundColor(Color.parseColor(bgColorCode));
        linearLayout.setPadding(Utility.convertDpToPx(this,2), Utility.convertDpToPx(this,4), Utility.convertDpToPx(this,2), Utility.convertDpToPx(this,4));
        linearLayout.setLayoutParams(layoutBoxParams);
        linearWrapperLayout.addView(linearLayout);

        // Create Desc TextView
        if(zone != null) {
            LinearLayout.LayoutParams descTextParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            descTextParams.setMargins(Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2));
            TextView descText = new TextView(this);
            descText.setLayoutParams(descTextParams);
            descText.setText(zone);
            descText.setMinimumWidth(Utility.convertDpToPx(this, 40));
            descText.setPadding(Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 1), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 1));
            descText.setTextColor(Color.WHITE);
            descText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            descText.setGravity(Gravity.CENTER);
            linearLayout.addView(descText);
        }

        // Create Availability TextView
        if(available != null) {
            LinearLayout.LayoutParams zoneTextParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            zoneTextParams.setMargins(Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2), Utility.convertDpToPx(this, 2));
            TextView zoneText = new TextView(this);
            zoneText.setLayoutParams(zoneTextParams);
            zoneText.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_whitebox));
            if(available.equals("0")) {
                zoneText.setText("Full");
                zoneText.setTextColor(Color.parseColor("#CE1F28"));
            } else {
                zoneText.setText(available);
                zoneText.setTextColor(Color.BLACK);
            }
            zoneText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            zoneText.setTypeface(null, Typeface.BOLD);
            zoneText.setMinimumWidth(Utility.convertDpToPx(this, 55));
            zoneText.setPadding(0, Utility.convertDpToPx(this, 3), 0, Utility.convertDpToPx(this, 3));
            zoneText.setGravity(Gravity.CENTER);
            linearLayout.addView(zoneText);
        }
    }

    private void parseJSONResponse(String mediaString) {
        try {
            JSONArray jsonMediaList = new JSONArray(mediaString);
            for (int i = 0; i < jsonMediaList.length(); i++) {
                JSONObject obj = jsonMediaList.getJSONObject(i);
                if("i".equalsIgnoreCase(obj.getString("t"))) {
                    EventMedia em = new EventImage();
                    em.setType("i");
                    em.setUrl(obj.getString("u"));
                    mediaList.add(em);
                } else {
                    EventMedia em = new EventYoutube();
                    em.setType(obj.getString("t"));
                    em.setUrl(obj.getString("u"));
                    mediaList.add(em);
                }
            }
        } catch(JSONException e) {
            Log.e("JSON", e.getMessage());
        }
    }

    public void directionButtonListener(View view) {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            return;
        }


        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location == null) {
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        String currentLocation = "";
        if (location != null) {
            currentLocation = location.getLatitude() + "," + location.getLongitude();
        }

        String destinationLocation = "IMPACT Muang Thong Thani @"+lat + ","+lon;
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?f=d&saddr=" + currentLocation + "&daddr=" + destinationLocation));
        startActivity(intent);

        FlurryUtil.logEvent(FlurryConstants.EVENT_DIRECTIONS_TO_PARKING);
		/*
		 * https://maps.google.com/maps?saddr=Current+Location&daddr=IMPACT+Muang + Thong+Thani+%4013.91249,100.547905&hl=en&sll=13.91249,100.547905&sspn
		 * =0.008769,0.013937&geocode=FZbh0gAdhgUABinzYQ0oMmAdMTEgSOJdsgABAQ%3 BFapJ1AAdQT3-BQ&mra=pd&t=m&z=18
		 */
        finish();
    }

    public void cancelButtonListener(View view) {
        finish();
    }

    private class LoadingTask extends AsyncTask<Void, Void, Integer> {
        /**
         * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
         */
        protected Integer doInBackground(Void... val) {
            return 0;
        }

        /**
         * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
         */
        protected void onPostExecute(Integer i) {

            // Set the pager with an adapter
            ParkingDetailPagerAdapter pagerAdapter = new ParkingDetailPagerAdapter(getSupportFragmentManager(), mediaList);
            final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
            viewPager.setAdapter(pagerAdapter);

            // Bind the title indicator to the adapter
            CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
            pageIndicator.setViewPager(viewPager);

            if (mediaList.size() < 2) {
                pageIndicator.setVisibility(View.GONE);
            }

            /* if the screen is small, resize dialog components width, height */
//            int preferredBoxWidth = Utility.convertDpToPx(context, 360);
//            int boxWidth = (int) (0.9 * fullScreenWidth);
//            if( preferredBoxWidth > boxWidth ) {
//                /* Set dialog layout */
//                int preferredBoxHeight = (int) (0.65 * fullScreenHeight);
//                LinearLayout wrapperLayout = (LinearLayout) findViewById(R.id.wrapperLayout);
//                LinearLayout.LayoutParams llparam = new LinearLayout.LayoutParams(boxWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
//                llparam.gravity = Gravity.CENTER_HORIZONTAL;
//                wrapperLayout.setLayoutParams(llparam);
//
//                /* Set scrollView Layout */
//                ScrollView scrollView = (ScrollView) findViewById(R.id.scrollViewLayout);
//                LinearLayout.LayoutParams scrlParam = new LinearLayout.LayoutParams(boxWidth, preferredBoxHeight);
//                scrlParam.gravity = Gravity.CENTER_HORIZONTAL;
//                scrollView.setLayoutParams(scrlParam);
//
//                /* Set direction button layout */
//                int buttonHeight = Utility.convertDpToPx(context, 50);
//                int preferredMarginTop = Utility.convertDpToPx(context, 5);
//
//                LinearLayout.LayoutParams buttonparam = new LinearLayout.LayoutParams(boxWidth, buttonHeight);
//                buttonparam.gravity = Gravity.CENTER;
//                buttonparam.topMargin = preferredMarginTop;
//
//                FrameLayout directionLayout = (FrameLayout) findViewById(R.id.directionFrameLayout);
//                directionLayout.setLayoutParams(buttonparam);
//
//                FrameLayout cancelLayout = (FrameLayout) findViewById(R.id.cancelFrameLayout);
//                cancelLayout.setLayoutParams(buttonparam);
//            }


            TextView titleText = (TextView) findViewById(R.id.parkingDetailTitleText);
            titleText.setText(title);
            titleText.setTextColor(Color.parseColor("#"+titleColor));
            registerForContextMenu(titleText);

            TextView descText = (TextView) findViewById(R.id.parkingDetailDescriptionText);
            descText.setText(description);

            //TODO: for this version, display availability only P3
            if(title.contains("(P3)")) {
                TextView availableTotalParkingText = (TextView) findViewById(R.id.totalAvailableParkingText);
                availableTotalParkingText.setText(availableTotalParking);
                availableTotalParkingText.setTextColor(Color.parseColor("#058A39"));
            } else {
                ((LinearLayout) findViewById(R.id.totalAvailableParkingLayout)).setVisibility(View.GONE);
            }

            //TODO: next version to enable available parking indiciator.
//            createP1ParkingLayout();
//            createP2ParkingLayout();
            createP3ParkingLayout();

            registerForContextMenu(descText);

        }

    }



    private class LoadParkingTask extends AsyncTask<Void, Void, ParkingDetail> {
        /**
         * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
         */
        protected ParkingDetail doInBackground(Void... val) {
            ParkingDetail p;
            try {
                p = dc.getParkingP3();
            } catch (ImpactException e) {
                Log.e(getClass().getName(), e.getMessage());
                return null;
            }
            return p;
        }


        /**
         * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
         */
        protected void onPostExecute(ParkingDetail p) {

            if(p != null) {
                availableTotalParking = p.getAvailableParking();
                parkingMap = p.getParkingLot();

                TextView availableTotalParkingText = (TextView) findViewById(R.id.totalAvailableParkingText);
                availableTotalParkingText.setText(availableTotalParking);
                availableTotalParkingText.setTextColor(Color.parseColor("#058A39"));
                GridLayout layout = (GridLayout) findViewById(R.id.P3ParkingLayout);
                layout.setVisibility(View.VISIBLE);
                layout.removeAllViewsInLayout();

                createP3ParkingLayout();
            }
        }

    }
}
