package com.mobile.promkitti.impactapp.events.detail;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.ActivityParameters;
import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.util.Utility;

public class OrganizerInfoActivity extends ImpactActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.organizer_info);

//		View view = findViewById(R.id.titlebarLayout);
//		Utility.fixBackgroundRepeat(view);
		View view = findViewById(R.id.organizerInfoLayout);
		Utility.fixBackgroundRepeat(view);
		
//		String id = getIntent().getStringExtra(ActivityParameters.ID);
		String orgInfo = getIntent().getStringExtra(ActivityParameters.ORG_INFO);
		
		TextView organizerInfoText = (TextView) findViewById(R.id.organizerInfoText);
		organizerInfoText.setText(orgInfo);
	}
}
