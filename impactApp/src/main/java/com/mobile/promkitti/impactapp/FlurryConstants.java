package com.mobile.promkitti.impactapp;



public class FlurryConstants {
	
	/* PARAMETERS */
	public static final String PARAM_AD_ID = "ad_id";
	public static final String PARAM_EVENT_ID = "event_id";
	public static final String PARAM_PROMO_ID = "promo_id";

	/**/public static final String PAGE_AD_DETAIL = "Page: Ad Detail.";  /* did not see it yet **/
	/**/public static final String PAGE_AD_LIST = "Page: Ads List.";
	/**/public static final String PAGE_EVENT_DETAIL = "Page: Event Detail."; /* did not see it yet **/
	/**/public static final String PAGE_EVENT_LIST = "Page: Event List."; /* did not see it yet **/
	/**/public static final String PAGE_FB_FAN_PAGE = "Page: Facebook Fan Page."; /* did not see it yet **/
	/**/public static final String PAGE_CALENDAR = "Page: Calendar.";
	/**/public static final String PAGE_MORE = "Page: More.";
	/**/public static final String PAGE_PROMO_DETAIL = "Page: Promo Detail.";
	/**/public static final String PAGE_PROMO_LIST = "Page: Promo List.";
	/**/public static final String PAGE_LANGUAGE_OPTION = "Page: Language Option.";
	/**/public static final String OPEN_AD_LIST = "Open Ad List.";
	/**/public static final String OPEN_EVENT_FROM_EVENT_LIST = "Open Event from Event List.";
	/**/public static final String OPEN_EVENT_FROM_CALENDAR = "Open Event from Calendar.";
	/**/public static final String VIEW_AD_ID = "View Ad Id :";
	/**/public static final String SHARE_AD = "Share Ad :";
	/**/public static final String SHARE_PROMO = "Share Promotion :";
//	/**/public static final String SHARE_AD_VIA_EMAIL = "Share Ad via Email :";
	/**/public static final String SHARE_FB_AD_ID = "Share FB Ad Id :";
	/**/public static final String VIEW_EVENT_ID = "View Event Id :";
	/**/public static final String SHARE_EVENT = "Share Event :";
	/**/public static final String SHARE_EVENT_VIA_EMAIL = "Share Event via Email :";
	/**/public static final String SHARE_FB_EVENT_ID = "Share FB Event Id:";
	/**/public static final String FB_LOGIN = "FB Login.";
	/**/public static final String FB_LOGOUT = "FB Logout.";
	/**/public static final String LANG_SWITCH_TO_THAI = "Lang switch to Thai.";
	/**/public static final String LANG_SWITCH_TO_ENG = "Lang switch to English.";
	/**/public static final String MORE_FEEDBACK_LAUNCHED = "More: Feedback launched.";
	/**/public static final String MORE_IMPACT_WEB_LAUNCHED = "More: IMPACT website launched.";
	/**/public static final String MORE_MAPS_LAUNCHED = "More: Google Map launched.";
	/**/public static final String MORE_FACEBOOK_LOGIN_LAUNCHED = "More: Facebook Login launched.";
	/**/public static final String VIEW_PROMO_ID = "View Promo Id :";
	/**/public static final String RF_SHARE_TO_FRIENDS = "RF: Share to friends";
	/**/public static final String RF_POST_ON_MY_FB_WALL = "RF: Post on my FB Wall.";
	/**/public static final String RF_RATE_IMPACT_APP = "RF: Rate IMPACT app.";
	/**/public static final String RF_EMAIL = "RF: Email.";
	/**/public static final String EVENT_TICKET_URL = "Event: Ticket URL.";
	/**/public static final String EVENT_TICKET_TEL = "Event: Ticket Tel.";
	/**/public static final String EVENT_URL = "Event: URL.";
	/**/public static final String EVENT_DIRECTIONS_TO_EVENT = "Event: Directions to Event.";
	/**/public static final String EVENT_BUILDINGS = "Event: Buildings.";
	/**/public static final String EVENT_ORG_INFO = "Event: Org Info.";
	/**/public static final String EVENT_SHARE_FB = "Event: Share FB.";
	/**/public static final String EVENT_EMAIL = "Event: Email.";
	/**/public static final String EVENT_VIEW_ON_FB = "Event: View on FB."; /* did not see it yet **/
	/**/public static final String EVENT_ADDED_TO_CALENDAR = "Event: Added to Calendar.";
	/**/public static final String AD_CALL = "Ad: Call."; 
	/**/public static final String AD_LINK_TO_WEB = "Ad: Link to Web.";
	/**/public static final String AD_SHARE_FB = "Ad: Share FB.";
	/**/public static final String AD_EMAIL = "Ad: Email.";
	/**/public static final String PUSH_OPEN_APP = "Push: Open App."; 
	/**/public static final String PUSH_OPEN_PROMOTIONS = "Push: Open Promotions."; 
	/**/public static final String PUSH_OPEN_PROMOTION_ID = "Push: Open Promotion Id :"; 
	/**/public static final String PUSH_OPEN_EVENTS = "Push: Open Events."; 
	/**/public static final String PUSH_OPEN_EVENT_ID = "Push: Open Event Id :"; 
	/**/public static final String PUSH_OPEN_ADS = "Push: Open Ads."; 
	/**/public static final String PUSH_OPEN_AD_ID = "Push: Open Ad Id :";
	/**/public static final String SOCIAL_FB = "Social: Open Facebook";
	/**/public static final String SOCIAL_IG = "Social: Open Instragram";

	/**/public static final String PAGE_PARKING_DETAIL = "Page: Parking Detail";
	/**/public static final String EVENT_DIRECTIONS_TO_PARKING = "Event: Direction to Parking Google Maps";

	private FlurryConstants() {
		
	}
	

}
