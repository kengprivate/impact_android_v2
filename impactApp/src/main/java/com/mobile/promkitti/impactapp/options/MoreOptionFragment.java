package com.mobile.promkitti.impactapp.options;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.Utility;

public class MoreOptionFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.more_options, container, false);
		
//		View view = v.findViewById(R.id.titlebarLayout);
//		Utility.fixBackgroundRepeat(view);
		View view = v.findViewById(R.id.moreOptionLayout);
		Utility.fixBackgroundRepeat(view);
		
		return v;
	}
	

	
	public void languageButtonListener(View view) {
//		view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		Intent intent = new Intent(getActivity(), LanguageOptionActivity.class);
    	startActivity(intent);
	}
	
	
	public void feedBackButtonListener(View view) {
		String aEmailList[] = { "appfeedback@impact.co.th" };  
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);  
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);  
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback via IMPACT App");  
		emailIntent.setType("plain/text");  
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "\n\nSent from my Android");  
		  
		startActivity(emailIntent);  
		
        FlurryUtil.logEvent(FlurryConstants.MORE_FEEDBACK_LAUNCHED);
	}
	
	
	public void websiteButtonListener(View view) {
		String url = "http://www.impact.co.th/";
		Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(myIntent);
		
        FlurryUtil.logEvent(FlurryConstants.MORE_IMPACT_WEB_LAUNCHED);
	}
	
	public void mapsButtonListener(View view) {

		if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
			requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
		} else {
			Constants.setLocationPermissionAllowed(true);
			gotoMaps();
		}

	}

	private void gotoMaps() {
		LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		String currentLocation = "";
		if(location!=null) {
			currentLocation = location.getLatitude()+","+location.getLongitude();
		}
		String destinationLocation = "IMPACT Muang Thong Thani @13.91249,100.547905";
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?f=d&saddr=" + currentLocation + "&daddr=" + destinationLocation));

		startActivity(intent);

		FlurryUtil.logEvent(FlurryConstants.MORE_MAPS_LAUNCHED);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if(grantResults[0] < 0) {
			Log.i("IMPACT", "Permission DENIED");
			Constants.setLocationPermissionAllowed(false);
		} else {
			// Permission Granted
			Log.i("IMPACT", "Permission Granted");
			Constants.setLocationPermissionAllowed(true);
			gotoMaps();
		}
	}


	public void facebookLoginButtonListener(View view) {
//		view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		Intent intent = new Intent(getActivity(), FacebookLoginOptionActivity.class);
    	startActivity(intent);
	}
	
	
	public void recommendFriendsButtonListener(View view) {
		Intent myIntent = new Intent(getActivity(), RecommendFriendsOptionActivity.class);
		startActivity(myIntent);
	}

}
