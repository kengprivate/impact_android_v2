package com.mobile.promkitti.impactapp.react;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.util.FlurryUtil;

/**
 * Created by nananetta on 7/10/16.
 */
public class ReactNavigatorModule extends ReactContextBaseJavaModule  {

    private ReactApplicationContext reactContext;
    public ReactNavigatorModule(ReactApplicationContext reactContext) {

        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "RNViewController";
    }

    @ReactMethod
    public void pushToViewName(String name) {
//        Toast.makeText(getReactApplicationContext(), name, Toast.LENGTH_SHORT).show();
        Activity activity = getCurrentActivity();
        if("Parking".equalsIgnoreCase(name)) {
            Intent intent = new Intent(getReactApplicationContext(), ReactParkingActivity.class);
            getCurrentActivity().startActivity(intent);
        } else if("Traffic".equalsIgnoreCase(name)) {
            Intent intent = new Intent(getReactApplicationContext(), ReactTrafficActivity.class);
            getCurrentActivity().startActivity(intent);
        } else if("Transportation".equalsIgnoreCase(name)) {
            Intent intent = new Intent(getReactApplicationContext(), ReactTransportationActivity.class);
            getCurrentActivity().startActivity(intent);
        }
    }

    @ReactMethod
    public void pushToParkingDetails(String lang, String lat, String lon, ReadableMap details, String availableTotalParking) {
        if(getCurrentActivity()==null) {
            Log.w("PARKING DETAILS!!!", "NO ACTIVITY!!!");
            return;
        }
        Log.i("PARKING DETAILS!!!", "lang: "+lang+" lat:"+lat + "  lon:"+lon + " at:" + availableTotalParking + "  details: "+details);
        Intent intent = new Intent(getReactApplicationContext(), ReactParkingDetailActivity.class);
        intent.putExtra("LAT", lat);
        intent.putExtra("LON", lon);
        intent.putExtra("TITLE", details.getString("t"));
        intent.putExtra("TITLE_COLOR", details.getString("tc"));
        intent.putExtra("DESC", details.getString("d"));
        if(details.hasKey("pl")) {
            intent.putExtra("PL", details.getMap("pl").toString());
        }
        intent.putExtra("MEDIALIST", details.getArray("m").toString());
        if(null != availableTotalParking && !"".equals(availableTotalParking)) {
            intent.putExtra("AVAIL", availableTotalParking);
        }
        getCurrentActivity().startActivity(intent);
    }


    @ReactMethod
    public void logEvent(String message) {
        FlurryUtil.logEvent(message);
    }


    @ReactMethod
    public void checkLocationPermission(Callback callback) {
        callback.invoke(Constants.isLocationPermissionAllowed());
    }
}
