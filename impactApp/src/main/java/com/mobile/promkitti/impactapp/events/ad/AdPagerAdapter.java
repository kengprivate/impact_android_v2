package com.mobile.promkitti.impactapp.events.ad;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.events.detail.YoutubeFragment;

public class AdPagerAdapter extends FragmentStatePagerAdapter {
	private ArrayList<EventMedia> mediaList;

	public AdPagerAdapter(FragmentManager fm, ArrayList<EventMedia> mediaList) {
		super(fm);
		this.mediaList = mediaList;
	}

	@Override
	public int getCount() {
		return mediaList.size();
	}

	@Override
	public Fragment getItem(int position) {
		EventMedia media = mediaList.get(position);
		String type = media.getType().toLowerCase();
		if ("y".equals(type)) {
			return YoutubeFragment.newInstance(media);
		}
		return AdImageFragment.newInstance(media);
	}
}