package com.mobile.promkitti.impactapp.events.list;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.AmazingAdapter;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.events.ad.AdListActivity;
import com.mobile.promkitti.impactapp.events.detail.EventDetailsActivity;
import com.mobile.promkitti.impactapp.react.ReactTrafficActivity;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.NotificationUtil;
import com.mobile.promkitti.impactapp.valueobjects.AdBanner;
import com.mobile.promkitti.impactapp.valueobjects.CalendarEvent;
import com.mobile.promkitti.impactapp.valueobjects.Event;
import com.mobile.promkitti.impactapp.valueobjects.EventListObject;
import com.mobile.promkitti.impactapp.valueobjects.Traffic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SectionListAdapter extends AmazingAdapter implements OnItemClickListener, OnScrollListener {

	private DataController dc = DataController.getInstance();
	private List<Pair<String, List>> allSectionPairList = new ArrayList<Pair<String, List>>();
	private ArrayList<Event> allItemList = new ArrayList<Event>();
	private static Activity context;

	private static AdBanner currentAd = new AdBanner();
	private static ArrayList<AdBanner> adList = new ArrayList<AdBanner>();

	/* Variables for Thread */
	private static Thread adTask;
	private static LoaderAdView adView;
	private static int currentAdIndex = 0;
	private static boolean isRunning = false;
	private long startTimestamp;
	private long adjustTime = 0;
	private long waitTime;
	private boolean isFirstOut = true;
	private AsyncTask<Integer, Void, Pair<String, List>> backgroundTask;
	private ArrayList<CalendarEvent> todayList;
	private ArrayList<CalendarEvent> upcomingList;
	private SharedPreferences mPrefs;
	private CalendarEvent event;
	private CalendarEvent updateEvent;

	public SectionListAdapter(Activity activity) {
		context = activity;
		mPrefs = context.getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);
		isRunning = true;
		String currDateString = convertDateToString(getCurrentDate());
		refresh(activity, currDateString);
	}

	private void refresh(Activity context, String currDateString) {
		try {
			EventListObject eventListObject = dc.getEventList(currDateString);
			adList = eventListObject.getAdList();

			// Create Ads list
			ArrayList<AdBanner> adList = new ArrayList<AdBanner>();
			AdBanner ads = new AdBanner();
			adList.add(ads);

			// Create Traffic List
			ArrayList<Traffic> trafficList = new ArrayList<>();
			trafficList.add(dc.getTraffic());

			// Create Todays Events list, Upcoming Event List
			todayList = new ArrayList<CalendarEvent>();
			upcomingList = new ArrayList<CalendarEvent>();
			for (Iterator<CalendarEvent> i = eventListObject.getEventList().iterator(); i.hasNext();) {
				event = i.next();

				// verify event timestamp against persisted data
				String eventCalendarInfo = mPrefs.getString(event.getInternalId(), null);
				if (eventCalendarInfo != null) {
					String str[] = eventCalendarInfo.split(":");
					long timestamp = Long.parseLong(str[1]);
					if (event.getTimestamp() > timestamp) {

						// update persisted data
						SharedPreferences.Editor editor = mPrefs.edit();
						StringBuilder sb = new StringBuilder();
						sb.append(event.getEndDate());
						sb.append(":");
						sb.append(Long.toString(event.getTimestamp()));
						String updatedEventCalendarInfo = sb.toString();
						editor.putString(event.getInternalId(), updatedEventCalendarInfo);
						editor.commit();

						AlertDialog.Builder builder = new AlertDialog.Builder(context);
						updateEvent = event;
						String titleMsg = String.format(context.getString(R.string.calendar_change_notification_message), event.getTitle());
						builder.setMessage(titleMsg);
						builder.setPositiveButton(context.getString(R.string.calendar_change_notification_view_button), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent intent = new Intent(SectionListAdapter.context, EventDetailsActivity.class);
								intent.putExtra("INT_ID", (updateEvent.getInternalId()));
								intent.putExtra("ID", (updateEvent.getId()));
								SectionListAdapter.context.startActivity(intent);
							}
						}).setNegativeButton(context.getString(R.string.calendar_change_notification_close_button), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
						builder.create().show();
					}
				}

				if (isTodayEvent(event.getStartDate(), event.getEndDate())) {
					todayList.add(event);
				} else {
					upcomingList.add(event);
				}
			}

			// Add all events list to pair list
			allSectionPairList.add(new Pair<String, List>("ADS", adList));
			allSectionPairList.add(new Pair<String, List>(context.getResources().getString(R.string.traffic_header_title), trafficList));
			allSectionPairList.add(new Pair<String, List>("Today Events", todayList));
			allSectionPairList.add(new Pair<String, List>("Upcoming Events", upcomingList));

			for (Iterator<Pair<String, List>> i = allSectionPairList.iterator(); i.hasNext();) {
				Pair<String, List> pair = i.next();
				allItemList.addAll(pair.second);
			}

		} catch (ImpactException e) {
			Log.e(Constants.TAG_EVENT_LIST_FRAGMENT, e.getMessage());
			Toast.makeText(context, Constants.ERR_CONNECTION_ERROR, Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
		}
	}

	private Date getCurrentDate() {
		return new Date(System.currentTimeMillis());
	}

	private boolean isTodayEvent(String startDateStr, String endDateStr) throws ParseException {
		String todayDateString = convertDateToString(getCurrentDate());
		long todayDate = Long.parseLong(todayDateString);
		long startDate = Long.parseLong(startDateStr);
		long endDate = Long.parseLong(endDateStr);
		if (todayDate > endDate || todayDate < startDate) {
			return false;
		} else {
			return true;
		}
	}

	private String convertDateToString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(date);
	}

	public void reset() {
		if (backgroundTask != null)
			backgroundTask.cancel(false);
		notifyDataSetChanged();
	}

	public int getEventListCount() {
		return allItemList.size();
	}

	@Override
	public int getCount() {
		int res = 0;
		for (int i = 0; i < allSectionPairList.size(); i++) {
			res += allSectionPairList.get(i).second.size();
		}
		return res;
	}

	@Override
	public Event getItem(int position) {
		int c = 0;
		for (int i = 0; i < allSectionPairList.size(); i++) {
			if (position >= c && position < c + allSectionPairList.get(i).second.size()) {
				return (Event) allSectionPairList.get(i).second.get(position - c);
			}
			c += allSectionPairList.get(i).second.size();
		}
		return null;
	}

	private int getTodayListSize() {
		return allSectionPairList.get(2).second.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	protected void onNextPageRequested(int page) {
		if (backgroundTask != null) {
			backgroundTask.cancel(false);
		}

		backgroundTask = new AsyncTask<Integer, Void, Pair<String, List>>() {
			@Override
			protected Pair<String, List> doInBackground(Integer... params) {
				return null;
			}

			@Override
			protected void onPostExecute(Pair<String, List> result) {
				if (isCancelled())
					return;
				notifyMayHaveMorePages();
			};
		}.execute(page);
	}

	@Override
	protected void bindSectionHeader(View view, int position, boolean displaySectionHeader) {
		if (displaySectionHeader) {
			view.findViewById(R.id.header).setVisibility(View.VISIBLE);
			TextView lSectionTitle = (TextView) view.findViewById(R.id.header);
			lSectionTitle.setText(getSections()[getSectionForPosition(position)]);
			String title = lSectionTitle.getText().toString();
//			if (context.getResources().getString(R.string.traffic_header_title).equals(title)) {
//				lSectionTitle.setText(context.getResources().getString(R.string.traffic_header_title));
//			} else
			if ("Today Events".equals(title)) {
				lSectionTitle.setBackgroundColor(context.getResources().getColor(R.color.today_event_title_bg));
				lSectionTitle.setTextColor(context.getResources().getColor(R.color.today_event_title_color));
			} else if ("Upcoming Events".equals(title)) {
				lSectionTitle.setBackgroundColor(context.getResources().getColor(R.color.upcoming_event_title_bg));
				lSectionTitle.setTextColor(context.getResources().getColor(R.color.upcoming_event_title_color));
			}
		} else {
			if (null != view.findViewById(R.id.header)) {
				view.findViewById(R.id.header).setVisibility(View.GONE);
			}
		}
	}

	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		View res = convertView;
		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (position == 0) {
			res = vi.inflate(R.layout.item_event_noheader, parent, false);

			adView = (LoaderAdView) res.findViewById(R.id.loaderAdView);
			ImageView adBarView = (ImageView) res.findViewById(R.id.adBarView);

			// find appropriate height
			Drawable placeHolder = ContextCompat.getDrawable(context, R.drawable.ad_placeholder);
			int targetWidth = placeHolder.getIntrinsicWidth();
			int targetHeight = placeHolder.getIntrinsicHeight();
			float ratio = (float) targetWidth / (float) targetHeight;

			DisplayMetrics metrics = new DisplayMetrics();
			WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			wm.getDefaultDisplay().getMetrics(metrics);
			int realHeight = (int) ((float) metrics.widthPixels / ratio);
			Log.d("", "new height: " + realHeight);
			FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(metrics.widthPixels, realHeight);
			frameLayoutParams.gravity = Gravity.CENTER;
			adView.setLayoutParams(frameLayoutParams);

			adBarView.setLayoutParams(frameLayoutParams);
			Event event = getItem(position);
			if (event instanceof AdBanner) {
				adView.setImageDrawable(currentAd.getInternalId());
			}

			TextView atv = (TextView) res.findViewById(R.id.adNotificationCount);
			if (NotificationUtil.getAdNotificationList().size() > 0 && NotificationUtil.getAdNotificationList().hasNew()) {
				atv.setText(Integer.toString(NotificationUtil.getAdNotificationList().size()));
				atv.setVisibility(View.VISIBLE);
			} else {
				atv.setText("0");
				atv.setVisibility(View.GONE);
			}
		} else if (position == 1) {
			res = vi.inflate(R.layout.item_traffic, parent, false);
			Event event = getItem(position);
			if (event instanceof Traffic) {
				Traffic trafficObj = (Traffic) event;
				TextView titleText = (TextView) res.findViewById(R.id.titleText);
				TextView trafficText = (TextView) res.findViewById(R.id.trafficText);
				titleText.setText(trafficObj.getTitle());
				trafficText.setText(trafficObj.getDescription());
			}

		} else {
			Event event = getItem(position);
			if (position < getTodayListSize() + 2) {
				if (NotificationUtil.getEventNotificationList().contains(event.getId())) {
					res = vi.inflate(R.layout.item_event_today_hilight, parent, false);
				} else {
					res = vi.inflate(R.layout.item_event_today, parent, false);
				}
			} else {
				if (NotificationUtil.getEventNotificationList().contains(event.getId())) {
					res = vi.inflate(R.layout.item_event_upcoming_hilight, parent, false);
				} else {
					res = vi.inflate(R.layout.item_event_upcoming, parent, false);
				}
			}
			if (event instanceof CalendarEvent) {
				TextView titleText = (TextView) res.findViewById(R.id.titleText);
				TextView dateLocationText = (TextView) res.findViewById(R.id.dateLocationText);
				try {
					titleText.setText(((CalendarEvent) event).getTitle());
					dateLocationText.setText(((CalendarEvent) event).getDateLocation());
					LoaderImageView imageView = (LoaderImageView) res.findViewById(R.id.loaderImageView);
					imageView.setImageDrawable(((CalendarEvent) event).getInternalId());
				} catch (Exception e) {
					Log.e("", e.toString());
				}
			}

		}
		return res;
	}

	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		TextView lSectionHeader = ((TextView) header.findViewById(R.id.header));
		lSectionHeader.setText(getSections()[getSectionForPosition(position)]);
		String title = lSectionHeader.getText().toString();
		if ("Today Events".equals(title)) {
			lSectionHeader.setBackgroundColor(context.getResources().getColor(R.color.today_event_title_bg));
			lSectionHeader.setTextColor(context.getResources().getColor(R.color.today_event_title_color));
		} else if ("Upcoming Events".equals(title)) {
			lSectionHeader.setBackgroundColor(context.getResources().getColor(R.color.upcoming_event_title_bg));
			lSectionHeader.setTextColor(context.getResources().getColor(R.color.upcoming_event_title_color));
		}
	}

	@Override
	public int getPositionForSection(int section) {
		if (section < 0)
			section = 0;
		if (section >= allSectionPairList.size())
			section = allSectionPairList.size() - 1;
		int c = 0;
		for (int i = 0; i < allSectionPairList.size(); i++) {
			if (section == i) {
				return c;
			}
			c += allSectionPairList.get(i).second.size();
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int position) {
		int c = 0;
		for (int i = 0; i < allSectionPairList.size(); i++) {
			if (position >= c && position < c + allSectionPairList.get(i).second.size()) {
				return i;
			}
			c += allSectionPairList.get(i).second.size();
		}
		return -1;
	}

	@Override
	public String[] getSections() {
		String[] res = new String[allSectionPairList.size()];
		for (int i = 0; i < allSectionPairList.size(); i++) {
			res[i] = allSectionPairList.get(i).first;
		}
		return res;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (position == allItemList.size()) {
			if (allItemList.size() != 0) {
				if(allItemList.get(allItemList.size() - 1) instanceof CalendarEvent) {
					Event event = ((CalendarEvent) allItemList.get(allItemList.size() - 1));
					new LoadMoreTask().execute(event.getStartDate(), event.getInternalId());
				} else {
					notifyNoMorePages();
				}
			}
			reset();
		} else if (allItemList.get(position) instanceof CalendarEvent) {
			NotificationUtil.getEventNotificationList().removeNotification(allItemList.get(position).getId());

			Intent intent = new Intent(context, EventDetailsActivity.class);
			intent.putExtra("INT_ID", ((CalendarEvent) allItemList.get(position)).getInternalId());
			intent.putExtra("ID", ((CalendarEvent) allItemList.get(position)).getId());
			context.startActivity(intent);

			FlurryUtil.logEvent(FlurryConstants.OPEN_EVENT_FROM_EVENT_LIST);
		} else if (position == 0) {
			Intent intent = new Intent(context, AdListActivity.class);
			context.startActivity(intent);
			FlurryUtil.logEvent(FlurryConstants.OPEN_AD_LIST);
		} else {
			Intent intent = new Intent(context, ReactTrafficActivity.class);
			context.startActivity(intent);
		}

	}

	class LoadMoreTask extends AsyncTask<String, Void, EventListObject> {
		@Override
		protected EventListObject doInBackground(String... params) {
			try {
				return dc.getMoreEventList(params[0], params[1]);
			} catch (ImpactException e) {
				return null;
			}
		}

		@Override
		protected void onPostExecute(EventListObject result) {
			if (result == null) {
				Log.e(Constants.TAG_EVENT_LIST_FRAGMENT, "Cannot load more events");
				Toast.makeText(context, "Cannot load more events", Toast.LENGTH_SHORT).show();
				return;
			}
			ArrayList<CalendarEvent> upcomingList = result.getEventList();
			// Log.e("LOAD SIZE:", ""+upcomingList.size());
			if (upcomingList.size() == 0) {
				notifyNoMorePages();
			}
			// 2 is Upcoming Event list
			Pair<String, List> pair = allSectionPairList.get(3);
			ArrayList<CalendarEvent> currentUpcomingList = (ArrayList<CalendarEvent>) pair.second;

			for (Iterator iterator = upcomingList.iterator(); iterator.hasNext();) {
				CalendarEvent calendarEvent = (CalendarEvent) iterator.next();
				allItemList.add(calendarEvent);
				currentUpcomingList.add(calendarEvent);
			}

			notifyDataSetChanged();
		};
	}

	public void stopThread() {
		Log.d("Thread", "StopThread");
		isRunning = false;
		if (adTask != null) {
			// isAllow = false;
			adTask.interrupt();
			// lock = 0;
		}
	}

	public void startThread() {
		Log.d("Thread", "StartThread");
		// Log.d("", adTask.getState().toString());
		isRunning = true;
		if (adTask != null && !adTask.isAlive()) {
			Log.d("", "run adTask");
			Log.d("", "state: " + adTask.getState());
			adTask = new SectionListAdapter.AdRotationThread();
			adTask.start();
		}
	}

	private volatile Integer lock = 0;

	/* Don't forget to pause the thread when click on different tabs or change screen */
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

		try {
			if (firstVisibleItem == 0) {
				// Log.d("", "first visible item = 0 && adTask is alive = " + (adTask == null ? "false" : "true") );
				if ((adTask == null || !adTask.isAlive())) {
					Log.d("", "adTask is alive");
					synchronized (lock) {
						adTask = new SectionListAdapter.AdRotationThread();
						adTask.start();
					}
				}
			}
			// Scroll OUT from the 1st row
			else {
				if (adTask != null) {
					// adjust next wait time
					if (isFirstOut) {
						adjustTime = System.currentTimeMillis() - startTimestamp;
						// Log.d("", "clock has ticked for " + adjustTime);
						isRunning = false;
						isFirstOut = false;
						if (!adTask.isInterrupted()) {
							adTask.interrupt();
						}
					}
				}
			}
		} catch (Exception e) {
			Log.e("ADTASK_ONSCROLL", e.getClass().getName() + ": " + e.getMessage());
		}
	}

	private static AdBanner ad;
	private static Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what > 0) {
				if (currentAdIndex == adList.size() - 1) {
					currentAdIndex = 0;
				} else {
					currentAdIndex = currentAdIndex + 1;
				}
			}
			// Log.d("","current AdIndex: "+currentAdIndex);
			ad = adList.get(currentAdIndex);
			if (adView != null) {
				// context.runOnUiThread(new Runnable() {
				// EventListFragment.this.getActivity().runOnUiThread(new Runnable() {
				// public void run() {
				adView.setImageDrawable(ad.getInternalId());
				// }
				// });
				// Log.d("","Ad changed to "+currentAdIndex);
			}
		}
	};

	class AdRotationThread extends Thread {
		public void run() {
			try {
				isRunning = true;
				currentAd = adList.get(currentAdIndex);
				// Log.d("", "current Ad index is " + currentAdIndex);
				handler.sendEmptyMessage(0); // 0 = to show current Ad, 1 = to show next ad
				// Log.d("", "sent request to change ad banner");

				while (isRunning) {
					if (!adView.isLoaded())
						continue;
					currentAd = adList.get(currentAdIndex);
					// Log.d("", "current Ad index is " + currentAdIndex + " " + currentAd.getId());
					startTimestamp = System.currentTimeMillis();
					isFirstOut = true;
					waitTime = Long.parseLong(currentAd.getDisplayTime()) * 1000 - adjustTime;
					adjustTime = 0;
					try {
						synchronized (adTask) {
							Log.d("", "showing Ad " + currentAdIndex + " for " + waitTime + " millisecs.");
							if (waitTime > 0) {
								wait(waitTime);
							}
							// Log.d("","send empty message");
							handler.sendEmptyMessage(1); // change to next ad
						}
					} catch (InterruptedException e) {
						Log.e("ADTASK_THREAD", "exception");
					}
					// Log.d("", "-----------");
				}
				Log.d("", "End Thread");
			} catch (Exception e) {
				if (e.getMessage() != null) {
					Log.e("ADTASK_THREAD", e.getMessage());
				}
				adTask = null;
			}
		}
	}

}