package com.mobile.promkitti.impactapp.react;

import android.Manifest;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.R;

import java.util.Locale;

/**
 * Created by nananetta on 5/20/16.
 */
public class ReactTransportationActivity extends ParentReactActivity implements DefaultHardwareBackBtnHandler {

    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transportation_details);

        FrameLayout fl = (FrameLayout)findViewById(R.id.reactLayout);
        _askForOverlayPermission();
        mReactInstanceManager = getReactInstance();
        mReactRootView = new ReactRootView(this);
        Bundle initParams = new Bundle();
        initParams.putString("mainViewName", "Transportation");
        initParams.putString("lang", getLocale().getLanguage());
        mReactRootView.startReactApplication(mReactInstanceManager, "impactapp", initParams);
        fl.addView(mReactRootView);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            Constants.setLocationPermissionAllowed(true);
        }
    }

    private Locale getLocale() {
        SharedPreferences mPrefs = getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);
        if (mPrefs.contains("language")) {
            return new Locale(mPrefs.getString("language", Constants.LANGUAGE_DEFAULT));
        } else {
            return new Locale(Constants.LANGUAGE_DEFAULT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] < 0) {
            Log.i("IMPACT", "Permission DENIED");
            Constants.setLocationPermissionAllowed(false);
        } else {
            // Permission Granted
            Log.i("IMPACT", "Permission Granted");
            Constants.setLocationPermissionAllowed(true);
        }
    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU && mReactInstanceManager != null) {
            mReactInstanceManager.showDevOptionsDialog();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}