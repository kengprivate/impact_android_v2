package com.mobile.promkitti.impactapp.events.calendar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.ActivityParameters;
import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.events.detail.EventDetailsActivity;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.Utility;
import com.mobile.promkitti.impactapp.valueobjects.CalendarEvent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CalendarFragment extends ListFragment implements OnClickListener, SwipeRefreshLayout.OnRefreshListener {

	private static final String tag = "CalendarView";

	private TextView currentMonth;
	private ImageView prevMonth;
	private ImageView nextMonth;
	private GridView calendarView;
	private GridCellAdapter adapter;
	private LinearLayout overlay;
	private Calendar _calendar = Calendar.getInstance(Locale.getDefault());
	private int month, year;
	private ArrayList<CalendarEvent> events = new ArrayList<CalendarEvent>();
	private static Date selectedDate;
	private static Date todayDate;
	private CalendarListAdapter listAdapter;
	private DataController dc = DataController.getInstance();
	public static final String DATE_FORMAT = "yyyyMMdd";
	private static boolean showSelectedDate = true;
	private SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
	private Map<String, ArrayList<String>> calendarMap = new HashMap<String, ArrayList<String>>();
	private final int[] daysOfMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	private final String[][] weekdays = new String[][] { { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" },
			{ "อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส." } };
	private final String[][] months = {
			{ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
					"November", "December" },
			{ "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฏาคม", "สิงหาคม", "กันยายน",
					"ตุลาคม", "พฤศจิกายน", "ธันวาคม" } };
	// private ArrayList<CalendarEvent> items = new ArrayList<CalendarEvent>();
	private View v;
	private final int listRowHeightInDp = 50;
	private int listRowHeightInPx;
	private int listViewHeight;
	private SwipeRefreshLayout swipeLayout;
	private int numOfRow;

	/** Called when the activity is first created. */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.simple_calendar_view, container, false);

		Log.i(Constants.TAG_APP, "onCreateView");
		prevMonth = (ImageView) v.findViewById(R.id.prevMonth);
		prevMonth.setOnClickListener(this);
		currentMonth = (TextView) v.findViewById(R.id.currentMonth);
		nextMonth = (ImageView) v.findViewById(R.id.nextMonth);
		nextMonth.setOnClickListener(this);
		calendarView = (GridView) v.findViewById(R.id.calendar);
		overlay = (LinearLayout) v.findViewById(R.id.loadingOverlayLayout);

		todayDate = new Date(_calendar.get(Calendar.YEAR) - 1900, _calendar.get(Calendar.MONTH),
				_calendar.get(Calendar.DAY_OF_MONTH));
		month = todayDate.getMonth() + 1;
		year = todayDate.getYear() + 1900;

		selectedDate = todayDate;

		Locale locale = Locale.getDefault();
		String language = locale.getLanguage();
		if ("en".equalsIgnoreCase(language)) {
			currentMonth.setText(months[0][month - 1] + " " + year);
		} else {
			currentMonth.setText(months[1][month - 1] + " " + (year + 543));
		}

		listAdapter = new CalendarListAdapter(getActivity(), R.layout.calendar_list_row, events);

		LinearLayout overlay = (LinearLayout) v.findViewById(R.id.loadingOverlayLayout);
		overlay.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});

		swipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
		swipeLayout.setOnRefreshListener(this);
		swipeLayout.setColorScheme(R.color.pulldown_refresh_color_1, R.color.pulldown_refresh_color_2,
				R.color.pulldown_refresh_color_3, R.color.pulldown_refresh_color_4);

        refresh();
		return v;
	}

	@Override
	public void onRefresh() {
		Log.i(Constants.TAG_APP, "OnRefresh");
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				swipeLayout.setRefreshing(false);
				refresh();
			}
		}, 2000);
	}

	public void refresh() {
		refresh(dateFormatter.format(todayDate), false, true);
	}

	public void refresh(String dateString, boolean resetItemList, boolean showSelectedDate) {
		overlay.setVisibility(View.VISIBLE);
		String[] params = { dateString, Boolean.toString(resetItemList), Boolean.toString(showSelectedDate) };
		new LoadingTask().execute(params);
	}

	private void updateUI(Date date, boolean resetItemList, boolean showSelectedDate) {
		month = date.getMonth() + 1;
		year = date.getYear() + 1900;
		Log.d(Constants.TAG_APP, "Calendar Instance:= " + "Month: " + month + " " + "Year: " + year);

		ImageView prevMonthView = (ImageView) v.findViewById(R.id.prevMonth);
		if (todayDate.getMonth() + 1 >= month && todayDate.getYear() + 1900 >= year) {
			prevMonthView.setVisibility(View.INVISIBLE);
		} else {
			prevMonthView.setVisibility(View.VISIBLE);
		}

		TextView sunText = (TextView) v.findViewById(R.id.sunText);
		TextView monText = (TextView) v.findViewById(R.id.monText);
		TextView tueText = (TextView) v.findViewById(R.id.tueText);
		TextView wedText = (TextView) v.findViewById(R.id.wedText);
		TextView thuText = (TextView) v.findViewById(R.id.thuText);
		TextView friText = (TextView) v.findViewById(R.id.friText);
		TextView satText = (TextView) v.findViewById(R.id.satText);

		Locale locale = Locale.getDefault();
		String language = locale.getLanguage();
		if (Constants.LANGUAGE_ENGLISH.equalsIgnoreCase(language)) {
			currentMonth.setText(months[0][month - 1] + " " + year);
			sunText.setText(weekdays[0][0]);
			monText.setText(weekdays[0][1]);
			tueText.setText(weekdays[0][2]);
			wedText.setText(weekdays[0][3]);
			thuText.setText(weekdays[0][4]);
			friText.setText(weekdays[0][5]);
			satText.setText(weekdays[0][6]);
		} else {
			currentMonth.setText(months[1][month - 1] + " " + (year + 543));
			sunText.setText(weekdays[1][0]);
			monText.setText(weekdays[1][1]);
			tueText.setText(weekdays[1][2]);
			wedText.setText(weekdays[1][3]);
			thuText.setText(weekdays[1][4]);
			friText.setText(weekdays[1][5]);
			satText.setText(weekdays[1][6]);
		}
		CalendarFragment.showSelectedDate = showSelectedDate;

		// Initialised
		adapter = new GridCellAdapter(getActivity().getApplicationContext(), R.id.calendar_day_gridcell, month, year);
		adapter.notifyDataSetChanged();
		calendarView.setAdapter(adapter);
		int cellHeight = ContextCompat.getDrawable(getContext(), R.drawable.calendar_day_n).getIntrinsicHeight() + Utility.convertDpToPx(getActivity(), 1);
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, numOfRow * cellHeight);
		swipeLayout.setLayoutParams(params);

		if (resetItemList) {
			events.clear();
		}
		listAdapter.notifyDataSetChanged();
		setListAdapter(listAdapter);

		LinearLayout calendarContainer = (LinearLayout) v.findViewById(R.id.calendarListContainer);
		listViewHeight = calendarContainer.getMeasuredHeight();
		// Log.d(tag, "measuredHeight: "+ listViewHeight);
		listRowHeightInPx = Utility.convertDpToPx(getActivity(), listRowHeightInDp);
	}

	private void listCalendarEventsByDate(Date yearMonthDay, boolean resetItemList) {
		Log.d(Constants.TAG_APP, "list event Date: " + yearMonthDay);
		// Log.d(tag, "reset ItemList : " + resetItemList);

		selectedDate = yearMonthDay;

		events.clear();
		if (!resetItemList) {
			ArrayList<String> eventList = calendarMap.get(dateFormatter.format(selectedDate));
			if (eventList != null) {
				for (Iterator<String> i = eventList.iterator(); i.hasNext();) {
					String id = i.next();
					events.add((CalendarEvent) dc.getCalendarEventBrief(id));
				}
			} else {
				events.clear();
			}
		}

		// Log.d(tag, "listCalendarEventsByDate events.size() : " +
		// events.size());

		int numOfRow = listViewHeight / listRowHeightInPx;
		while (events.size() < numOfRow) {
			events.add(new CalendarEvent());
		}
		listAdapter.notifyDataSetChanged();
		setListAdapter(listAdapter);
	}

	public void onListItemClick(ListView parent, View v, int position, long id) {
		if (Utility.isBlank(events.get(position).getId())) {
			return;
		}
		Intent intent = new Intent(getActivity().getApplicationContext(), EventDetailsActivity.class);
		intent.putExtra(ActivityParameters.INTERNAL_ID, ((CalendarEvent) events.get(position)).getInternalId());
		intent.putExtra(ActivityParameters.ID, ((CalendarEvent) events.get(position)).getId());
		getActivity().startActivity(intent);
		FlurryUtil.logEvent(FlurryConstants.OPEN_EVENT_FROM_CALENDAR);
	}

	@Override
	public void onClick(View v) {
		if (v == prevMonth) {
			goPreviousMonth(null, true, false);
		}
		if (v == nextMonth) {
			goNextMonth(null, true, false);
		}
	}

	private void goPreviousMonth(Date specificDate, boolean resetItemList, boolean showSelectedDate) {
		if (month <= 1) {
			month = 12;
			year--;
		} else {
			month--;
		}
		int day = 0;
		if (specificDate != null) {
			day = specificDate.getDate();
		} else {
			_calendar.set(year, month - 1, 1);
			day = _calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		Log.d(tag, "Setting Prev Month in GridCellAdapter: " + "Day: " + day + " Month: " + month + " Year: " + year);
		String dateString = dateFormatter.format(new Date(year - 1900, month - 1, day));
		refresh(dateString, resetItemList, showSelectedDate);
	}

	private void goNextMonth(Date specificDate, boolean resetItemList, boolean showSelectedDate) {
		if (month > 11) {
			month = 1;
			year++;
		} else {
			month++;
		}
		int day = 0;
		if (specificDate != null) {
			day = specificDate.getDate();
		} else {
			_calendar.set(year, month - 1, 1);
			day = _calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		Log.d(tag, "Setting Next Month in GridCellAdapter: " + "Day: " + day + " Month: " + month + " Year: " + year);
		String dateString = dateFormatter.format(new Date(year - 1900, month - 1, day));
		refresh(dateString, resetItemList, showSelectedDate);
	}

	@Override
	public void onDestroy() {
		Log.i(Constants.TAG_APP, "Destroying View ...");
		super.onDestroy();
	}

	private class LoadingTask extends AsyncTask<String, Void, String[]> {
		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 */

		protected String[] doInBackground(String... val) {
			String dateString = val[0];
			try {
				Log.i(Constants.TAG_APP, "Loading from DataController: " + dateString);
				calendarMap = dc.getCalendarEvents(dateString);
			} catch (Exception e) {
				Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
				cancel(false);
			}
			return new String[] { dateString, val[1], val[2] };
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		protected void onPostExecute(String[] params) {
			if (getActivity() != null) {
				try {
					Date date = dateFormatter.parse(params[0]);
					updateUI(date, Boolean.parseBoolean(params[1]), Boolean.parseBoolean(params[2]));
					listCalendarEventsByDate(date, Boolean.parseBoolean(params[1]));
					LinearLayout overlay = (LinearLayout) v.findViewById(R.id.loadingOverlayLayout);
					overlay.setVisibility(View.GONE);
				} catch (Exception e) {
					Log.e(Constants.TAG_APP, e.getMessage());
				}

			}
		}
	}

	// ///////////////////////////////////////////////////////////////////////////////////////
	// Inner Class
	public class GridCellAdapter extends BaseAdapter implements OnClickListener {
		private static final String tag = "GridCellAdapter";
		private final Context _context;

		private final List<String> list;
		private static final int DAY_OFFSET = 1;
		private int daysInMonth;
		private int currentDayOfMonth;
		private int currentWeekDay;
		private Button gridcell;
		private ImageView calendar_marker;
		private Button selectedButton;
		private Button previousSelection;

		// Days in Current Month
		public GridCellAdapter(Context context, int textViewResourceId, int month, int year) {
			super();
			this._context = context;
			this.list = new ArrayList<String>();
			previousSelection = null;

			Calendar calendar = Calendar.getInstance();
			setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
			setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));

			// Print Month
			printMonth(month, year);
		}

		private String getMonthAsString(int i) {
			int month = i == 0 ? 11 : i - 1;
			return months[0][month];
		}

		private String getWeekDayAsString(int i) {
			int day = i == 0 ? 6 : i - 1;
			return weekdays[0][day];
		}

		private int getNumberOfDaysOfMonth(int i) {
			int month = i == 0 ? 11 : i - 1;
			return daysOfMonth[month];
		}

		public String getItem(int position) {
			return list.get(position);
		}

		@Override
		public int getCount() {
			return list.size();
		}

		/**
		 * Prints Month
		 * 
		 * @param mm
		 * @param yy
		 */
		private void printMonth(int mm, int yy) {
			// Log.d(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);
			// The number of days to leave blank at
			// the start of this month.
			int trailingSpaces = 0;
			int daysInPrevMonth = 0;
			int prevMonth = 0;
			int prevYear = 0;
			int nextMonth = 0;
			int nextYear = 0;

			int currentMonth = mm;
			daysInMonth = getNumberOfDaysOfMonth(currentMonth);

			// Log.d(tag, "Current Month: " + " " + currentMonthName +
			// " having " + daysInMonth + " days.");

			// Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
			GregorianCalendar cal = new GregorianCalendar(yy, currentMonth - 1, 1);
			// Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

			if (currentMonth == 11) {
				prevMonth = currentMonth - 1;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
				nextMonth = 0;
				prevYear = yy;
				nextYear = yy + 1;
				// Log.d(tag, "*->PrevYear: " + prevYear + " PrevMonth:" +
				// prevMonth + " NextMonth: " + nextMonth + " NextYear: " +
				// nextYear);
			} else if (currentMonth == 0) {
				prevMonth = 11;
				prevYear = yy - 1;
				nextYear = yy;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
				nextMonth = 1;
				// Log.d(tag, "**--> PrevYear: " + prevYear + " PrevMonth:" +
				// prevMonth + " NextMonth: " + nextMonth + " NextYear: " +
				// nextYear);
			} else {
				prevMonth = currentMonth - 1;
				nextMonth = currentMonth + 1;
				nextYear = yy;
				prevYear = yy;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
				// Log.d(tag, "***---> PrevYear: " + prevYear + " PrevMonth:" +
				// prevMonth + " NextMonth: " + nextMonth + " NextYear: " +
				// nextYear);
			}

			// Compute how much to leave before before the first day of the
			// month.
			// getDay() returns 0 for Sunday.
			int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
			trailingSpaces = currentWeekDay;

			// Log.d(tag, "Week Day:" + currentWeekDay + " is " +
			// getWeekDayAsString(currentWeekDay));
			// Log.d(tag, "No. Trailing space to Add: " + trailingSpaces);
			// Log.d(tag, "No. of Days in Previous Month: " + daysInPrevMonth);

			if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1) {
				++daysInMonth;
			}

			// Trailing Month days
			for (int i = 0; i < trailingSpaces; i++) {
				// Log.d(tag,"PREV MONTH:= "+ prevMonth+ " => "+
				// getMonthAsString(prevMonth)+ " "+
				// String.valueOf((daysInPrevMonth- trailingSpaces +
				// DAY_OFFSET)+ i));
				list.add(String.format("%02d", (daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-GREY" + "-"
						+ String.format("%02d", prevMonth) + "-" + prevYear);
			}

			// Log.e("",""+currentMonth
			// +" cal: "+(Calendar.getInstance().get(Calendar.MONTH)+1));
			// Current Month Days
			for (int i = 1; i <= daysInMonth; i++) {
				// Log.d(currentMonthName, String.format("%02d",i) + " " +
				// String.format("%02d",currentMonth) + " " + yy);
				if (i == getCurrentDayOfMonth() && currentMonth == Calendar.getInstance().get(Calendar.MONTH) + 1) {
					list.add(String.format("%02d", i) + "-BLUE" + "-" + String.format("%02d", currentMonth) + "-" + yy);
				} else {
					list.add(String.format("%02d", i) + "-WHITE" + "-" + String.format("%02d", currentMonth) + "-" + yy);
				}
			}

			// Leading Month days
			for (int i = 0; i < list.size() % 7; i++) {
				// Log.d(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));
				list.add(String.format("%02d", i + 1) + "-GREY" + "-" + String.format("%02d", nextMonth) + "-"
						+ nextYear);
			}
			
			numOfRow = list.size() / 7;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private boolean isSelectedDateInTheMonth(Date selectDate, String themonth, String theyear) {
			int month = Integer.parseInt(themonth);
			int year = Integer.parseInt(theyear);
			if ((selectDate.getMonth() + 1 == month) && (selectDate.getYear() + 1900 == year)) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			if (row == null) {
				LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(R.layout.calendar_day_gridcell, parent, false);
			}

			// Get a reference to the Day gridcell
			gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
			gridcell.setOnClickListener(this);

			String[] day_color = list.get(position).split("-");
			String theday = day_color[0];
			String themonth = day_color[2];
			String theyear = day_color[3];

			// Set the Day GridCell
			gridcell.setText(String.valueOf(Integer.parseInt(theday)));
			gridcell.setTag(theyear + themonth + theday);

			if (day_color[1].equals("GREY")) {
				gridcell.setTextColor(Color.LTGRAY);
			}
			if (day_color[1].equals("WHITE")) {
				gridcell.setTextColor(getResources().getColor(R.color.content_color));
			}
			if (day_color[1].equals("BLUE")) {
				gridcell.setTextColor(Color.WHITE);
				gridcell.setBackgroundResource(R.drawable.calendar_today_cell);
			}

			Date calDate = new Date(System.currentTimeMillis());
			try {
				calDate = dateFormatter.parse(theyear + themonth + theday);
			} catch (Exception e) {
				Log.e("", e.getMessage());
			}

			ArrayList<String> eventList = calendarMap.get(theyear + themonth + theday);
			if (eventList == null) {
				gridcell.setBackgroundResource(R.drawable.calendar_button_selector);
			} else {
				if(!calDate.before(todayDate)) {
					gridcell.setBackgroundResource(R.drawable.calendar_mark_n);
				}
			}
			
			if (showSelectedDate) {
				if (selectedButton == null && gridcell.getTag().equals(dateFormatter.format(todayDate))
						&& isSelectedDateInTheMonth(selectedDate, themonth, theyear)
						&& (todayDate.compareTo(selectedDate) == 0)) {
					selectedButton = gridcell;
					gridcell.setTextColor(Color.WHITE);
					gridcell.setBackgroundResource(R.drawable.calendar_today_h);
				}
				// update selected date color
				else if (gridcell.getTag().equals(dateFormatter.format(selectedDate))) {
					selectedButton = gridcell;
					gridcell.setTextColor(Color.WHITE);
					gridcell.setBackgroundResource(R.drawable.calendar_day_h);
				}
			} 
			if (gridcell.getTag().equals(dateFormatter.format(todayDate))) {
				if(gridcell.getTag().equals(dateFormatter.format(selectedDate))) {
					gridcell.setBackgroundResource(R.drawable.calendar_today_h);
				} else {
					gridcell.setBackgroundResource(R.drawable.calendar_mark_h);
				}
			}

			return row;
		}

		@Override
		public void onClick(View view) {
			Date currSelectedDate = new Date(System.currentTimeMillis());
			try {
				currSelectedDate = dateFormatter.parse((String) view.getTag());
				Log.d(tag, "Curr Selected Date: " + currSelectedDate);

				int currSelectedMonthInt = currSelectedDate.getYear() * 100 + currSelectedDate.getMonth();
				int selectedMonthInt = selectedDate.getYear() * 100 + selectedDate.getMonth();

				selectedDate = dateFormatter.parse((String) view.getTag());

				if (currSelectedMonthInt < selectedMonthInt) {
					goPreviousMonth(selectedDate, false, true);
				} else if (currSelectedMonthInt > selectedMonthInt) {
					goNextMonth(selectedDate, false, true);
				}

			} catch (ParseException e) {
				Log.e("", e.getMessage());
			}

			String todayDateString = dateFormatter.format(todayDate);
			String selectedDateString = (String) view.getTag();
			if (selectedButton != null) {
				if (selectedButton.getTag().equals(todayDateString)) {
					selectedButton.setTextColor(Color.WHITE);
					selectedButton.setBackgroundResource(R.drawable.calendar_mark_h);
				} else {
					ArrayList<String> eventList = calendarMap.get(dateFormatter.format(currSelectedDate));
					if (eventList == null) {
						selectedButton.setTextColor(getResources().getColor(R.color.content_color));
						selectedButton.setBackgroundResource(R.drawable.calendar_day_n);
					} else {
						selectedButton.setTextColor(getResources().getColor(R.color.content_color));
						selectedButton.setBackgroundResource(R.drawable.calendar_mark_n);
					}
				}
				
				if(previousSelection != null) {
					ArrayList<String> previousEventList = calendarMap.get(previousSelection.getTag());
					if (previousSelection.getTag().equals(todayDateString)) {
						previousSelection.setTextColor(Color.WHITE);
						previousSelection.setBackgroundResource(R.drawable.calendar_mark_h);
					} else {
						Date previousSelectionDate = null;
						try {
							previousSelectionDate = dateFormatter.parse(previousSelection.getTag().toString());
						} catch (ParseException e) {
							e.printStackTrace();
						}
						if (previousEventList == null || !previousSelectionDate.after(todayDate)) {
							previousSelection.setTextColor(getResources().getColor(R.color.content_color));
							previousSelection.setBackgroundResource(R.drawable.calendar_day_n);
						} else {
							previousSelection.setTextColor(getResources().getColor(R.color.content_color));
							previousSelection.setBackgroundResource(R.drawable.calendar_mark_n);
						}
					}
				}
			}
			selectedButton = (Button) view;
			if (selectedButton.getTag().equals(todayDateString)) {
				selectedButton.setTextColor(Color.WHITE);
				selectedButton.setBackgroundResource(R.drawable.calendar_today_h);
			} else if (selectedButton.getTag().equals(selectedDateString)) {
				selectedButton.setTextColor(Color.WHITE);
				selectedButton.setBackgroundResource(R.drawable.calendar_day_h);
			}
			selectedButton.invalidate();
			previousSelection = selectedButton;
			
			if (todayDate.after(currSelectedDate)) {
				events.clear();

				Log.d(tag, "listCalendarEventsByDate events.size() : " + events.size());
				// insert empty rows
				int numOfRow = listViewHeight / listRowHeightInPx;
				while (events.size() < numOfRow) {
					events.add(new CalendarEvent());
				}
				Log.d(tag, "listCalendarEventsByDate events.size() : " + events.size());

				listAdapter.notifyDataSetChanged();
				setListAdapter(listAdapter);
				return;
			}
			listCalendarEventsByDate(selectedDate, false);
		}

		public int getCurrentDayOfMonth() {
			return currentDayOfMonth;
		}

		private void setCurrentDayOfMonth(int currentDayOfMonth) {
			this.currentDayOfMonth = currentDayOfMonth;
		}

		public void setCurrentWeekDay(int currentWeekDay) {
			this.currentWeekDay = currentWeekDay;
		}

		public int getCurrentWeekDay() {
			return currentWeekDay;
		}
	}
}
