package com.mobile.promkitti.impactapp.common;

public interface NotificationHandler {

	void process();
	
}
