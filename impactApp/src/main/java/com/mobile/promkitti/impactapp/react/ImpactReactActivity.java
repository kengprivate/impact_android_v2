package com.mobile.promkitti.impactapp.react;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.mobile.promkitti.impactapp.Constants;

import java.util.Locale;

/**
 * Created by nananetta on 5/20/16.
 */
public class ImpactReactActivity extends ParentReactActivity implements DefaultHardwareBackBtnHandler {
    private ReactRootView mReactRootView;

    private ReactInstanceManager mReactInstanceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _askForOverlayPermission();
        mReactInstanceManager = getReactInstance();
        mReactRootView = new ReactRootView(this);
        Bundle initParams = new Bundle();
        initParams.putString("mainViewName", "Traffic");
        initParams.putString("lang", getLocale().getLanguage());
        mReactRootView.startReactApplication(mReactInstanceManager, "impactapp", initParams);
        setContentView(mReactRootView);
}

    private Locale getLocale() {
        SharedPreferences mPrefs = getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);
        if (mPrefs.contains("language")) {
            return new Locale(mPrefs.getString("language", Constants.LANGUAGE_DEFAULT));
        } else {
            return new Locale(Constants.LANGUAGE_DEFAULT);
        }
    }


    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU && mReactInstanceManager != null) {
            mReactInstanceManager.showDevOptionsDialog();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}