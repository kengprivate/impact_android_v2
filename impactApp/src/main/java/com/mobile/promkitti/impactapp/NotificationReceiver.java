package com.mobile.promkitti.impactapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.mobile.promkitti.impactapp.common.NotificationActivity;
import com.mobile.promkitti.impactapp.common.NotificationList;
import com.mobile.promkitti.impactapp.events.ad.AdDetailActivity;
import com.mobile.promkitti.impactapp.events.detail.EventDetailsActivity;
import com.mobile.promkitti.impactapp.promotion.PromotionDetailActivity;
import com.mobile.promkitti.impactapp.util.NotificationUtil;
import com.mobile.promkitti.impactapp.util.Utility;
import com.mobile.promkitti.impactapp.valueobjects.PushMessage;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OSNotificationPayload;
import com.onesignal.OSNotificationReceivedResult;
import com.onesignal.OneSignal;
import com.onesignal.shortcutbadger.ShortcutBadger;

import org.json.JSONObject;

public class NotificationReceiver extends NotificationExtenderService implements OneSignal.NotificationReceivedHandler, OneSignal.NotificationOpenedHandler {

	private static final String logTag = "Notification Receiver";
	private PushMessage receivedData;
	private String promotionId;
	private String eventId;
	private String adId;
	private Context context;

	public NotificationReceiver() {
	}

	public NotificationReceiver(Context context) {
		this.context = context;
	}

	@Override
	public void notificationReceived(OSNotification notification) {
		if(context == null) {
			context = getApplicationContext();
		}
		Log.d(logTag, "RECEIVED onReceived - isActive: " + Constants.isActive());
		ShortcutBadger.removeCount(context);
		removeNotification(notification.androidNotificationId);
	}

//	@Override
//	public void notificationReceived(OSNotification notification) {
//
//		if(context == null) {
//			context = getApplicationContext();
//		}
//
//		createMessageObject(notification.payload);
//		Log.d(logTag, "RECEIVED onReceived - isActive: " + Constants.isActive());
//
//		ShortcutBadger.removeCount(context);
//
//		if (!Utility.isBlank(promotionId)) {
//			NotificationUtil.getPromotionNotificationList().addNotification(promotionId);
//		} else if (!Utility.isBlank(eventId)) {
//			NotificationUtil.getEventNotificationList().addNotification(eventId);
//		} else if (!Utility.isBlank(adId)) {
//			NotificationUtil.getAdNotificationList().addNotification(adId);
//		}
//		saveNotificationValues();
//		removeNotification(notification.androidNotificationId);
//
//		// If app is running
//		if (Constants.isActive()) {
//			Intent popupIntent = createPopupIntent();
//			prepareIntentParameters(popupIntent);
//			context.startActivity(popupIntent);
//		} else {
//			showPushNotification();
//		}
//
//	}

	@Override
	public void notificationOpened(OSNotificationOpenResult result) {

		if(context == null) {
			context = getApplicationContext();
		}

		createMessageObject(result.notification.payload);
		removeNotification(result.notification.androidNotificationId);
		Log.d(logTag, "CLICKED onOpened - isActive: " + Constants.isActive());
		ShortcutBadger.removeCount(context);

		// If app is running
		if (Constants.isActive()) {
			Log.d(logTag, "User clicked while App is ACTIVE");

			Intent popupIntent = createPopupIntent();
			prepareIntentParameters(popupIntent);
			context.startActivity(popupIntent);
		}
		// if app is not opened or in background
		else {
			Log.d(logTag, "User clicked while App is NON-ACTIVE");

			Intent popupIntent = createPopupIntent();
			prepareIntentParameters(popupIntent);
			context.startActivity(popupIntent);
		}
	}

	@Override
	protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {

		if(context == null) {
			context = getApplicationContext();
		}

		createMessageObject(receivedResult.payload);
		Log.d(logTag, "RECEIVED onProcessing - isActive: " + Constants.isActive());
		ShortcutBadger.removeCount(context);

		if (!Utility.isBlank(promotionId)) {
			NotificationUtil.getPromotionNotificationList().addNotification(promotionId);
		} else if (!Utility.isBlank(eventId)) {
			NotificationUtil.getEventNotificationList().addNotification(eventId);
		} else if (!Utility.isBlank(adId)) {
			NotificationUtil.getAdNotificationList().addNotification(adId);
		}
		saveNotificationValues();
//		removeAllNotifications();

		// If app is running
		if (Constants.isActive()) {
			Intent popupIntent = createPopupIntent();
			prepareIntentParameters(popupIntent);
			context.startActivity(popupIntent);
		} else {
			showPushNotification();
		}

		return false;
	}

	private void saveNotificationValues() {
		saveSpecifiedNotificationValues("promotionIdList", NotificationUtil.getPromotionNotificationList());
		saveSpecifiedNotificationValues("adIdList", NotificationUtil.getAdNotificationList());
		saveSpecifiedNotificationValues("eventIdList", NotificationUtil.getEventNotificationList());
	}

	private void saveSpecifiedNotificationValues(String name, NotificationList list) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
			if (i < list.size() - 1) {
				sb.append(",");
			}
		}
		SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, sb.toString());

		// Commit the edits!
		editor.commit();
	}

	private void removeNotification(int notificationId) {
		Log.i(logTag, "Removing Notification ID: " + notificationId);
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(notificationId);
	}

	private void removeAllNotifications() {
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancelAll();
	}

	private void prepareIntentParameters(Intent popupIntent) {
		popupIntent.putExtra("MSG", receivedData.getMessage());
		if (!Utility.isBlank(promotionId)) {
			popupIntent.putExtra("PROMOTION_ID", promotionId);
		} else if (!Utility.isBlank(eventId)) {
			popupIntent.putExtra("EVENT_ID", eventId);
		} else if (!Utility.isBlank(adId)) {
			popupIntent.putExtra("AD_ID", adId);
		}
	}

	private Intent createPopupIntent() {
		Intent popupIntent = new Intent(Intent.ACTION_MAIN);
		popupIntent.setClass(context, NotificationActivity.class);
		popupIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return popupIntent;
	}

	private void createMessageObject(OSNotificationPayload payload) {
		JSONObject additionalData = payload.additionalData;
		Log.i(logTag, "Received additional data: " + additionalData);
//		Log.i(logTag, "Received notificationID: " + payload.notificationId);
		Log.i(logTag, "Received raw data: " + payload.body);
		receivedData = new PushMessage(payload);
		promotionId = receivedData.getPromotionId();
		eventId = receivedData.getEventId();
		adId = receivedData.getAdId();
}

	private void showPushNotification() {
		Log.d(logTag, "Notify IMPACT");

		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if (!Utility.isBlank(promotionId)) {
			intent.putExtra("ID", promotionId);
			intent.putExtra("DIRECT", true);
			intent.setClass(context, PromotionDetailActivity.class);

		} else if (!Utility.isBlank(eventId)) {
			intent.putExtra("ID", eventId);
			intent.putExtra("DIRECT", true);
			intent.setClass(context, EventDetailsActivity.class);

		} else if (!Utility.isBlank(adId)) {
			intent.putExtra("ID", adId);
			intent.putExtra("DIRECT", true);
			intent.setClass(context, AdDetailActivity.class);

		} else {
			intent.putExtra("DIRECT", true);
			intent.setClass(context, StartActivity.class);
		}

		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.launcher_icon).setContentTitle("IMPACT").setContentText(receivedData.getMessage())
				.setDefaults(Notification.DEFAULT_ALL).setAutoCancel(true).setContentIntent(pendingIntent); // Required on Gingerbread and below
		notificationManager.notify(getUniqueNumber(), nBuilder.build());
	}

	private int getUniqueNumber() {
		int r = (int) (Math.random() * 10000);
		return r;
	}

}