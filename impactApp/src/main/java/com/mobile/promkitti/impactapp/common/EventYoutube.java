package com.mobile.promkitti.impactapp.common;

import android.os.Parcel;
import android.os.Parcelable;

public class EventYoutube extends EventMedia {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public EventMedia createFromParcel(Parcel in) {
            return new EventYoutube();
        }

        public EventMedia[] newArray(int size) {
            return new EventYoutube[size];
        }
    };
}
