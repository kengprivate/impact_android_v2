package com.mobile.promkitti.impactapp.react;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.R;

import java.util.Locale;

/**
 * Created by nananetta on 5/20/16.
 */
public class ReactTrafficActivity extends ParentReactActivity implements DefaultHardwareBackBtnHandler {
    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager;
    private Activity thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.traffic_details);
        thisActivity = this;

        // check valid google play services version must > 8.9
        checkIfValidGooglePlayServicesVersion();

        FrameLayout fl = (FrameLayout)findViewById(R.id.reactLayout);
        _askForOverlayPermission();
        mReactInstanceManager = getReactInstance();
        mReactRootView = new ReactRootView(this);
        Bundle initParams = new Bundle();
        initParams.putString("mainViewName", "Traffic");
        initParams.putString("lang", getLocale().getLanguage());
        mReactRootView.startReactApplication(mReactInstanceManager, "impactapp", initParams);
        fl.addView(mReactRootView);
    }

    private void checkIfValidGooglePlayServicesVersion() {
        try {
            int v = getPackageManager().getPackageInfo(GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE, 0).versionCode;
//			Log.d("PLAYSVC", "version code: "+v);
            // if Google Play Services is lower than 9.0
            if(v < 9000000) {
                AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity).setTitle("IMPACT")
                        .setMessage("This feature does not support Google Play Services version lower than 9.0. Please upgrade your Google Play Services.")
                        .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int actionId) {
                                thisActivity.finish();
                            }
                        });
                builder.create().show();
            }
        }catch (PackageManager.NameNotFoundException e) {
            Log.e(this.getClass().getName(), e.getLocalizedMessage());
        }
    }

    private Locale getLocale() {
        SharedPreferences mPrefs = getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);
        if (mPrefs.contains("language")) {
            return new Locale(mPrefs.getString("language", Constants.LANGUAGE_DEFAULT));
        } else {
            return new Locale(Constants.LANGUAGE_DEFAULT);
        }
    }


    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU && mReactInstanceManager != null) {
            mReactInstanceManager.showDevOptionsDialog();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}