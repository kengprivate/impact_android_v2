package com.mobile.promkitti.impactapp.events.calendar;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.util.Utility;
import com.mobile.promkitti.impactapp.valueobjects.CalendarEvent;

public class CalendarListAdapter extends ArrayAdapter<CalendarEvent> {

    private ArrayList<CalendarEvent> items;

    public CalendarListAdapter(Context context, int textViewResourceId, ArrayList<CalendarEvent> items) {
            super(context, textViewResourceId, items);
            this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	View v = convertView;
    	if (v == null) {
    		LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		v = vi.inflate(R.layout.calendar_list_row, parent, false);
    	}	
		CalendarEvent o = (CalendarEvent) items.get(position);
		
		if (o != null) {
			TextView pt = (TextView) v.findViewById(R.id.eventText);
			if (pt != null) {
				pt.setText( o.getTitle() );
			}
		}
		
    	return v;
    }
    
    @Override
    public boolean isEnabled(int position) {
    	CalendarEvent o = (CalendarEvent) items.get(position);
		
		if (o != null) {
			if(Utility.isBlank(o.getId())) {
				return false;
			}
			else {
				return true;
			}
		}
		return false;
    }
    
    public void setCalendarEventList(ArrayList<CalendarEvent> items) {
    	this.items = items;
    	notifyDataSetChanged();
    }
    
}