package com.mobile.promkitti.impactapp.valueobjects;

import android.graphics.drawable.Drawable;

import com.mobile.promkitti.impactapp.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CalendarEvent extends Event {

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

	private String dateLocation;
	private String url;
	private String locations;
	private String date;
	private String time;
	private String ticketPrice;
	private String organizerInfo;
	private String ticketTel;
	private String ticketUrl;
	private String fbUrl;
	private String fbShareUrl;
	private Date startDateTime;
	private Date endDateTime;
	private long timestamp;
	private String lat;
	private String lon;

	private boolean isDetailsLoaded = false;

	private Drawable listImage;
	private Map<String, Drawable> detailImageMap = new HashMap<String, Drawable>();
	
	public CalendarEvent() {
	}

	public CalendarEvent(String id, String title, String description, String dateLoation, String startDate,
			String endDate) {
		setId(id);
		setTitle(title);
		setStartDate(startDate);
		setEndDate(endDate);
		this.dateLocation = dateLoation;
		detailImageMap = new HashMap<String, Drawable>();
	}

	public String getPrefix() {
		return Constants.PREFIX_CALENDAR_EVENT;
	}

	public String getDateLocation() {
		return dateLocation;
	}

	public void setDateLocation(String dateLocation) {
		this.dateLocation = dateLocation;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(String ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public String getOrganizerInfo() {
		return organizerInfo;
	}

	public void setOrganizerInfo(String organizerInfo) {
		this.organizerInfo = organizerInfo;
	}

	public Drawable getListImage() {
		return listImage;
	}

	public void setListImage(Drawable listImage) {
		this.listImage = listImage;
	}

	public Drawable getDetailImage(String key) {
		return detailImageMap.get(key);
	}

	public void setDetailImage(String key, Drawable detailImage) {
		detailImageMap.put(key, detailImage);
	}

	public String getTicketTel() {
		return ticketTel;
	}

	public void setTicketTel(String ticketTel) {
		this.ticketTel = ticketTel;
	}

	public boolean isDetailsLoaded() {
		return isDetailsLoaded;
	}

	public void setDetailsLoaded(boolean isDetailsLoaded) {
		this.isDetailsLoaded = isDetailsLoaded;
	}

	public String getTicketUrl() {
		return ticketUrl;
	}

	public void setTicketUrl(String ticketUrl) {
		this.ticketUrl = ticketUrl;
	}

	public String getFbShareUrl() {
		return fbShareUrl;
	}

	public void setFbShareUrl(String fbShareUrl) {
		this.fbShareUrl = fbShareUrl;
	}

	public String getFbUrl() {
		return fbUrl;
	}

	public void setFbUrl(String fbUrl) {
		this.fbUrl = fbUrl;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) throws ParseException {
		this.timestamp = dateFormat.parse(timestamp).getTime();
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}


	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}
}
