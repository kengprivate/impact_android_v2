package com.mobile.promkitti.impactapp.fb;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Created by nananetta on 3/9/16.
 */
public class FacebookSharingActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        {
            super.onCreate(savedInstanceState);

            // Init Facebook APIs
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager = CallbackManager.Factory.create();
            shareDialog = new ShareDialog(this);

            // Get intent, action and MIME type
            Intent intent = getIntent();
            String action = intent.getAction();
            String type = intent.getType();

            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if ("text/plain".equals(type)) {
                    shareLink(intent);
                }
            } else {
                // Handle other intents, such as being started from the home screen
            }
            finish();
        }
    }

    private void shareLink(Intent intent) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            Bundle bb = intent.getBundleExtra("FB_BUNDLE");
            String contentUrl = bb.getString("CONTENT_URL");
            String imageUrl = bb.getString("IMAGE_URL");
            Uri contentUri = null;
            if (contentUrl != null) {
                contentUri = Uri.parse(contentUrl);
            }
            String contentTitle = intent.getStringExtra(Intent.EXTRA_SUBJECT);
            Uri imageUri = null;
            if (imageUrl != null) {
                imageUri = Uri.parse(imageUrl);
            }
            String contentDesc = intent.getStringExtra(Intent.EXTRA_TEXT);
            Log.e("FB", "contentURL:" + contentUrl);
            Log.e("FB", "contentTitle:" + contentTitle);
            Log.e("FB", "imageUrl:" + imageUrl);
            Log.e("FB", "contentDesc:" + contentDesc);
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(contentUri)
                    .setContentTitle(contentTitle)
                    .setImageUrl(imageUri)
                    .setContentDescription(contentDesc)
                    .build();
            shareDialog.show(content);
        }

    }

//    private void handleSendText(Intent intent) {
//        FacebookSdk.sdkInitialize(getApplicationContext());
//
//        String subject = intent.getStringExtra(Intent.EXTRA_SUBJECT);
//        String shareContent = intent.getStringExtra(Intent.EXTRA_TEXT);
//        sharePhotoToFacebook(shareContent);
//        Log.e("FB Sharing", "sharing facebook success");
//    }
//
//    private void sharePhotoToFacebook(String shareContent){
//        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.ad_detail_placeholder);
//        SharePhoto photo = new SharePhoto.Builder()
//                .setBitmap(image)
//                .setCaption(shareContent)
//                .build();
//
//        SharePhotoContent content = new SharePhotoContent.Builder()
//                .addPhoto(photo)
//                .build();
//
//        ShareApi.share(content, null);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int responseCode, Intent data)
//    {
//        super.onActivityResult(requestCode, responseCode, data);
//        callbackManager.onActivityResult(requestCode, responseCode, data);
//    }

}
