package com.mobile.promkitti.impactapp.valueobjects;

import android.util.Log;

import com.onesignal.OSNotificationPayload;

import org.json.JSONException;
import org.json.JSONObject;

public class PushMessage {

	private String promotionId;
	private String adId;
	private String eventId;
	private String action;
	private String message;

	public PushMessage(OSNotificationPayload payload) {
		try {
			JSONObject json = payload.additionalData;
			if(json!=null) {
				promotionId = json.isNull("p") ? null : json.getString("p");
				adId = json.isNull("a") ? null : json.getString("a");
				eventId = json.isNull("e") ? null : json.getString("e");
			}
			message = payload.body;
		} catch (JSONException e) {
			Log.e("PushMessage", e.getMessage());
		}
	}

	public String getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}

	public String getAdId() {
		return adId;
	}

	public void setAdId(String adId) {
		this.adId = adId;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
