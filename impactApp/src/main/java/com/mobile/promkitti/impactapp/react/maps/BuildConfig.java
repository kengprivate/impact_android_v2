package com.mobile.promkitti.impactapp.react.maps;

/**
 * Created by nananetta on 8/20/16.
 */
public final class BuildConfig {
    public static final boolean DEBUG = true;
    public static final String APPLICATION_ID = "com.mobile.promkitti.impactapp.react.maps";
    public static final String BUILD_TYPE = "release";
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = -1;
    public static final String VERSION_NAME = "";

    public BuildConfig() {
    }
}
