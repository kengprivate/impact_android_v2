package com.mobile.promkitti.impactapp;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryAgentListener;
import com.mobile.promkitti.impactapp.common.DataController;
import com.onesignal.OneSignal;

import static com.mobile.promkitti.impactapp.Constants.FLURRY_API_KEY;

public class ImpactApp extends Application {

	@Override
	public void onCreate() {

		// The following line enables the push notification
		if (Constants.PUSH_NOTIFICATION_ENABLED) {
			OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.NONE);
			NotificationReceiver receiverInstance = new NotificationReceiver(getApplicationContext());
			NotificationReceiver openerInstance = new NotificationReceiver(getApplicationContext());
			OneSignal.startInit(this).setNotificationOpenedHandler(openerInstance).setNotificationReceivedHandler(receiverInstance).inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None).init();
			OneSignal.clearOneSignalNotifications();
			if (!Constants.PUSH_IN_PRODUCTION) {
				OneSignal.sendTag("osdev", "true");
			}
		}

		if(Constants.FLURRY_ENABLED) {
			new FlurryAgent.Builder()
					.withLogEnabled(true)
					.withCaptureUncaughtExceptions(true)
					.withContinueSessionMillis(10)
					.withLogEnabled(true)
					.withLogLevel(Log.VERBOSE)
					.withListener(new FlurryAgentListener() {
						@Override
						public void onSessionStarted() {

						}
					})
					.build(this, FLURRY_API_KEY);
		}

		Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.list_placeholder);
		DataController.getInstance().setDummyListImage(drawable);
		super.onCreate();
	}
}
