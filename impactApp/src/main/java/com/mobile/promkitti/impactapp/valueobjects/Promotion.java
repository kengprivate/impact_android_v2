package com.mobile.promkitti.impactapp.valueobjects;

import java.util.HashMap;
import java.util.Map;

import android.graphics.drawable.Drawable;

import com.mobile.promkitti.impactapp.Constants;

public class Promotion extends Event {

	private Drawable listImage;
	private Map<String, Drawable> detailImageMap = new HashMap<String, Drawable>();
	private String dateLocation;

	private boolean isDetailsLoaded = false;

	public Promotion() {
		super();
		detailImageMap = new HashMap<String, Drawable>();
	}

	public String getPrefix() {
		return Constants.PREFIX_PROMOTION;
	}

	public Drawable getListImage() {
		return listImage;
	}

	public void setListImage(Drawable listImage) {
		this.listImage = listImage;
	}

	public Drawable getDetailImage(String key) {
		return detailImageMap.get(key);
	}

	public void setDetailImage(String key, Drawable detailImage) {
		detailImageMap.put(key, detailImage);
	}

	public boolean isDetailsLoaded() {
		return isDetailsLoaded;
	}

	public void setDetailsLoaded(boolean isDetailsLoaded) {
		this.isDetailsLoaded = isDetailsLoaded;
	}

	public String getDateLocation() {
		return dateLocation;
	}

	public void setDateLocation(String dateLocation) {
		this.dateLocation = dateLocation;
	}
	
	

}
