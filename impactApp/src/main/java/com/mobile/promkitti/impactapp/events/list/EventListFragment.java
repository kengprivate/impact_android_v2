package com.mobile.promkitti.impactapp.events.list;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.AmazingListView;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.util.Utility;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EventListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

	private AmazingListView amazingListView;
	private SectionListAdapter adapter;
	private View v;
	private SwipeRefreshLayout swipeLayout;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.event_listview, container, false);
		try {
			View view = v.findViewById(R.id.eventListViewLayout);
			Utility.fixBackgroundRepeat(view);

			swipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
			swipeLayout.setOnRefreshListener(this);
			swipeLayout.setColorScheme(R.color.pulldown_refresh_color_1, R.color.pulldown_refresh_color_2,
					R.color.pulldown_refresh_color_3, R.color.pulldown_refresh_color_4);

			amazingListView = (AmazingListView) v.findViewById(R.id.lsComposer);
			amazingListView.setLoadingView(inflater.inflate(R.layout.item_event_more, null));

			refresh();
			
			LinearLayout overlay = (LinearLayout)v.findViewById(R.id.loadingOverlayLayout);
			overlay.setOnTouchListener(new OnTouchListener()
			{
			    public boolean onTouch(View v, MotionEvent event)
			    {
			        return true;
			    }
			});
			
		} catch (Exception e) {
			Log.e(this.getClass().getName(),e.getMessage()==null?"":e.getMessage());
		}
		return v;
	}

	@Override
	public void onRefresh() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				swipeLayout.setRefreshing(false);
				DataController.getInstance().clearCache();
				refresh();
			}
		}, 2000);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
//		adapter.stopThread();
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(Constants.TAG_APP,"ON STOP!!");
		stopAdBannerRotation();
	}


	@Override
	public void onResume() {
		super.onResume();
		Log.d(Constants.TAG_APP,"ON Resume!!");
		startAdBannerRotation();
		if(adapter!=null) {
			adapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		Log.d(Constants.TAG_APP,"ON Pause!!");
		if(adapter!=null) {
			adapter.stopThread();
		}
	}
	
	public void stopAdBannerRotation() {
		Log.d(this.getClass().getName(), "stop ad rotation");
		if(adapter!=null) {
			adapter.stopThread();
		}
	}
	
	public void startAdBannerRotation() {
		PowerManager pm = null;
		if(getActivity()!=null) {
			pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE); 
		}
		
		if(adapter!=null && pm!=null && pm.isScreenOn()) {
			adapter.startThread();
		}
	}
	
	
	public void refresh() {
		Log.d(this.getClass().getName(), "refresh EventListFragment");
		LinearLayout overlay = (LinearLayout)v.findViewById(R.id.loadingOverlayLayout);
		overlay.setVisibility(View.VISIBLE);
		new LoadingTask().execute();
	}
	
	private class LoadingTask extends AsyncTask<Void, Void, Integer> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected Integer doInBackground(Void... val) {
	    	// Initial load of Event list 
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			String currDateString = dateFormat.format(new Date(System.currentTimeMillis()));
			try {
				DataController.getInstance().getEventList(currDateString);
			} catch (Exception e) {
				Log.e(this.getClass().getName(),e.getMessage()==null?"":e.getMessage());
				cancel(false);
			}
			return 0;
	    }
	    
	    /** The system calls this to perform work in the UI thread and delivers
	      * the result from doInBackground() */
	    protected void onPostExecute(Integer i) {
	    	if(getActivity()!=null) {
	    		adapter = new SectionListAdapter(getActivity());
				if(amazingListView !=null) {
					amazingListView.setAdapter(adapter);
					amazingListView.setOnItemClickListener(adapter);
					amazingListView.setOnScrollListener(adapter);

					// set PinnedHeaderView( traffic Header, other header);
					amazingListView.setPinnedHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_header, amazingListView, false)
					, LayoutInflater.from(getActivity()).inflate(R.layout.item_traffic_header, amazingListView, false));
					if(adapter.getEventListCount() > 0) {
						adapter.notifyMayHaveMorePages();
					}
					amazingListView.requestLayout();
		
			    	LinearLayout overlay = (LinearLayout)v.findViewById(R.id.loadingOverlayLayout);
					overlay.setVisibility(View.GONE);
					v.invalidate();
					v.refreshDrawableState();
				}
	    	}
	    }
	}
}
