package com.mobile.promkitti.impactapp.valueobjects;

import java.util.HashMap;
import java.util.Map;

import android.graphics.drawable.Drawable;

import com.mobile.promkitti.impactapp.Constants;

public class Advertisement extends Event {

	private String url;
	private String telephone;
	private boolean isDetailsLoaded = false;
	private Drawable listImage;
	private Map<String, Drawable> detailImageMap = new HashMap<String, Drawable>();
	private String dateLocation;

	public Advertisement() {
		super();
		detailImageMap = new HashMap<String, Drawable>();
	}

	public String getPrefix() {
		return Constants.PREFIX_ADVERTISEMENT;
	}

	public Drawable getListImage() {
		return listImage;
	}

	public void setListImage(Drawable listImage) {
		this.listImage = listImage;
	}

	public Drawable getDetailImage(String key) {
		return detailImageMap.get(key);
	}

	public void setDetailImage(String key, Drawable detailImage) {
		detailImageMap.put(key, detailImage);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public boolean isDetailsLoaded() {
		return isDetailsLoaded;
	}

	public void setDetailsLoaded(boolean isDetailsLoaded) {
		this.isDetailsLoaded = isDetailsLoaded;
	}

	public String getDateLocation() {
		return dateLocation;
	}

	public void setDateLocation(String dateLocation) {
		this.dateLocation = dateLocation;
	}
	
}
