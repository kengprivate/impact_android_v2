package com.mobile.promkitti.impactapp.events.detail;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mobile.promkitti.impactapp.common.EventMedia;

import java.util.ArrayList;

public class EventPagerAdapter extends FragmentStatePagerAdapter {
	private ArrayList<EventMedia> mediaList;

	public EventPagerAdapter(FragmentManager fm, ArrayList<EventMedia> mediaList) {
		super(fm);
		this.mediaList = mediaList;
	}

	@Override
	public int getCount() {
		return mediaList.size();
	}

	@Override
	public Fragment getItem(int position) {
		EventMedia media = mediaList.get(position);
		String type = media.getType().toLowerCase();
		if ("y".equals(type)) {
			return YoutubeFragment.newInstance(media);
		}
		return EventImageFragment.newInstance(media);
	}
}