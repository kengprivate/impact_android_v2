package com.mobile.promkitti.impactapp;

public class Constants {

	/* FLURRY */
	public static final boolean FLURRY_ENABLED = false;

	/* PUSH NOTIFICATION */
	public static final boolean PUSH_NOTIFICATION_ENABLED = true;
	public static final boolean PUSH_IN_PRODUCTION = true;

	/********
	 *  Production 
	 ********/
	public static final String FLURRY_API_KEY = "P3NC8K5VCR2JJ4QFD74J"; /* Production */
	public static final String SERVER_URL = "http://iphoneapp.impact.co.th/i";

	/********
	 *  Dev 
	 ********/
//	 public static final String FLURRY_API_KEY = "P38XSDH48RVTCPMCNYYY"; /* Dev */
//	 public static final String SERVER_URL = "http://iphoneapp.impact.co.th/dev";
//	public static final String SERVER_URL = "http://votetowinbasketball.com/i";

	
	public static final String TAG_ERROR = "ERR";

	/* RESULT CODE */
	public static final int RESULT_CODE_OK = 200;
	public static final int RESULT_CODE_ERROR = 500;
	/* */

	public static final String LANGUAGE_ENGLISH = "en";
	public static final String LANGUAGE_THAI = "th";
	public static final String LANGUAGE_DEFAULT = LANGUAGE_THAI;

	/* PREFIX */
	public static final String PREFIX_CALENDAR_EVENT = "EV-";
	public static final String PREFIX_AD_BANNER = "AB-";
	public static final String PREFIX_PROMOTION = "PR-";
	public static final String PREFIX_ADVERTISEMENT = "AD-";
	public static final String PREFIX_TRAFFIC = "TF-";
	public static final String PREFIX_PARKING_DETAIL = "PK-";

	/* TAG */
	public static final String TAG_EVENT_LIST_FRAGMENT = "EventListFragment";
	public static final String TAG_APP = "IMPACTAPP";

	public static final String ERR_CONNECTION_ERROR = "Unable to load the data.";

	public static final int APP_RATE_DAYS_UNTIL_PROMPT = 1;
	public static final int APP_RATE_LAUNCHES_UNTIL_PROMPT = 20;

	public static final String PREFS_NAME = "ImpactNotificationPref";
	public static final String SPACE = " ";

	private static boolean isActive = false;

	private static boolean isLocationPermissionAllowed = false;

	private Constants() {

	}

	public static synchronized void setActive(boolean value) {
		isActive = value;
	}

	public static boolean isActive() {
		return isActive;
	}


	public static boolean isLocationPermissionAllowed() {
		return isLocationPermissionAllowed;
	}

	public static void setLocationPermissionAllowed(boolean isLocationPermissionAllowed) {
		Constants.isLocationPermissionAllowed = isLocationPermissionAllowed;
	}
}
