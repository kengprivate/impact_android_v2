package com.mobile.promkitti.impactapp.fb;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mobile.promkitti.impactapp.R;

public class InstagramWebViewFragment extends Fragment {

	private WebView webView;
	private String url = "https://www.instagram.com/impactvenue";
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View v = inflater.inflate(R.layout.social_webview, container, false);
    	webView = (WebView) v.findViewById(R.id.webView);
		webView.clearCache(false);
        return v;
    }
    
    public void display() {
		WebSettings webSettings = webView.getSettings();
//		webSettings.setPluginsEnabled(true);
		webSettings.setJavaScriptEnabled(true);
//    		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultZoom(ZoomDensity.MEDIUM);
//    		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webSettings.setSupportMultipleWindows(true);
		webView.invokeZoomPicker();
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		
		webView.setWebViewClient(new WebViewClient() {  

			@Override  
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				view.loadUrl(url);
				return true;
			}

			@Override  
			public void onPageFinished(WebView view, String url)  
			{  
				if (webView!=null) {
					Log.d("PAGE","Finish loading "+webView.getUrl());
				}
			}

			public void onPageStarted(WebView view, String url, Bitmap favicon)
			{
				
			}
		});  

		webView.loadUrl(url);
	}
    
    public void stopButtonListener(View view) {
		webView.stopLoading();
		webView.clearCache(false);
	}
	
	public void reloadButtonListener(View view) {
		webView.loadUrl(url);
	}
	
	public void homeButtonListener(View view) {
		webView.loadUrl(url);
	}

	public void backButtonListener(View view) {
		webView.goBack();
	}
	
	public void nextButtonListener(View view) {
		webView.goForward();
	}
}
    