package com.mobile.promkitti.impactapp.promotion;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.common.NotificationHandler;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.NotificationUtil;

public class PromotionNotificationHandler implements NotificationHandler {

	private String id;
	private Activity activity;

	public PromotionNotificationHandler(Activity activity, String id) {
		this.id = id;
		this.activity = activity;
	}


	@Override
	public void process() {

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(activity.getIntent().getStringExtra("MSG")).setCancelable(false).setTitle("IMPACT Message");
		builder.setPositiveButton("View Now", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int actionId) {
				Intent intent = new Intent(activity, PromotionDetailActivity.class);
				intent.putExtra("ID", id);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.startActivity(intent);

				Map<String, String> params = new HashMap<String, String>();
				params.put(FlurryConstants.PARAM_PROMO_ID, id);
				FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_PROMOTION_ID, params);
				FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_PROMOTION_ID + Constants.SPACE + id);

				activity.finish();
			}
		}).setNegativeButton("View Later", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int actionId) {
				NotificationUtil.getPromotionNotificationList().addNotification(id);
				activity.finish();
			}
		});
		builder.create().show();
	}

}
