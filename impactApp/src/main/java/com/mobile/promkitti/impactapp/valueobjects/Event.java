package com.mobile.promkitti.impactapp.valueobjects;

import com.mobile.promkitti.impactapp.common.EventMedia;

import java.util.ArrayList;

public abstract class Event {

	private String id;
	private String internalId;
	private String startDate;
	private String endDate;
	private String title;
	private String description;
	private ArrayList<EventMedia> mediaList;
	private boolean hasImage = true;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		this.internalId = getPrefix() + id;
	}

	public String getInternalId() {
		return internalId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<EventMedia> getMediaList() {
		return mediaList;
	}

	public void setMediaList(ArrayList<EventMedia> mediaList) {
		this.mediaList = mediaList;
	}

	public abstract String getPrefix();

	public boolean hasImage() {
		return hasImage;
	}

	public void setHasImage(boolean hasImage) {
		this.hasImage = hasImage;
	}
}
