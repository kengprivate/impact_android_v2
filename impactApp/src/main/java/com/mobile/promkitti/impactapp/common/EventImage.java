package com.mobile.promkitti.impactapp.common;


import android.os.Parcel;
import android.os.Parcelable;

public class EventImage extends EventMedia {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public EventMedia createFromParcel(Parcel in) {
            return new EventImage();
        }

        public EventMedia[] newArray(int size) {
            return new EventImage[size];
        }
    };
}
