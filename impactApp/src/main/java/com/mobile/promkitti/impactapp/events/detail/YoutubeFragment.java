package com.mobile.promkitti.impactapp.events.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnFullscreenListener;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.common.EventYoutube;

public class YoutubeFragment extends YouTubePlayerSupportFragment {

	private YouTubePlayer p;

	public static final YoutubeFragment newInstance(EventMedia eventMedia) {
		YoutubeFragment fragment = new YoutubeFragment();
		final Bundle args = new Bundle(1);
		args.putParcelable("EVENT_MEDIA", eventMedia);
		fragment.setArguments(args);
		return fragment;
	}

	private void init() {
		initialize("AIzaSyB3vC523BHF4x_yWdEFokkxj1YH-3ZgZgM", new YoutubeOnInitializedListener());
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			init();
		} else {
			if (p != null) {
				p.release();
			}
		}
	}

	private final class YoutubeOnInitializedListener implements OnInitializedListener {

		@Override
		public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
			final EventYoutube media = getArguments().getParcelable("EVENT_MEDIA");
			Log.d("youtube", media.getId() + "  " + media.getUrl());
			if (!wasRestored) {
				p = player;
				p.cueVideo(media.getUrl());
				p.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
				p.setOnFullscreenListener(new OnFullscreenListener() {
					@Override
					public void onFullscreen(boolean isFullscreen) {
						Log.d("YoutubeFragment", "go fullscreen " + isFullscreen + media.getUrl());
						p.setFullscreen(isFullscreen);
						if (isFullscreen) {
							EventYoutube media = getArguments().getParcelable("EVENT_MEDIA");
							Intent playIntent = new Intent(Intent.ACTION_VIEW);
							playIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							playIntent.setData(Uri.parse("http://www.youtube.com/v/" + media.getUrl()));
							playIntent.putExtra("force_fullscreen", true);
							getActivity().getApplicationContext().startActivity(playIntent);
						}
					}
				});
			}

		}

		@Override
		public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
			Log.e("Youtube Error", arg1.getDeclaringClass().getName());
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (p != null) {
			p.release();
		}
	}

}