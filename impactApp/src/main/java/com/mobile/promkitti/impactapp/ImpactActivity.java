package com.mobile.promkitti.impactapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.common.NotificationList;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.NotificationUtil;
import com.mobile.promkitti.impactapp.util.Utility;

public abstract class ImpactActivity extends Activity {

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		// cast the received View to TextView so that you can get its text
		TextView textView = (TextView) v;

		if (!Utility.isBlank(textView.getText().toString())) {
			// user has long pressed your TextView
			menu.add(0, v.getId(), 0, getResources().getString(R.string.copy_text));

			// place your TextView's text in clipboard
			ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
			clipboard.setText(textView.getText());
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Constants.setActive(true);
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryUtil.onStartSession(this);
		Constants.setActive(true);
		Log.d(getClass().getName(), "onStart : " + getLocalClassName());
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryUtil.onEndSession(this);
		saveNotificationValues();
		Log.d(getClass().getName(), "onStop : " + getLocalClassName());
	}

	@Override
	protected void onPause() {
		super.onPause();
		Constants.setActive(false);
		Log.d(getClass().getName(), "onPause : " + getLocalClassName());
	}

	@Override
	protected void onResume() {
		super.onResume();
		Constants.setActive(true);
		Log.d(getClass().getName(), "onResume : " + getLocalClassName());
	}

	private void saveNotificationValues() {
		saveSpecifiedNotificationValues("promotionIdList", NotificationUtil.getPromotionNotificationList());
		saveSpecifiedNotificationValues("adIdList", NotificationUtil.getAdNotificationList());
		saveSpecifiedNotificationValues("eventIdList", NotificationUtil.getEventNotificationList());
	}

	private void saveSpecifiedNotificationValues(String name, NotificationList list) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
			if (i < list.size() - 1) {
				sb.append(",");
			}
		}

		SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(name, sb.toString());

		// Commit the edits!
		editor.commit();
	}

}
