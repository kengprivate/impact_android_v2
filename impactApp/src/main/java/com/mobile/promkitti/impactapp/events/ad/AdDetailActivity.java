package com.mobile.promkitti.impactapp.events.ad;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.promkitti.impactapp.ActivityParameters;
import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.FlurryConstants;
import com.mobile.promkitti.impactapp.FragmentImpactActivity;
import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.MainActivity;
import com.mobile.promkitti.impactapp.R;
import com.mobile.promkitti.impactapp.common.BrowserActivity;
import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.common.ShareUtils;
import com.mobile.promkitti.impactapp.util.FlurryUtil;
import com.mobile.promkitti.impactapp.util.NotificationUtil;
import com.mobile.promkitti.impactapp.util.Utility;
import com.mobile.promkitti.impactapp.valueobjects.Advertisement;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.mobile.promkitti.impactapp.Constants.SERVER_URL;

public class AdDetailActivity extends FragmentImpactActivity {

	private DataController dc = DataController.getInstance();
	private Advertisement ad;
	private String id;
	private boolean isDirect;
	private Context context;
	private int fullScreenWidth;
	private int fullScreenHeight;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ad_details);

		View view = findViewById(R.id.scrollViewLayout);
		Utility.fixBackgroundRepeat(view);
		Drawable d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.titlebar_bg);
		view.setPadding(0, d.getIntrinsicHeight() + Utility.convertDpToPx(getApplicationContext(), 1), 0, 0);

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		fullScreenWidth = dm.widthPixels;
		Drawable placeHolder = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ad_detail_placeholder);
		fullScreenHeight = fullScreenWidth * placeHolder.getIntrinsicHeight() / placeHolder.getIntrinsicWidth();
		ImageView placeHolderView = (ImageView) findViewById(R.id.adDetailImagePlaceholder);
		placeHolderView.setLayoutParams(new FrameLayout.LayoutParams(fullScreenWidth, fullScreenHeight));

		android.support.v4.view.ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setLayoutParams(new FrameLayout.LayoutParams(fullScreenWidth, fullScreenHeight));

		context = getApplicationContext();
		id = getIntent().getStringExtra(ActivityParameters.ID);
		isDirect = getIntent().getBooleanExtra(ActivityParameters.DIRECT, false);
		if (isDirect) {
			Log.d("FLURRY", FlurryConstants.PUSH_OPEN_AD_ID);
			Map<String, String> params = new HashMap<String, String>();
			params.put(FlurryConstants.PARAM_AD_ID, id);
			FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_AD_ID, params);
			FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_AD_ID + Constants.SPACE + id);
		}

		// Clear badge
		NotificationUtil.getAdNotificationList().removeNotification(id);

		Log.d("Ad ID:", id);
		FlurryUtil.onPageView();
		FlurryUtil.logEvent(FlurryConstants.PAGE_AD_DETAIL);
		new LoadingTask().execute();
	}

	@Override
	public void onBackPressed() {
		if (isDirect) {
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.setClass(getApplicationContext(), MainActivity.class);
			startActivity(intent);
			finish();
		} else {
			super.onBackPressed();
		}
	}

	public void urlAdButtonListener(View view) {
		String url = ad.getUrl();
		if (Utility.isBlank(url)) {
			return;
		}
		Intent intent = new Intent(context, BrowserActivity.class);
		intent.putExtra(ActivityParameters.URL, url);
		startActivity(intent);

		FlurryUtil.logEvent(FlurryConstants.AD_LINK_TO_WEB);
	}

	public void telAdButtonListener(View view) {
		TextView telAdButton = (TextView) findViewById(R.id.telAdButton);
//		String number = "";
//		try {
//			number = telAdButton.getText().toString().split(" ")[1];
//		} catch (Exception e) {
//			Log.e("", "Invalid Number");
//		}
		String url = "tel:" + telAdButton.getText().toString();
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
		if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
		} else {
			startActivity(intent);
			FlurryUtil.logEvent(FlurryConstants.AD_CALL);
		}

	}

	public void shareAdButtonListener(View view) {
		String contentURL = "http://www.impact.co.th";
		String imageURL = SERVER_URL + "/al/" + ad.getId() + ".png";
		ArrayList<EventMedia> mediaList = ad.getMediaList();
		for(EventMedia media : mediaList) {
			if("i".equalsIgnoreCase(media.getType())) {
				imageURL = media.getUrl();
				break;
			}
		}

		StringBuilder sb = new StringBuilder();
		sb.append(ad.getDescription());
		sb.append("\r\n\r\n");

		if (!Utility.isBlank(ad.getUrl())) {
			sb.append("URL:\r\n");
			sb.append(ad.getUrl());
			sb.append("\r\n\r\n");
			contentURL = ad.getUrl();
		}

		if (!Utility.isBlank(ad.getTelephone())) {
			sb.append("Tel:\r\n");
			sb.append(ad.getTelephone());
			sb.append("\r\n\r\n");
		}
		sb.append("Sent from my Android");

		Intent chooserIntent = ShareUtils.share(this, view, sb.toString(), ad.getTitle(), contentURL, imageURL);
		if(chooserIntent == null) {
			return;
		}

		Map<String, String> flurryParams = new HashMap<String, String>();
		flurryParams.put(FlurryConstants.PARAM_AD_ID, id);
		FlurryUtil.logEvent(FlurryConstants.SHARE_AD, flurryParams);
		FlurryUtil.logEvent(FlurryConstants.SHARE_AD + Constants.SPACE + id);

		startActivity(chooserIntent);
	}

	private class LoadingTask extends AsyncTask<Void, Void, Integer> {
		/**
		 * The system calls this to perform work in a worker thread and delivers it the parameters given to AsyncTask.execute()
		 */
		protected Integer doInBackground(Void... val) {
			try {
				dc.getAdDetail(id);
			} catch (ImpactException e) {
				return -1;
			}
			return 0;
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers the result from doInBackground()
		 */
		protected void onPostExecute(Integer i) {
			try {
				if (i < 0) {
					throw new ImpactException(getString(R.string.loading_error));
				}
				ad = dc.getAdDetail(id);

				if (ad == null) {
					finish();
					return;
				}

				ImageView placeholderView = (ImageView) findViewById(R.id.adDetailImagePlaceholder);
				placeholderView.setVisibility(View.GONE);

				// Set the pager with an adapter
				ArrayList<EventMedia> mediaList = ad.getMediaList();
				AdPagerAdapter pagerAdapter = new AdPagerAdapter(getSupportFragmentManager(), mediaList);
				final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
				viewPager.setAdapter(pagerAdapter);

				// Bind the title indicator to the adapter
				CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
				pageIndicator.setViewPager(viewPager);

				if (mediaList.size() < 2) {
					pageIndicator.setVisibility(View.GONE);
				}

				TextView titleText = (TextView) findViewById(R.id.adDetailTitleText);
				// titleText.setText(ad.getTitle() == null ? "" :
				// Utility.unEscapeHtml(ad.getTitle()));
				titleText.setText(ad.getTitle());
				registerForContextMenu(titleText);

				TextView descText = (TextView) findViewById(R.id.adDetailDescriptionText);
				// descText.setText(ad.getDescription() == null ? "" :
				// Utility.unEscapeHtml(ad.getDescription()));
				descText.setText(ad.getDescription());
				registerForContextMenu(descText);

				FrameLayout telAdLayout = (FrameLayout) findViewById(R.id.telAdLayout);
				TextView telAdButton = (TextView) findViewById(R.id.telAdButton);
				if (Utility.isBlank(ad.getTelephone())) {
					telAdLayout.setVisibility(View.GONE);
					telAdButton.setText("");
				} else {
					telAdLayout.setVisibility(View.VISIBLE);
					Locale locale = Locale.getDefault();
					String currentLanguage = locale.getLanguage();
					telAdButton.setText(ad.getTelephone());
				}

				FrameLayout urlAdLayout = (FrameLayout) findViewById(R.id.urlAdLayout);
				if (Utility.isBlank(ad.getUrl())) {
					urlAdLayout.setVisibility(View.GONE);
				} else {
					urlAdLayout.setVisibility(View.VISIBLE);
				}

			} catch (ImpactException e) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage(Constants.ERR_CONNECTION_ERROR).setCancelable(false).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				});
				builder.create().show();
			}
			LinearLayout contentLayout = (LinearLayout) findViewById(R.id.contentLayout);
			contentLayout.setVisibility(View.VISIBLE);
			LinearLayout overlay = (LinearLayout) findViewById(R.id.loadingOverlayLayout);
			overlay.setVisibility(View.GONE);

			Map<String, String> params = new HashMap<String, String>();
			params.put(FlurryConstants.PARAM_AD_ID, id);
			FlurryUtil.logEvent(FlurryConstants.VIEW_AD_ID, params);
			FlurryUtil.logEvent(FlurryConstants.VIEW_AD_ID + Constants.SPACE + id);
		}
	}

}
