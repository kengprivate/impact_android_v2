package com.mobile.promkitti.impactapp.fb;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mobile.promkitti.impactapp.ActivityParameters;
import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.R;

public class SocialWebViewActivity extends ImpactActivity {

    private WebView webView;
    private String url = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.social_webview);
        url = getIntent().getStringExtra(ActivityParameters.URL);
        webView = (WebView) findViewById(R.id.webView);
        webView.clearCache(false);
        display();
    }


    public void display() {
        WebSettings webSettings = webView.getSettings();
//		webSettings.setPluginsEnabled(true);
        webSettings.setJavaScriptEnabled(true);
//    		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webSettings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
//    		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setSupportMultipleWindows(true);
        webView.invokeZoomPicker();
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (webView != null) {
                    Log.d("PAGE", "Finish loading " + webView.getUrl());
                }
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {

            }
        });

        webView.loadUrl(url);
    }

    public void backButtonListener(View view) {
        webView.goBack();
    }

    public void forwardButtonListener(View view) {
        webView.goForward();
    }

    public void homeButtonListener(View view) {
        webView.loadUrl(url);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
    }
    

}
