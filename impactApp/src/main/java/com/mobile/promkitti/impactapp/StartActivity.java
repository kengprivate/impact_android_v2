package com.mobile.promkitti.impactapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mobile.promkitti.impactapp.common.DataController;
import com.mobile.promkitti.impactapp.react.ImpactReactActivity;
import com.mobile.promkitti.impactapp.util.FlurryUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public class StartActivity extends ImpactReactActivity {

	
    private static final int STOPSPLASH = 0;
    private static final long SPLASHTIME = 2000;
    private static final String DATE_FORMAT = "yyyyMMdd"; 
    private ImageView splash;
    private Context context;
    private SharedPreferences mPrefs;
    private SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    private static Handler splashHandler = new Handler();
    
    protected void removeSplashScreen() {
    	splash.setVisibility(View.GONE);
		Intent intent = new Intent(StartActivity.this, MainActivity.class);
		context.startActivity(intent);
		finish();
    }
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
//    	if (savedInstanceState != null) {
//    		Intent intent = new Intent(StartActivity.this, MainActivity.class);
//    		context.startActivity(intent);
//    		finish();
//    		return;
//		}
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);

        this.context = this;
        splash = (ImageView) findViewById(R.id.splashscreen);
        Message msg = new Message();
        msg.what = STOPSPLASH;

        /* Init React Native */
        getReactInstance();
        
        /* Check calendar and remove the passed date event */
        Date todayDate = new Date(System.currentTimeMillis());
        mPrefs = getSharedPreferences("ImpactApp", Activity.MODE_PRIVATE);
		Map<String, ?> allVal = mPrefs.getAll();
		Set<String> keySet = allVal.keySet();
		Object keys[] = keySet.toArray();
		for(int i = 0; i < keys.length; i++) {
			String key = (String)keys[i];
			if(key.startsWith(Constants.PREFIX_CALENDAR_EVENT)) {
				String eventCalendarInfo = mPrefs.getString(key, null);
				if(eventCalendarInfo != null) {
					String str[] = eventCalendarInfo.split(":");
					String endDateString = str[0];
					try {
						Date endDate = dateFormat.parse(endDateString);
						if(endDate.before(todayDate)) {
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.remove(key);
							editor.commit();
						}
					} catch (ParseException e) {
						Log.e(this.getClass().getName(), e.getLocalizedMessage());
					}
				}
			}
		}

        
        /* Check Screen Density */ 
        DisplayMetrics metrics = new DisplayMetrics();    
        getWindowManager().getDefaultDisplay().getMetrics(metrics);    
        int screenDensity = metrics.densityDpi;
        Log.d("DENSITY", screenDensity+"");
        
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.mobile.promkitti.impactapp",PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("kh:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        
        DataController.getInstance().clearCache();
        
		boolean isDirect = getIntent().getBooleanExtra(ActivityParameters.DIRECT, false);
		if(isDirect) {
			Log.d("FLURRY", FlurryConstants.PUSH_OPEN_APP);
			FlurryUtil.logEvent(FlurryConstants.PUSH_OPEN_APP);
		}

        splashHandler.postDelayed(new Runnable() {
        	@Override
            public void run() {
              removeSplashScreen();
            }
          }, SPLASHTIME);

    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	finish();
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	Constants.setActive(true);
    	FlurryUtil.onStartSession(this);
    }
    
    @Override
	protected void onStop() {
		super.onStop();
		if(splashHandler != null) {
			splashHandler.removeMessages(STOPSPLASH);
		}
        FlurryUtil.onEndSession(this);
	}

}