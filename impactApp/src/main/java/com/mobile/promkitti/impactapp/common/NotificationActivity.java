package com.mobile.promkitti.impactapp.common;

import android.os.Bundle;

import com.mobile.promkitti.impactapp.ImpactActivity;
import com.mobile.promkitti.impactapp.events.ad.AdNotificationHandler;
import com.mobile.promkitti.impactapp.events.detail.EventNotificationHandler;
import com.mobile.promkitti.impactapp.promotion.PromotionNotificationHandler;
import com.mobile.promkitti.impactapp.util.Utility;

public class NotificationActivity extends ImpactActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String promotionId = getIntent().getStringExtra("PROMOTION_ID");
		String adId = getIntent().getStringExtra("AD_ID");
		String eventId = getIntent().getStringExtra("EVENT_ID");
		NotificationHandler nHandler = null;
		if (!Utility.isBlank(promotionId)) {
			nHandler = new PromotionNotificationHandler(this, promotionId);

		} else if (!Utility.isBlank(adId)) {
			nHandler = new AdNotificationHandler(this, adId);

		} else if (!Utility.isBlank(eventId)) {
			nHandler = new EventNotificationHandler(this, eventId);

		} else {
			nHandler = new DefaultNotificationHandler(this);
		}
		nHandler.process();
	}

}
