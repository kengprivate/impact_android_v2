package com.mobile.promkitti.impactapp.react;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.mobile.promkitti.impactapp.common.EventMedia;
import com.mobile.promkitti.impactapp.events.detail.YoutubeFragment;

import java.util.ArrayList;

public class ParkingDetailPagerAdapter extends FragmentStatePagerAdapter {
	private ArrayList<EventMedia> mediaList;

	public ParkingDetailPagerAdapter(FragmentManager fm, ArrayList<EventMedia> mediaList) {
		super(fm);
		this.mediaList = mediaList;
	}

	@Override
	public int getCount() {
		return mediaList.size();
	}

	@Override
	public Fragment getItem(int position) {
		EventMedia media = mediaList.get(position);
		String type = media.getType().toLowerCase();
		Log.e("MEDIA_URL", "mediaURL:"+media.getUrl());
		if ("y".equals(type)) {
			return YoutubeFragment.newInstance(media);
		}
		return ParkingDetailImageFragment.newInstance(media);
	}
}