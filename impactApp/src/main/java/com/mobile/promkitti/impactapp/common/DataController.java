package com.mobile.promkitti.impactapp.common;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.Pair;

import com.mobile.promkitti.impactapp.Constants;
import com.mobile.promkitti.impactapp.ImpactException;
import com.mobile.promkitti.impactapp.util.DateUtil;
import com.mobile.promkitti.impactapp.util.Utility;
import com.mobile.promkitti.impactapp.valueobjects.AdBanner;
import com.mobile.promkitti.impactapp.valueobjects.Advertisement;
import com.mobile.promkitti.impactapp.valueobjects.CalendarEvent;
import com.mobile.promkitti.impactapp.valueobjects.Event;
import com.mobile.promkitti.impactapp.valueobjects.EventListObject;
import com.mobile.promkitti.impactapp.valueobjects.ParkingDetail;
import com.mobile.promkitti.impactapp.valueobjects.Promotion;
import com.mobile.promkitti.impactapp.valueobjects.Traffic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.mobile.promkitti.impactapp.Constants.SERVER_URL;

public class DataController {

    private static final String DATE_FORMAT = "yyyyMMdd";
    private static int LIST_LENGTH = 25;

    private static DataController instance;
    private static HashMap<String, Event> eventCache = new HashMap<String, Event>(); // key = event id e.g. EV-xxx, AB-xxx
    // to store all events (include ad banners)
    // for eventlist tab and calendar tab
    private static ArrayList<String> eventList = new ArrayList<String>(); // to store events listing on eventlist tab
    // both events and ad banners
    private static HashMap<String, ArrayList<String>> calendarMap = new HashMap<String, ArrayList<String>>(); // key = yyyyMMdd e.g. 20120618
    // to store events listing on
    // calendar tab events only

    private static ArrayList<String> adList = new ArrayList<String>(); // to store events listing on eventlist tab
    private static ArrayList<String> promotionList = new ArrayList<String>(); // to store all promotions
    private static Traffic trafficCache = null;

    private static Date lastLoadedDate = new Date(System.currentTimeMillis());

    private static HashMap<String, Drawable> parkingDetailImageCache = new HashMap<String, Drawable>(); // key = event id e.g. EV-xxx, AB-xxx

    private Drawable dummyListImage;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    private static String language = Constants.LANGUAGE_THAI;

    public static DataController getInstance() {
        if (instance == null) {
            instance = new DataController();
        }
        return instance;
    }

    private DataController() {

    }

    public void setLanguage(String lang) {
        this.language = lang;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setDummyListImage(Drawable drawable) {
        dummyListImage = drawable;
    }

    public void clearCache() {
        // Reset all data
        eventList.clear();
        calendarMap.clear();
        adList.clear();
        trafficCache = null;
    }

    public void clearPromotionCache() {
        promotionList.clear();
        ArrayList<String> removeList = new ArrayList<String>();
        Set<String> keySet = eventCache.keySet();
        for (Iterator<String> i = keySet.iterator(); i.hasNext(); ) {
            String key = i.next();
            if (key.startsWith(Constants.PREFIX_PROMOTION)) {
                removeList.add(key);
            }
        }

        for (Iterator<String> i = removeList.iterator(); i.hasNext(); ) {
            String key = i.next();
            eventCache.remove(key);
        }
    }

    public void clearCalendarCache() {
        // Reset all Ad data
        eventList.clear();
        calendarMap.clear();
        ArrayList<String> removeList = new ArrayList<String>();
        Set<String> keySet = eventCache.keySet();
        for (Iterator<String> i = keySet.iterator(); i.hasNext(); ) {
            String key = i.next();
            if (key.startsWith(Constants.PREFIX_CALENDAR_EVENT)) {
                removeList.add(key);
            }
        }

        for (Iterator<String> i = removeList.iterator(); i.hasNext(); ) {
            String key = i.next();
            eventCache.remove(key);
        }

        // Reset lastLoadDate
        lastLoadedDate = new Date(System.currentTimeMillis());
    }

    public void clearAdCache() {
        // Reset all Ad data
        adList.clear();
        ArrayList<String> removeList = new ArrayList<String>();
        Set<String> keySet = eventCache.keySet();
        for (Iterator<String> i = keySet.iterator(); i.hasNext(); ) {
            String key = i.next();
            if (key.startsWith(Constants.PREFIX_ADVERTISEMENT)) {
                removeList.add(key);
            }
        }

        for (Iterator<String> i = removeList.iterator(); i.hasNext(); ) {
            String key = i.next();
            eventCache.remove(key);
        }
    }

    // clear cache and load first 25 events
    public void initCalendarEvents() {

    }

    private int EVENT_TYPE_CALENDAR = 1;
    private int EVENT_TYPE_EVENT = 2;

    public void updateSharedEventCache(String startDate, String endDate, int eventType) throws Exception, ParseException {
        EventListObject eventListObject = new EventListObject();
        /* search for existing data in cache */
        int existingData = checkExistingData(startDate, endDate, eventType);

        if (existingData == EXISTING_DATA_ALL) {
            return;

        } else if (existingData == EXISTING_DATA_NONE) {
            // Reset all data
            eventList.clear();
            calendarMap.clear();
            eventListObject = requestEvents(startDate, null);

        } else if (existingData == EXISTING_DATA_SOME) {
            if (eventType == EVENT_TYPE_EVENT) {
                // if there's something
                String mostUpdatedEvent = eventList.get(eventList.size() - 1);
                Event event = eventCache.get(mostUpdatedEvent);
                if (event instanceof CalendarEvent) {
                    String newStartDate = event.getStartDate();
                    eventListObject = requestEvents(newStartDate, null);
                }
            } else if (eventType == EVENT_TYPE_CALENDAR) {
                eventListObject = requestEvents(startDate, endDate);

            } else {
                throw new Exception("bad event type");
            }
        } else {
            throw new Exception("bad existing data level");
        }

        // Add advertisement banner into eventCache and eventList
        for (Iterator<AdBanner> j = eventListObject.getAdList().iterator(); j.hasNext(); ) {
            AdBanner event = (AdBanner) j.next();
            if (!eventList.contains(event.getInternalId())) {
                eventCache.put(event.getInternalId(), event);
                eventList.add(event.getInternalId());
            }
        }

        Date currentDate = dateFormat.parse(dateFormat.format(new Date()));
        // Add calendar event into eventCache, eventlist, and calendarMap
        for (Iterator<CalendarEvent> j = eventListObject.getEventList().iterator(); j.hasNext(); ) {
            CalendarEvent event = (CalendarEvent) j.next();
            if (!eventList.contains(event.getInternalId()) && !dateFormat.parse(event.getEndDate()).before(currentDate)) {
                eventCache.put(event.getInternalId(), event);
                eventList.add(event.getInternalId());

                // Add event into calendarMap
                Date utilStartDate;
                Date utilEndDate;
                synchronized (dateFormat) {
                    utilStartDate = dateFormat.parse(event.getStartDate());
                    utilEndDate = dateFormat.parse(event.getEndDate());
                }
                // Log.e("utilStartDate", utilStartDate.toString());
                for (Date utilCurrDate = utilStartDate; !utilCurrDate.after(utilEndDate); utilCurrDate = DateUtil.addDays(utilCurrDate, 1)) {
                    String calendarKey;
                    synchronized (dateFormat) {
                        calendarKey = dateFormat.format(utilCurrDate);
                    }
                    // Log.e("CalendarMapDate", calendarKey);
                    ArrayList<String> calendarMapKeyList = calendarMap.get(calendarKey);
                    if (calendarMapKeyList == null) {
                        calendarMapKeyList = new ArrayList<String>();
                        calendarMapKeyList.add(event.getInternalId());
                        calendarMap.put(calendarKey, calendarMapKeyList);
                    } else {
                        if (!calendarMapKeyList.contains(event.getInternalId())) {
                            calendarMapKeyList.add(event.getInternalId());
                        }
                    }
                }
                if (utilStartDate.after(lastLoadedDate)) {
                    lastLoadedDate = utilStartDate;
                }
            }
        }
    }

    private int EXISTING_DATA_NONE = 0;
    private int EXISTING_DATA_SOME = 1;
    private int EXISTING_DATA_ALL = 2;

    private int checkExistingData(String startDateString, String endDateString, int eventType) throws ParseException, Exception {
        Date reqStartDate = dateFormat.parse(startDateString);
        Date reqEndDate = new Date();
        if (endDateString != null) {
            reqEndDate = dateFormat.parse(endDateString);
        }
        if (eventList.size() == 0) {
            return EXISTING_DATA_NONE;
        } else {
            if (eventType == EVENT_TYPE_EVENT) {
                int validEventCount = 0;
                for (Iterator<String> i = eventList.iterator(); i.hasNext(); ) {
                    String eventId = i.next();
                    Event event = eventCache.get(eventId);
                    Date eventEndDate = dateFormat.parse(event.getEndDate());
                    if (reqStartDate.after(eventEndDate)) {
                        continue;
                    } else {
                        validEventCount++;
                    }
                }
                if (validEventCount == 0) {
                    return EXISTING_DATA_NONE;
                } else if (validEventCount < LIST_LENGTH) {
                    return EXISTING_DATA_SOME;
                } else {
                    return EXISTING_DATA_ALL;
                }
            } else if (eventType == EVENT_TYPE_CALENDAR) {
                String eventId = eventList.get(eventList.size() - 1);
                Event event = eventCache.get(eventId);
                Date eventStartDate = dateFormat.parse(event.getStartDate());
                if (reqEndDate.after(eventStartDate)) {
                    return EXISTING_DATA_SOME;
                } else {
                    return EXISTING_DATA_ALL;
                }
            } else {
                throw new Exception("bad eventType case");
            }
        }
    }

    private EventListObject requestEvents(String startDate, String endDate) throws JSONException, ParseException, IOException {
//        Log.w("load events:", "language: "+ language);

        EventListObject eventListObject = new EventListObject();
        String endDateString = "";
        if (endDate != null) {
            endDateString = "&e=" + endDate;
        }

        String jsonString = getAsString(SERVER_URL + "/events.php?b=1&l=" + this.language + "&s=" + startDate + endDateString);
        JSONObject json = new JSONObject(jsonString);
        String firstDate = json.getString("f");
        String lastDate = json.getString("l");
        JSONArray eventArray = json.getJSONArray("c");

        eventListObject.setFirstDate(firstDate);
        eventListObject.setLastDate(lastDate);

        ArrayList<CalendarEvent> eventList = new ArrayList<CalendarEvent>();
        for (int i = 0; i < eventArray.length(); i++) {
            JSONObject eventJSONObject = eventArray.getJSONObject(i);
            CalendarEvent event = new CalendarEvent();
            event.setTitle(Utility.unEscapeHtml(eventJSONObject.getString("n")));
            event.setId(eventJSONObject.getString("i"));
            event.setDateLocation(eventJSONObject.getString("d"));
            event.setTimestamp(eventJSONObject.getString("t"));
            event.setStartDate(eventJSONObject.getString("s"));
            event.setEndDate(eventJSONObject.getString("e"));
            eventList.add(event);
        }
        eventListObject.setEventList(eventList);

        ArrayList<AdBanner> adList = new ArrayList<AdBanner>();
        JSONArray bannerArray = json.getJSONArray("b");
        for (int i = 0; i < bannerArray.length(); i++) {
            JSONObject bannerJSONObject = bannerArray.getJSONObject(i);
            AdBanner ad = new AdBanner();
            ad.setId(bannerJSONObject.getString("i"));
            ad.setStartDate(bannerJSONObject.getString("s"));
            ad.setEndDate(bannerJSONObject.getString("e"));
            ad.setFileType(bannerJSONObject.getString("x"));
            ad.setDisplayTime(bannerJSONObject.getString("t"));
            adList.add(ad);
        }
        eventListObject.setAdList(adList);

        return eventListObject;
    }

    public EventListObject getEventList(String startDate) throws ImpactException {
        EventListObject eventListObject = new EventListObject();

        try {
            updateSharedEventCache(startDate, null, EVENT_TYPE_EVENT);

            for (Iterator<String> i = eventList.iterator(); i.hasNext(); ) {
                String key = i.next();

                // Double check to prevent duplicate even in the list
                if (eventListObject.getEventList().contains(key)) {
                    continue;
                }

                Event ev = eventCache.get(key);
                if (ev instanceof CalendarEvent) {
                    eventListObject.getEventList().add((CalendarEvent) ev);

                } else { // Assuming it's Advertisement banner
                    eventListObject.getAdList().add((AdBanner) ev);
                }
            }

        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }
        return eventListObject;
    }

    public EventListObject getMoreEventList(String startDate, String lastEventId) throws ImpactException {

        EventListObject eventListObject = new EventListObject();
        boolean foundFlag = false;
        try {
            updateSharedEventCache(startDate, null, EVENT_TYPE_EVENT);

            for (Iterator<String> i = eventList.iterator(); i.hasNext(); ) {
                String key = i.next();
                Event ev = eventCache.get(key);
                if (ev instanceof CalendarEvent) {

                    if (foundFlag) {
                        eventListObject.getEventList().add((CalendarEvent) ev);
                    }
                    if (lastEventId.equals(ev.getInternalId())) {
                        foundFlag = true;
                    }
                }
            }

        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }
        return eventListObject;
    }

    public Map<String, ArrayList<String>> getCalendarEvents(String currDateString) throws ImpactException {
        try {
            Date currDate = dateFormat.parse(currDateString);
            Calendar cal = Calendar.getInstance();
            cal.set(currDate.getYear() + 1900, currDate.getMonth(), cal.getActualMaximum(Calendar.DATE));
            int weekDayOfLastDate = cal.get(Calendar.DAY_OF_WEEK) - 1;
            int leadingSpaces = (6 - weekDayOfLastDate);
            cal.add(Calendar.DATE, leadingSpaces);
            Date utilEndDate = new Date(cal.getTimeInMillis());
            String endDate = dateFormat.format(utilEndDate);

            cal.set(currDate.getYear() + 1900, currDate.getMonth(), 1);
            int weekDayOfFirstDate = cal.get(Calendar.DAY_OF_WEEK) - 1;
            int trailingSpaces = weekDayOfFirstDate;
            cal.add(Calendar.DATE, -trailingSpaces);
            String startDate = dateFormat.format(new Date(cal.getTimeInMillis()));

            if (lastLoadedDate.before(utilEndDate)) {
                updateSharedEventCache(startDate, endDate, EVENT_TYPE_CALENDAR);
            }
        } catch (Exception e) {
            throw new ImpactException(e.getMessage());
        }
        return calendarMap;
    }

    public Event getCalendarEventBrief(String internalId) {
        return eventCache.get(internalId);
    }

    public CalendarEvent getEventDetail(String eventId) throws ImpactException {

        CalendarEvent event = (CalendarEvent) eventCache.get(Constants.PREFIX_CALENDAR_EVENT + eventId);
        if (event != null && event.isDetailsLoaded()) {
            return event;
        } else if (event == null) {
            event = new CalendarEvent();
        }

        try {
//            Log.d("LANGUAGE", language);
            String jsonString = getAsString(SERVER_URL + "/eventdetails.php?v=3&d=a&l=" + language + "&id=" + eventId);
            JSONObject json = new JSONObject(jsonString);
            String id = json.getString("i");

            String desc = json.isNull("d") ? "" : Utility.unEscapeHtml(json.getString("d"));
            String title = json.isNull("na") ? "" : Utility.unEscapeHtml(json.getString("na"));
            String eventUrl = json.isNull("u") ? "" : json.getString("u");
            String location = json.isNull("lc") ? "" : json.getString("lc");
            String dateLocation = json.isNull("dt") ? "" : json.getString("dt");
            String ticketPrice = json.isNull("tp") ? "" : json.getString("tp");
            String organizerInfo = json.isNull("od") ? "" : Utility.unEscapeHtml(json.getString("od"));

            String ticketTel = json.isNull("tt1") ? "" : json.getString("tt1");
            String ticketUrl = json.isNull("tu") ? "" : json.getString("tu");
            String fbUrl = json.isNull("fu") ? "" : json.getString("fu");
            String fbShareUrl = json.isNull("fsu") ? "" : json.getString("fsu");

            String startDate = json.isNull("sd") ? "" : json.getString("sd");
            String endDate = json.isNull("ed") ? "" : json.getString("ed");
            String startTime = json.isNull("st") ? "" : json.getString("st");
            startTime = startTime.isEmpty()?"00:00":startTime;

            String endTime = json.isNull("et") ? "" : json.getString("et");
            endTime = endTime.isEmpty()?"23:59":endTime;

            String lat = json.isNull("lat") ? "" : json.getString("lat");
            String lon = json.isNull("lon") ? "" : json.getString("lon");

            JSONArray mediaArray = json.getJSONArray("m");
            ArrayList<EventMedia> mediaList = new ArrayList<EventMedia>();
            for (int i = 0; i < mediaArray.length(); i++) {
                JSONObject mediaJSONObject = mediaArray.getJSONObject(i);

                String type = mediaJSONObject.getString("t").toLowerCase();
                if ("y".equals(type)) {
                    Log.d(this.getClass().getSimpleName(), "event " + id + " media " + i + " is youtube");
                    EventYoutube media = new EventYoutube();
                    media.setId(id);
                    media.setType(type);
                    media.setUrl(mediaJSONObject.getString("u"));
                    mediaList.add(media);
                } else {
                    Log.d(this.getClass().getSimpleName(), "event " + id + " media " + i + " is image");
                    EventImage media = new EventImage();
                    media.setId(id);
                    media.setType(type);
                    media.setUrl(mediaJSONObject.getString("u"));
                    mediaList.add(media);
                }
            }

            event.setStartDateTime(DateUtil.convertEventDateTimeToString(startDate + startTime));
            event.setEndDateTime(DateUtil.convertEventDateTimeToString(endDate + endTime));

            event.setId(id);
            event.setTitle(title);
            event.setDescription(desc);
            event.setUrl(eventUrl);
            event.setLocations(location);
            event.setDateLocation(dateLocation);
            event.setTicketPrice(ticketPrice);
            event.setTicketUrl(ticketUrl);
            event.setFbUrl(fbUrl);
            event.setFbShareUrl(fbShareUrl);
            event.setOrganizerInfo(organizerInfo);
            event.setTicketTel(ticketTel);
            event.setLat(lat);
            event.setLon(lon);
            event.setMediaList(mediaList);
            event.setDetailsLoaded(true);
            eventCache.put(event.getInternalId(), event);

        } catch (Exception e) {
            String errMsg = "Unknown Exception";
            if(e!=null) {
                errMsg = e.getMessage();
            }
            Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
            throw new ImpactException(errMsg, e);
        }

        return event;
    }

    public Drawable getEventListImage(String internalId) throws ImpactException {
        Drawable drawableImage = null;
        try {
            Event event = eventCache.get(internalId);
            if (event == null) {
                return null;
            }
            if (event instanceof CalendarEvent) {
                drawableImage = ((CalendarEvent) event).getListImage();
            } else { // assuming it's Advertisement banner
                drawableImage = ((AdBanner) event).getBannerImage();
            }
            if (drawableImage != null) {
                return drawableImage;
            }

            StringBuilder builder = new StringBuilder();
            if (event instanceof CalendarEvent) {
                builder.append(SERVER_URL + "/el/");
                builder.append(((CalendarEvent) event).getId());
                builder.append(".png");
                InputStream imageStream = get(builder.toString());
                drawableImage = Drawable.createFromStream(imageStream, "resource-name");

				/* If there is no image, use the default one */
                if (drawableImage == null) {
                    drawableImage = dummyListImage;
                    event.setHasImage(false);
                }

                ((CalendarEvent) event).setListImage(drawableImage);
            } else { // Assuming it's Advertisement banner
                builder.append(SERVER_URL + "/el/");
                builder.append(((AdBanner) event).getId());
                builder.append("." + ((AdBanner) event).getFileType());
                InputStream imageStream = get(builder.toString());
                drawableImage = Drawable.createFromStream(imageStream, "resource-name");
                ((AdBanner) event).setBannerImage(drawableImage);
            }
            return drawableImage;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage());
        }
    }

    public Drawable getEventDetailImage(String id, String url) throws ImpactException {
        Drawable drawableImage = null;
        try {
            CalendarEvent event = (CalendarEvent) eventCache.get(Constants.PREFIX_CALENDAR_EVENT + id);
            if (event == null) {
                return null;
            }
            drawableImage = event.getDetailImage(url);
            if (drawableImage != null) {
                return drawableImage;
            }

            InputStream imageStream = get(url);
            drawableImage = Drawable.createFromStream(imageStream, "resource-name");
            event.setDetailImage(url, drawableImage);
            return drawableImage;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage());
        }
    }

    public void setEventDetailImage(String id, String key, Drawable image) {
        CalendarEvent event = (CalendarEvent) eventCache.get(Constants.PREFIX_CALENDAR_EVENT + id);
        if (event != null) {
            event.setDetailImage(key, image);
        }
    }

    public boolean isAdBannerImageLoaded(String id) {
        AdBanner adBanner = (AdBanner) eventCache.get(id);
        if (adBanner == null) {
            return false;
        }
        Drawable drawableImage = adBanner.getBannerImage();
        if (drawableImage != null) {
            return true;
        }
        return false;
    }

    public Drawable getAdBannerImage(String id) throws ImpactException {
        try {
            AdBanner adBanner = (AdBanner) eventCache.get(id);
            if (adBanner == null) {
                return null;
            }
            Drawable drawableImage = adBanner.getBannerImage();
            if (drawableImage != null) {
                return drawableImage;
            }
            StringBuilder builder = new StringBuilder();
            // http://iphoneapp.impact.co.th/dev/bl/13.jpg
            builder.append(SERVER_URL);
            builder.append("/bl/");
            builder.append(adBanner.getId());
            builder.append("." + adBanner.getFileType());
            Log.d("DataController", "load ad banner: " + builder.toString() + " and wait for " + adBanner.getDisplayTime() + " secs.");
            InputStream imageStream = get(builder.toString());
            drawableImage = Drawable.createFromStream(imageStream, "resource-name");
            adBanner.setBannerImage(drawableImage);
            return drawableImage;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage());
        }
    }

    public synchronized ArrayList<Advertisement> getAdList(String currDateString) throws ImpactException {
        ArrayList<Advertisement> list = new ArrayList<Advertisement>();
        if (adList != null && adList.size() > 0) {
            for (Iterator<String> i = adList.iterator(); i.hasNext(); ) {
                Advertisement event = (Advertisement) eventCache.get(Constants.PREFIX_ADVERTISEMENT + i.next());
                list.add(event);
            }
            Log.d("", "cache: " + list.size());
            return list;
        }
//        Log.i("load AdList:", "language: "+ language);
        try {
            String jsonString = getAsString(SERVER_URL + "/ads.php?l=" + language + "&s=" + currDateString);
            JSONObject json = new JSONObject(jsonString);
            JSONArray eventArray = json.getJSONArray("c");
            for (int i = 0; i < eventArray.length(); i++) {
                JSONObject eventJSONObject = eventArray.getJSONObject(i);
                Advertisement ad = new Advertisement();
                ad.setTitle(Utility.unEscapeHtml(eventJSONObject.getString("n")));
                ad.setId(eventJSONObject.getString("i"));
                ad.setDateLocation(eventJSONObject.getString("d") == null ? "" : Utility.unEscapeHtml(eventJSONObject.getString("d")));
                eventCache.put(ad.getInternalId(), ad);
                adList.add(ad.getId());
                list.add(ad);
            }
            // Log.d("","fresh: "+list.size());

        } catch (Exception e) {
            throw new ImpactException(e.getMessage());
        }
        return list;
    }

    public Advertisement getAdDetail(String eventId) throws ImpactException {
        Advertisement event = (Advertisement) eventCache.get(Constants.PREFIX_ADVERTISEMENT + eventId);
        if (event != null && event.isDetailsLoaded()) {
            return event;
        }

        try {
            String jsonString = getAsString(SERVER_URL + "/addetails.php?v=3&l=" + language + "&id=" + eventId);
            JSONObject json = new JSONObject(jsonString);
            String id = json.getString("i");

            String desc = json.isNull("d") ? "" : Utility.unEscapeHtml(json.getString("d"));
            String title = json.isNull("n") ? "" : Utility.unEscapeHtml(json.getString("n"));
            String eventUrl = json.isNull("u") ? "" : json.getString("u");
            String telephone = json.isNull("t") ? "" : json.getString("t");

            JSONArray mediaArray = json.getJSONArray("m");
            ArrayList<EventMedia> mediaList = new ArrayList<EventMedia>();
            for (int i = 0; i < mediaArray.length(); i++) {
                JSONObject mediaJSONObject = mediaArray.getJSONObject(i);

                String type = mediaJSONObject.getString("t").toLowerCase();
                if ("y".equals(type)) {
                    Log.d(this.getClass().getSimpleName(), "event " + id + " media " + i + " is youtube");
                    EventYoutube media = new EventYoutube();
                    media.setId(id);
                    media.setType(type);
                    media.setUrl(mediaJSONObject.getString("u"));
                    mediaList.add(media);
                } else {
                    Log.d(this.getClass().getSimpleName(), "event " + id + " media " + i + " is image");
                    EventImage media = new EventImage();
                    media.setId(id);
                    media.setType(type);
                    media.setUrl(mediaJSONObject.getString("u"));
                    mediaList.add(media);
                }
            }

            if (event == null) {
                event = new Advertisement();
            }
            event.setId(id);
            event.setTitle(title);
            event.setDescription(desc);
            event.setUrl(eventUrl);
            event.setTelephone(telephone);
            event.setMediaList(mediaList);
            event.setDetailsLoaded(true);
            eventCache.put(event.getInternalId(), event);

        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }

        return event;
    }

    public Drawable getAdListImage(String id) throws ImpactException {
        try {
            Advertisement advertisement = (Advertisement) eventCache.get(Constants.PREFIX_ADVERTISEMENT + id);
            if (advertisement == null) {
                return null;
            }
            Drawable drawableImage = advertisement.getListImage();
            if (drawableImage != null) {
                return drawableImage;
            }
            StringBuilder builder = new StringBuilder();
            // http://iphoneapp.impact.co.th/dev/bl/13.jpg
            builder.append(SERVER_URL);
            builder.append("/al/");
            builder.append(id);
            builder.append(".png");
            InputStream imageStream = get(builder.toString());
            drawableImage = Drawable.createFromStream(imageStream, "resource-name");
            advertisement.setListImage(drawableImage);
            return drawableImage;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }
    }

    public Drawable getAdDetailImage(String id, String url) throws ImpactException {
        try {
            Advertisement advertisement = (Advertisement) eventCache.get(Constants.PREFIX_ADVERTISEMENT + id);
            if (advertisement == null) {
                return null;
            }
            Drawable drawableImage = advertisement.getDetailImage(url);
            if (drawableImage != null) {
                return drawableImage;
            }
            InputStream imageStream = get(url);
            drawableImage = Drawable.createFromStream(imageStream, "resource-name");
            advertisement.setDetailImage(url, drawableImage);
            return drawableImage;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }
    }


    public void setAdDetailImage(String id, String key, Drawable image) {
        Advertisement ad = (Advertisement) eventCache.get(Constants.PREFIX_ADVERTISEMENT + id);
        if (ad != null) {
            ad.setDetailImage(key, image);
        }
    }


    public synchronized List<Promotion> getPromotionList() throws ImpactException {

        List<Promotion> result = new ArrayList<Promotion>();
        try {
//            Log.i("load PromoList:", "language: "+ language);

            if (promotionList.size() == 0) {
                String jsonString = getAsString(SERVER_URL + "/promotions.php?l=" + language);
                JSONObject json = new JSONObject(jsonString);
                JSONArray bannerArray = json.getJSONArray("c");
                for (int i = 0; i < bannerArray.length(); i++) {
                    JSONObject bannerJSONObject = bannerArray.getJSONObject(i);
                    Promotion promotion = new Promotion();
                    promotion.setId(bannerJSONObject.getString("i"));
                    promotion.setTitle(Utility.unEscapeHtml(bannerJSONObject.getString("n")));
                    promotion.setDateLocation(bannerJSONObject.getString("d") == null ? "" : Utility.unEscapeHtml(bannerJSONObject.getString("d")));
                    eventCache.put(promotion.getInternalId(), promotion);
                    promotionList.add(promotion.getInternalId());
                }
            }

            for (Iterator<String> i = promotionList.iterator(); i.hasNext(); ) {
                String key = i.next();
                Event ev = eventCache.get(key);
                result.add((Promotion) ev);
            }

        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }
        return result;
    }

    public Promotion getPromotionDetail(String eventId) throws ImpactException {
        Promotion event = (Promotion) eventCache.get(Constants.PREFIX_PROMOTION + eventId);
        if (event != null && event.isDetailsLoaded()) {
            return event;
        }

        try {
//            Log.i("load promo detail:", "language: "+ language);

            String jsonString = getAsString(SERVER_URL + "/promodetails.php?v=3&l=" + language + "&id=" + eventId);
            JSONObject json = new JSONObject(jsonString);
            String id = json.getString("i");

            String desc = json.isNull("d") ? "" : Utility.unEscapeHtml(json.getString("d"));
            String title = json.isNull("n") ? "" : Utility.unEscapeHtml(json.getString("n"));

            JSONArray mediaArray = json.getJSONArray("m");
            ArrayList<EventMedia> mediaList = new ArrayList<EventMedia>();
            for (int i = 0; i < mediaArray.length(); i++) {
                JSONObject mediaJSONObject = mediaArray.getJSONObject(i);

                String type = mediaJSONObject.getString("t").toLowerCase();
                if ("y".equals(type)) {
                    Log.d(this.getClass().getSimpleName(), "event " + id + " media " + i + " is youtube");
                    EventYoutube media = new EventYoutube();
                    media.setId(id);
                    media.setType(type);
                    media.setUrl(mediaJSONObject.getString("u"));
                    mediaList.add(media);
                } else {
                    Log.d(this.getClass().getSimpleName(), "event " + id + " media " + i + " is image");
                    EventImage media = new EventImage();
                    media.setId(id);
                    media.setType(type);
                    media.setUrl(mediaJSONObject.getString("u"));
                    mediaList.add(media);
                }
            }

            if (event == null) {
                event = new Promotion();
            }
            event.setId(id);
            event.setTitle(title);
            event.setDescription(desc);
            event.setMediaList(mediaList);
            eventCache.put(event.getInternalId(), event);
            event.setDetailsLoaded(true);

        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }

        return event;
    }

    public Drawable getPromotionDetailImage(String id, String url) throws ImpactException {
        try {
            Promotion promotion = (Promotion) eventCache.get(Constants.PREFIX_PROMOTION + id);
            if (promotion == null) {
                return null;
            }
            Drawable drawableImage = promotion.getDetailImage(url);
            if (drawableImage != null) {
                return drawableImage;
            }
            InputStream imageStream = get(url);
            drawableImage = Drawable.createFromStream(imageStream, "resource-name");
            promotion.setDetailImage(url, drawableImage);
            return drawableImage;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }

    }

    public void setPromotionDetailImage(String id, String key, Drawable image) {
        Promotion event = (Promotion) eventCache.get(Constants.PREFIX_PROMOTION + id);
        if (event != null) {
            event.setDetailImage(key, image);
        }
    }

    public Drawable getPromotionListImage(String id) throws ImpactException {
        try {
            Promotion promotion = (Promotion) eventCache.get(Constants.PREFIX_PROMOTION + id);
            if (promotion == null) {
                return null;
            }
            Drawable drawableImage = promotion.getListImage();
            if (drawableImage != null) {
                return drawableImage;
            }
            StringBuilder builder = new StringBuilder();
            builder.append(SERVER_URL);
            builder.append("/pl/");
            builder.append(id);
            builder.append(".png");
            InputStream imageStream = get(builder.toString());
            drawableImage = Drawable.createFromStream(imageStream, "resource-name");
            promotion.setListImage(drawableImage);
            return drawableImage;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }
    }

    public ParkingDetail getParkingP3() throws ImpactException {
        ParkingDetail parking = new ParkingDetail();
        try {
            String jsonString = getAsString(SERVER_URL + "/parkings.php?v=3&l=" + language);
            JSONObject json = new JSONObject(jsonString);
            JSONArray pinList = json.getJSONArray("pins");
            Log.i("DEBUG", "pinList Len: " + pinList.length());
            for (int i = 0; i < pinList.length(); i++) {
                if(!pinList.getJSONObject(i).has("at")) {
                    Log.i("DEBUG", "continue: " );
                    continue;
                }
                String availableParking = pinList.getJSONObject(i).getString("at");
                parking.setAvailableParking(availableParking);
                Log.i("DEBUG", "availableParking: " + availableParking);

                JSONObject detail = pinList.getJSONObject(i).getJSONObject("d");
                JSONObject parkingLots = detail.getJSONObject("pl");

                for(Iterator<String> iter = parkingLots.keys(); iter.hasNext();) {
                    String key = iter.next();
                    String value = parkingLots.getString(key);
                    parking.getParkingLot().put(key, value);
                }
            }
        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }
        return parking;
    }

    private InputStream get(String url) throws IOException {
        InputStream inputStream = null;

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().byteStream();
    }

    private String getAsString(String url) throws IOException {
        InputStream stream = get(url);
        String content = convertStreamToString(stream);
        return content;
    }

    private String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine() method. We iterate until the BufferedReader return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
            }
        }
        return sb.toString();
    }

    private String convertStreamToQueryString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine() method. We iterate until the BufferedReader return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(this.getClass().getName(), e.getMessage() == null ? "" : e.getMessage());
            }
        }
        return "?" + sb.toString();
    }


    public Traffic getTraffic() throws ImpactException {
        try {
            if(trafficCache !=null) {
                return trafficCache;
            }
            StringBuilder builder = new StringBuilder();
            builder.append(SERVER_URL);
            builder.append("/trafficdetails.php?l="+language);
            String jsonString = getAsString(builder.toString());
            JSONObject json = new JSONObject(jsonString);
            String title = json.getString("tbt");
            String text = json.getString("t");
            String desc = json.getString("d");
            Traffic traffic = new Traffic();
            traffic.setTitle(title);
            traffic.setDescription(text);
            traffic.setLongDesc(desc);
            trafficCache = traffic;
            return traffic;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }
    }


    public Drawable getParkingDetailImage(String url) throws ImpactException {
        try {
            Drawable parkingDetailDrawable = (Drawable) parkingDetailImageCache.get(url);
            if (parkingDetailDrawable != null) {
                return parkingDetailDrawable;
            }
            InputStream imageStream = get(url);
            Drawable drawableImage = Drawable.createFromStream(imageStream, "resource-name");
            parkingDetailImageCache.put(url, drawableImage);
            return drawableImage;
        } catch (Exception e) {
            throw new ImpactException(e.getMessage(), e);
        }

    }

}
